import { TestBed, inject } from '@angular/core/testing';

import { OutdoorgamesService } from './outdoorgames.service';

describe('OutdoorgamesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [OutdoorgamesService]
    });
  });

  it('should be created', inject([OutdoorgamesService], (service: OutdoorgamesService) => {
    expect(service).toBeTruthy();
  }));
});

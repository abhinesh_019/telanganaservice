import { TestBed, inject } from '@angular/core/testing';

import { DancemainService } from './dancemain.service';

describe('DancemainService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DancemainService]
    });
  });

  it('should be created', inject([DancemainService], (service: DancemainService) => {
    expect(service).toBeTruthy();
  }));
});

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
 import 'rxjs/add/operator/map';
import { environment } from '../../../environments/environment.prod';

@Injectable()

export class PackersMoversService {
  constructor(private http: HttpClient) { }

  locationapi(){
    
    return this.http.get(environment.apiUrl +"/packersMoversLocations");
  }

  areaapi(id){
    
    return this.http.get(environment.apiUrl +"/packersMoversareas/"+id);
  }
  indoorUsers(id,data){
    
    return this.http.get(environment.apiUrl +"/packersMoversClientsget/"+id,{params:data});
  }
  indoorUserscountsAll(){
    
    return this.http.get(environment.apiUrl +"/packersMoversClientsAllCount");
  }

  indoorUserscountAllMainAreas(id){
    
    return this.http.get(environment.apiUrl +"/packersMoversClientsCountAreas/"+id);
  }

  indoorUserscountAllDist(){
    
    return this.http.get(environment.apiUrl +"/packersMoversClientsCountDist");
  }

  indoorUserscountAllAreas(id){
    
    return this.http.get(environment.apiUrl +"/packersMoversClientsCount/"+id);
  }
  indoorUserscountAllAreasover(id){
    
    return this.http.get(environment.apiUrl +"/packersMoversClientsAreas/"+id);
  }

  babycaresAddsOnea(id){

    return this.http.get(environment.apiUrl +"/packersMoversAddsOneGeta/"+id);
  }
  babycaresAddsTwoa(id){
    
    return this.http.get(environment.apiUrl +"/packersMoversAddsTwoGeta/"+id);
  }
  babycaresAddsThreea(id){
    
    return this.http.get(environment.apiUrl +"/packersMoversAddsThreeGeta/"+id);
  }
  babycaresAddsFoura(id){
    
    return this.http.get(environment.apiUrl +"/packersMoversAddsFourGeta/"+id);
  }
   
  babycaresAddsOnel(id){
    
    return this.http.get(environment.apiUrl +"/packersMoversAddsOneLocGet/"+id);
  }
  babycaresAddsTwol(id){
    
    return this.http.get(environment.apiUrl +"/packersMoversAddsTwoLocGet/"+id);
  }
  babycaresAddsThreel(id){
    
    return this.http.get(environment.apiUrl +"/packersMoversAddsThreeGetLoc/"+id);
  }
  babycaresAddsFourl(id){
    
    return this.http.get(environment.apiUrl +"/packersMoversAddsFourLocGet/"+id);
  }

  babycaresAddsFivel(id){
    
    return this.http.get(environment.apiUrl +"/packersMoversAddsFiveLocGet/"+id);
  }
  }
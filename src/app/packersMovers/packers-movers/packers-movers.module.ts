import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {HttpClientModule} from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { PackersMoversComponent } from './packers-movers.component';
import { PackersMoversService } from './packers-movers.service';
import { PackersMoversDetailsModule } from '../packers-movers-details/packers-movers-details.module';
 
const routes: Routes = [
  {
    path: '', component: PackersMoversComponent, children:
      [
      { path: 'packersMoversdetails/:_id/:name', loadChildren: 'app/packersMovers/packers-movers-details/packers-movers-details.module#PackersMoversDetailsModule'},
 
      ]
  }]



@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
     SharedModule,
     PackersMoversDetailsModule
       ],
  declarations: [
    PackersMoversComponent
     ],
  providers:[PackersMoversService]
})

export class PackersMoversModule { }

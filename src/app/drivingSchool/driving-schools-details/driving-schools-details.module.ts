import {MatInputModule} from '@angular/material/input';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material';

import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { DrivingSchoolsDetailsService } from './driving-schools-details.service';
import { DrivingSchoolsDetailsComponent } from './driving-schools-details.component';
 
const routes:Routes=[{path:'drivingSchoolsdetails/:_id/:name',component:DrivingSchoolsDetailsComponent},]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    SharedModule
    

  ],
  declarations: [
    DrivingSchoolsDetailsComponent,

  ],
  providers: [DrivingSchoolsDetailsService],
})
export class DrivingSchoolsDetailsModule { }

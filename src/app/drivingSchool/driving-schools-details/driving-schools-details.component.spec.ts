import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DrivingSchoolsDetailsComponent } from './driving-schools-details.component';

describe('DrivingSchoolsDetailsComponent', () => {
  let component: DrivingSchoolsDetailsComponent;
  let fixture: ComponentFixture<DrivingSchoolsDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DrivingSchoolsDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DrivingSchoolsDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

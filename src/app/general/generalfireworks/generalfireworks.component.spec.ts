import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GeneralfireworksComponent } from './generalfireworks.component';

describe('GeneralfireworksComponent', () => {
  let component: GeneralfireworksComponent;
  let fixture: ComponentFixture<GeneralfireworksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GeneralfireworksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GeneralfireworksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { TestBed, inject } from '@angular/core/testing';

import { GeneralTutionsService } from './general-tutions.service';

describe('GeneralTutionsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GeneralTutionsService]
    });
  });

  it('should be created', inject([GeneralTutionsService], (service: GeneralTutionsService) => {
    expect(service).toBeTruthy();
  }));
});

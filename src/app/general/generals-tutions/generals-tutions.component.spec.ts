import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GeneralsTutionsComponent } from './generals-tutions.component';

describe('GeneralsTutionsComponent', () => {
  let component: GeneralsTutionsComponent;
  let fixture: ComponentFixture<GeneralsTutionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GeneralsTutionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GeneralsTutionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

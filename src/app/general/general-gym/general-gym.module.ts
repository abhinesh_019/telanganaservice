import {MatInputModule} from '@angular/material/input';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material';

import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
 
import { GeneralGymComponent } from './general-gym.component';
import { GeneralGymService } from './general-gym.service';


const routes:Routes=[{path:'generalsgym',component:GeneralGymComponent}]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    SharedModule
    ],
  declarations: [
    GeneralGymComponent
  ],
  providers: [ GeneralGymService],
})
 
export class GeneralGymModule { }

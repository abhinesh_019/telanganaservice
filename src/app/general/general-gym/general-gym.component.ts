import { Component, OnInit } from '@angular/core';
import { GeneralGymService } from './general-gym.service';

@Component({
  selector: 'app-general-gym',
  templateUrl: './general-gym.component.html',
  styleUrls: ['./general-gym.component.css']
})
export class GeneralGymComponent implements OnInit {
  public imageAddsOne:any
  public babycaresAddsOnes:any
   
  public babycaresAddsFours:any
  public imageAddsFour:any
  public imageAddsThree:any
  public imageAddsTwo:any
  public babycaresAddsThrees:any
  public babycaresAddsTwos:any
  
  
  public babycaresAddsFivesLoc:any
  public imageAddsFiveLoc:any
  public imageAddsFourLoc:any
  public babycaresAddsFoursLoc:any
  public imageAddsThreeLoc:any
  public babycaresAddsThreeLoc:any
  public imageAddsTwoLoc:any
  public babycaresAddsTwoLoc:any
  public imageAddsOneLoc:any
  public babycaresAddsOneLoc:any
  public babycaresAddsOneLocl:any


  areaAddsOne={
    "addsOneLink":"",
    "addsOneImg":"",
  }
  areaAddsTwo={
    "addsTwoLink":"",
    "addsTwoImg":"",
  }
  areaAddsThree={
    "addsThreeLink":"",
    "addsThreeImg":"",
  }
  areaAddsFour={
    "addsFourLink":"",
    "addsFourImg":"",
  }
   
  
  areaAddsOneLoc={
    "addsOneLinkLoc":"",
    "addsOneImgLoc":"",
  }
  areaAddsTwoLoc={
    "addsTwoLinkLoc":"",
    "addsTwoImgLoc":"",
  }
  areaAddsThreeLoc={
    "addsThreeLinkLoc":"",
    "addsThreeImgLoc":"",
  }
  areaAddsFourLoc={
    "addsFourLinkLoc":"",
    "addsFourImgLoc":"",
  }
  areaAddsFiveLoc={
    "addsFiveLinkLoc":"",
    "addsFiveImgLoc":"",
  }
   
  locationsdata={
    "locations":""
  }

  areas={
    "area":""
  }

  usersMain={
    "gymName":"",
    "keyWords":"",
    "name":"",
    "ourTeam":"",
    "teamMembersNames":"",
    "teamDescriptions":"",
    "aboutUsDescription1":"",
    "aboutUsDescription2":"",
    "day1":"",
    "day2":"",
    "day3":"",
    "day4":"",
    "day5":"",
    "day6":"",
    "day7":"",
    "allTimes":"",
   "houseNo":"",
    "subArea":"",
    "landmark":"",
    "city":"",
  "distict":"",
   "state":"",
     "pincode":"",
    "officeNo":"",
     "mobileNo":"",
     "whatsupno":"",
     "emailId":"",
     "lat":"",
     "long":"",
   "images1":"",
  "images2":"",
  "images3":"",
  "images4":"",
  "images5":"",
  "images6":"",
  }
  
  public locationsgets:any
  public locationName:any
  public its:any
  public locationss:any
  public areasAll:any
  public selectAreaId:any
  public selectArea:any
  public imagesName:any
  public catList:any
  public catName:any
  public it:any

  formData: FormData = new FormData(); 

  constructor(private gym:GeneralGymService) { }

  ngOnInit() {
     this.getlocations()
  }

// ********************* locations ***************   

  postlocations(){
    this.gym.locationspost(this.locationsdata).subscribe((res)=>{
      this.locationsdata.locations=""
    })  }

    getlocations(){
      this.gym.locationsget().subscribe((res)=>{
      this.locationsgets=res
       var id =this.locationsgets[0];
      this.locations(id)
      })  }
      locations(get?){
        this.locationName=get.locations
        this.it=get._id
        this.locationss=get.locations
        this.allAreas()
        this.babycaressareasAddsOneLoc()
        this.babycaressareasAddsTwoLoc()
        this.babycaressareasAddsThreeLoc()
        this.babycaressareasAddsFourLoc()
        this.babycaressareasAddsFiveLoc()
      }
// ********************* Area ***************   

      allAreas(){
        this.gym.areaapi(this.it).subscribe((res)=>{
          this.areasAll=res
          var id =this.areasAll[0];          
          this.selectedAreas(id)
         })
       }
      
      selectedAreas(result){
        this.selectAreaId=result._id
        this.selectArea=result.area
        this.babycaressareasAddsOnea()
        this.babycaressareasAddsTwoa()
        this.babycaressareasAddsThreea()
        this.babycaressareasAddsFoura()
      }
      
      postarea(){
        this.gym.areapost(this.it,this.areas).subscribe((res)=>{
           this.areas.area=""
        }) 
      }

      // / ***************************************post users ********************************      
 handleFileInput($event:any,img) {
  let imageFile= File = $event.target.files[0];
  this.formData.append(img, imageFile); 
  
}
postUsers(){
  
   
    this.formData.append('name',this.usersMain.name);
    this.formData.append('gymName',this.usersMain.gymName);
    this.formData.append('keyWords',this.usersMain.keyWords);
    this.formData.append('aboutUsDescription1',this.usersMain.aboutUsDescription1);
    this.formData.append('aboutUsDescription2',this.usersMain.aboutUsDescription2);
    this.formData.append('ourTeam',this.usersMain.ourTeam);
    this.formData.append('teamMembersNames',this.usersMain.teamMembersNames);
    this.formData.append('teamDescriptions',this.usersMain.teamDescriptions);
     this.formData.append('day1',this.usersMain.day1);
    this.formData.append('day2',this.usersMain.day2);
    this.formData.append('day3',this.usersMain.day3);
    this.formData.append('day4',this.usersMain.day4);
    this.formData.append('day5',this.usersMain.day5);
    this.formData.append('day6',this.usersMain.day6);
    this.formData.append('day7',this.usersMain.day7);
    this.formData.append('allTimes',this.usersMain.allTimes);
    this.formData.append('houseNo',this.usersMain.houseNo);
    this.formData.append('mainArea',this.selectAreaId);
    this.formData.append('subArea',this.selectArea);
    this.formData.append('landmark',this.usersMain.landmark);
     this.formData.append('city',this.usersMain.city);
    this.formData.append('distict',this.locationName,);
    this.formData.append('state',this.usersMain.state);
    this.formData.append('pincode',this.usersMain.pincode);
    this.formData.append('officeNo',this.usersMain.officeNo);
    this.formData.append('mobileNo',this.usersMain.mobileNo);
    this.formData.append('whatsupno',this.usersMain.whatsupno);
    this.formData.append('emailId',this.usersMain.emailId);
    this.formData.append('lat',this.usersMain.whatsupno);
    this.formData.append('long',this.usersMain.emailId);
   

  this.usersMain.name="",
  this.usersMain.keyWords="",
  this.usersMain.gymName=""
  this.usersMain.aboutUsDescription1="",
  this.usersMain.aboutUsDescription2="",
  this.usersMain.lat="",
  this.usersMain.long="",
  this.usersMain.day1="",
  this.usersMain.day2="",
  this.usersMain.day3="",
  this.usersMain.day4="",
  this.usersMain.day5="",
  this.usersMain.day6="",
  this.usersMain.day7="",
  this.usersMain.allTimes="",
  this.usersMain.houseNo="",
  this.usersMain.subArea="",
  this.usersMain.landmark="",
  this.usersMain.city="",
  this.usersMain.distict="",
  this.usersMain.state="",
  this.usersMain.pincode="",
  this.usersMain.officeNo="",
  this.usersMain.mobileNo="",
  this.usersMain.whatsupno="",
  this.usersMain.emailId="",
  this.usersMain.images1="",
  this.usersMain.images2="",
  this.usersMain.images3="",
  this.usersMain.images4="",
  this.usersMain.images5="",
  this.usersMain.images6="",
   
  
      
        this.gym.areapostusers(this.selectAreaId,this.formData).subscribe((res)=>{
          this.areas.area=""
        }) 
      
  
  
  }

// **************************************************************************************
 
      

// ********************************************************* Adds Areas One ***********************************************************************

handleFileInputUpdates($event:any,images){
  this.imageAddsOne= File = $event.target.files[0];
 }
 

postAddsOne(){
this.formData.append('addsOneImg',this.imageAddsOne);
this.formData.append('addsOneLink',this.areaAddsOne.addsOneLink); 
 
 this.gym.postAddsOne(this.selectAreaId,this.formData).subscribe((res)=>{
     })
     this.areaAddsOne.addsOneLink="",
     this.areaAddsOne.addsOneImg=""
}
babycaressareasAddsOnea(){
  this.gym.babycaresAddsOnea(this.selectAreaId).subscribe((res)=>{
     this.babycaresAddsOnes=res
 
   })
}
babycaressareasAddsOneaDelete(identities){
  let areaIds=identities._id
  this.gym.babycaresAddsOneaDelete(areaIds).subscribe((res)=>{
    
  })
}

// ********************************************************* Adds Areas Two ***********************************************************************

handleFileInputUpdatesTwo($event:any,images){
  this.imageAddsTwo= File = $event.target.files[0];
 }
 

postAddsTwo(){
this.formData.append('addsTwoImg',this.imageAddsTwo);
this.formData.append('addsTwoLink',this.areaAddsTwo.addsTwoLink); 
 
 this.gym.postAddsTwo(this.selectAreaId,this.formData).subscribe((res)=>{
     })
     this.areaAddsTwo.addsTwoLink="",
     this.areaAddsTwo.addsTwoImg=""
}
babycaressareasAddsTwoa(){
  this.gym.babycaresAddsTwoa(this.selectAreaId).subscribe((res)=>{
     this.babycaresAddsTwos=res
   })
}
babycaressareasAddsTwoaDelete(identities){
  let areaIds=identities._id
  this.gym.babycaresAddsTwoaDelete(areaIds).subscribe((res)=>{
    
  })
}


// ********************************************************* Adds Areas Three ***********************************************************************

handleFileInputUpdatesThree($event:any,images){
  this.imageAddsThree= File = $event.target.files[0];
 }
 

postAddsThree(){
this.formData.append('addsThreeImg',this.imageAddsThree);
this.formData.append('addsThreeLink',this.areaAddsThree.addsThreeLink); 
 
 this.gym.postAddsThree(this.selectAreaId,this.formData).subscribe((res)=>{
     })
     this.areaAddsThree.addsThreeLink="",
     this.areaAddsThree.addsThreeImg=""
}
babycaressareasAddsThreea(){
  this.gym.babycaresAddsThreea(this.selectAreaId).subscribe((res)=>{
     this.babycaresAddsThrees=res
   })
}
babycaressareasAddsThreeaDelete(identities){
  let areaIds=identities._id
  this.gym.babycaresAddsThreeaDelete(areaIds).subscribe((res)=>{  
  })
}


// ********************************************************* Adds Areas Four ***********************************************************************

handleFileInputUpdatesFour($event:any,images){
  this.imageAddsFour= File = $event.target.files[0];
 }
 

postAddsFour(){
this.formData.append('addsFourImg',this.imageAddsFour);
this.formData.append('addsFourLink',this.areaAddsFour.addsFourLink); 
 
 this.gym.postAddsFour(this.selectAreaId,this.formData).subscribe((res)=>{
     })
     this.areaAddsFour.addsFourLink="",
     this.areaAddsFour.addsFourImg=""
}
babycaressareasAddsFoura(){
  this.gym.babycaresAddsFoura(this.selectAreaId).subscribe((res)=>{
     this.babycaresAddsFours=res
   })
}
babycaressareasAddsFouraDelete(identities){
  let areaIds=identities._id
  this.gym.babycaresAddsFouraDelete(areaIds).subscribe((res)=>{
  })
}

 

 


// ********************************************************* Adds Areas One ***********************************************************************

handleFileInputUpdatesOneLoc($event:any,images){
  this.imageAddsOneLoc= File = $event.target.files[0];
 }
 

postAddsOneLoc(){
this.formData.append('addsOneImgLoc',this.imageAddsOneLoc);
this.formData.append('addsOneLinkLoc',this.areaAddsOneLoc.addsOneLinkLoc); 
 
 this.gym.postAddsOnepl(this.it,this.formData).subscribe((res)=>{
    
})
     this.areaAddsOneLoc.addsOneImgLoc="",
     this.areaAddsOneLoc.addsOneLinkLoc=""
}
babycaressareasAddsOneLoc(){
  this.gym.babycaresAddsOnel(this.it).subscribe((res)=>{
     this.babycaresAddsOneLocl=res
   })
}
babycaressareasAddsOneLocDelete(identities){
  let areaIds=identities._id
  this.gym.babycaresAddsOneLocDelete(areaIds).subscribe((res)=>{
    
  })
}

// ********************************************************* Adds Areas Two ***********************************************************************

handleFileInputUpdatesTwoLoc($event:any,images){
  this.imageAddsTwoLoc= File = $event.target.files[0];
 }
 

postAddsTwoLoc(){
this.formData.append('addsTwoImgLoc',this.imageAddsTwoLoc);
this.formData.append('addsTwoLinkLoc',this.areaAddsTwoLoc.addsTwoLinkLoc); 
 
 this.gym.postAddsTwopl(this.it,this.formData).subscribe((res)=>{
     })
     this.areaAddsTwoLoc.addsTwoLinkLoc="",
     this.areaAddsTwoLoc.addsTwoImgLoc=""
}
babycaressareasAddsTwoLoc(){
  this.gym.babycaresAddsTwol(this.it).subscribe((res)=>{
     this.babycaresAddsTwoLoc=res
   })
}
babycaressareasAddsTwoLocDelete(identities){
  let areaIds=identities._id
  this.gym.babycaresAddsTwoLocDelete(areaIds).subscribe((res)=>{
    
  })
}


// ********************************************************* Adds Areas Three Loc***********************************************************************

handleFileInputUpdatesThreeLoc($event:any,images){
  this.imageAddsThreeLoc= File = $event.target.files[0];
 }
 

postAddsThreeLoc(){
this.formData.append('addsThreeImgLoc',this.imageAddsThreeLoc);
this.formData.append('addsThreeLinkLoc',this.areaAddsThreeLoc.addsThreeLinkLoc); 
 
 this.gym.postAddsThreepl(this.it,this.formData).subscribe((res)=>{
     })
     this.areaAddsThreeLoc.addsThreeImgLoc="",
     this.areaAddsThreeLoc.addsThreeLinkLoc=""
}
babycaressareasAddsThreeLoc(){
  this.gym.babycaresAddsThreel(this.it).subscribe((res)=>{
     this.babycaresAddsThreeLoc=res
   })
}
babycaressareasAddsThreeLocDelete(identities){
  let areaIds=identities._id
  this.gym.babycaresAddsThreeLocDelete(areaIds).subscribe((res)=>{
  
  })
}


// ********************************************************* Adds Areas Four Loc***********************************************************************

handleFileInputUpdatesFourLoc($event:any,images){
  this.imageAddsFourLoc= File = $event.target.files[0];
 }
 

postAddsFourLoc(){
this.formData.append('addsFourImgLoc',this.imageAddsFourLoc);
this.formData.append('addsFourLinkLoc',this.areaAddsFourLoc.addsFourLinkLoc); 
 
 this.gym.postAddsFourpl(this.it,this.formData).subscribe((res)=>{
     })
     this.areaAddsFourLoc.addsFourImgLoc="",
     this.areaAddsFourLoc.addsFourLinkLoc=""
}
babycaressareasAddsFourLoc(){
  this.gym.babycaresAddsFourl(this.it).subscribe((res)=>{
     this.babycaresAddsFoursLoc=res
   })
}
babycaressareasAddsFourLocDelete(identities){
  let areaIds=identities._id
  this.gym.babycaresAddsFourLocDelete(areaIds).subscribe((res)=>{
    
  })
}


// ********************************************************* Adds Areas Five Loc***********************************************************************

handleFileInputUpdatesFiveLoc($event:any,images){
  this.imageAddsFiveLoc= File = $event.target.files[0];
 }
 

postAddsFiveLoc(){
this.formData.append('addsFiveImgLoc',this.imageAddsFiveLoc);
this.formData.append('addsFiveLinkLoc',this.areaAddsFiveLoc.addsFiveLinkLoc); 
 
 this.gym.postAddsFivepl(this.it,this.formData).subscribe((res)=>{
     })
     this.areaAddsFiveLoc.addsFiveImgLoc="",
     this.areaAddsFiveLoc.addsFiveLinkLoc=""
}
babycaressareasAddsFiveLoc(){
  this.gym.babycaresAddsFivel(this.it).subscribe((res)=>{
     this.babycaresAddsFivesLoc=res
   })
}
babycaressareasAddsFiveLocDelete(identities){
  let areaIds=identities._id
  this.gym.babycaresAddsFiveLocDelete(areaIds).subscribe((res)=>{
    
  })
}

 
}

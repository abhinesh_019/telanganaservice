import {MatInputModule} from '@angular/material/input';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material';
import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { GeneralrestaurantsComponent } from './generalrestaurants.component';
import { GeneralrestaurantsService } from './generalrestaurants.service';
 
 
const routes:Routes=[{path:'generalsrestaurants',component:GeneralrestaurantsComponent},]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    SharedModule
   ],
  declarations: [
    GeneralrestaurantsComponent
  ],
  providers: [ GeneralrestaurantsService ],
})
 
export class GeneralrestaurantsModule { }

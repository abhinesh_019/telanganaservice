import {MatInputModule} from '@angular/material/input';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material';
import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { GeneralpackersMoversComponent } from './generalpackers-movers.component';
import { GeneralpackersMoversService } from './generalpackers-movers.service';
 
const routes:Routes=[{path:'generalPackersMovers',component:GeneralpackersMoversComponent},]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    SharedModule
    

  ],
  declarations: [
    GeneralpackersMoversComponent
  ],
  providers: [ GeneralpackersMoversService],
})
export class GeneralpackersMoversModule { }

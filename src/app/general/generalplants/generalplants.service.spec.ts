import { TestBed, inject } from '@angular/core/testing';

import { GeneralplantsService } from './generalplants.service';

describe('GeneralplantsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GeneralplantsService]
    });
  });

  it('should be created', inject([GeneralplantsService], (service: GeneralplantsService) => {
    expect(service).toBeTruthy();
  }));
});

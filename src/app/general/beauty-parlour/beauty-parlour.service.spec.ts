import { TestBed, inject } from '@angular/core/testing';

import { BeautyParlourService } from './beauty-parlour.service';

describe('BeautyParlourService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BeautyParlourService]
    });
  });

  it('should be created', inject([BeautyParlourService], (service: BeautyParlourService) => {
    expect(service).toBeTruthy();
  }));
});

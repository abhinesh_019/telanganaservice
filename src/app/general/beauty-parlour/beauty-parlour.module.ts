import {MatInputModule} from '@angular/material/input';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material';

import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
  
 import { BeautyParlourComponent } from './beauty-parlour.component';
import { BeautyParlourService } from './beauty-parlour.service';


const routes:Routes=[{path:'generalsbeautyparlour',component:BeautyParlourComponent}]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    SharedModule
    

  ],
  declarations: [
    BeautyParlourComponent
  ],
  providers: [ BeautyParlourService],
})
 
export class BeautyParlourModule { }

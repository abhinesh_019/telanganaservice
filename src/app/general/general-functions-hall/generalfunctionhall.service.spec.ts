import { TestBed, inject } from '@angular/core/testing';

import { GeneralfunctionhallService } from './generalfunctionhall.service';

describe('GeneralfunctionhallService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GeneralfunctionhallService]
    });
  });

  it('should be created', inject([GeneralfunctionhallService], (service: GeneralfunctionhallService) => {
    expect(service).toBeTruthy();
  }));
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GeneralFunctionsHallComponent } from './general-functions-hall.component';

describe('GeneralFunctionsHallComponent', () => {
  let component: GeneralFunctionsHallComponent;
  let fixture: ComponentFixture<GeneralFunctionsHallComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GeneralFunctionsHallComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GeneralFunctionsHallComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

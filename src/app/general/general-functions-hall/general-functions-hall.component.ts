import { Component, OnInit } from '@angular/core';
import { GeneralfunctionhallService } from './generalfunctionhall.service';

@Component({
  selector: 'app-general-functions-hall',
  templateUrl: './general-functions-hall.component.html',
  styleUrls: ['./general-functions-hall.component.css']
})
export class GeneralFunctionsHallComponent implements OnInit {
  formData: FormData = new FormData(); 
  public imageAddsOne:any
  public babycaresAddsOnes:any
   
  public babycaresAddsFours:any
  public imageAddsFour:any
  public imageAddsThree:any
  public imageAddsTwo:any
  public babycaresAddsThrees:any
  public babycaresAddsTwos:any
  
  
  public babycaresAddsFivesLoc:any
  public imageAddsFiveLoc:any
  public imageAddsFourLoc:any
  public babycaresAddsFoursLoc:any
  public imageAddsThreeLoc:any
  public babycaresAddsThreeLoc:any
  public imageAddsTwoLoc:any
  public babycaresAddsTwoLoc:any
  public imageAddsOneLoc:any
  public babycaresAddsOneLoc:any
  public babycaresAddsOneLocl:any


  areaAddsOne={
    "addsOneLink":"",
    "addsOneImg":"",
  }
  areaAddsTwo={
    "addsTwoLink":"",
    "addsTwoImg":"",
  }
  areaAddsThree={
    "addsThreeLink":"",
    "addsThreeImg":"",
  }
  areaAddsFour={
    "addsFourLink":"",
    "addsFourImg":"",
  }
   
  
  areaAddsOneLoc={
    "addsOneLinkLoc":"",
    "addsOneImgLoc":"",
  }
  areaAddsTwoLoc={
    "addsTwoLinkLoc":"",
    "addsTwoImgLoc":"",
  }
  areaAddsThreeLoc={
    "addsThreeLinkLoc":"",
    "addsThreeImgLoc":"",
  }
  areaAddsFourLoc={
    "addsFourLinkLoc":"",
    "addsFourImgLoc":"",
  }
  areaAddsFiveLoc={
    "addsFiveLinkLoc":"",
    "addsFiveImgLoc":"",
  }
   

  locationsdata={
    "locations":""
  }
  catdata={
    "categories":"",
    "images1":"",
   }
  areas={
    "area":""
  }

usersMain={
  "name":"",
  "shopName":"",
  "hallCatageries":"",
  "profileDes1":"",
  "profileDes2":"",
  "ourTeam":"",
  "vinueInformations":"",
  "brides":"",
   "grooms":"",
  "day1":"",
  "day2":"",
  "day3":"",
  "day4":"",
  "day5":"",
  "day6":"",
  "day7":"",
  "allTimes":"",
 "houseNo":"",
  "subArea":"",
  "landmark":"",
  "city":"",
"distict":"",
 "state":"",
   "pincode":"",
  "officeNo":"",
   "mobileNo":"",
   "whatsupno":"",
   "emailId":"",
   "latitude":"",
   "longitude":"",
 "images1":"",
"images2":"",
"images3":"",
"images4":"",
"images5":"",
"images6":"",
}

 
  public locationsgets:any
  public locationName:any
  public its:any
  public it:any
  public locationss:any
  public areasAll:any
  public selectAreaId:any
  public selectArea:any
  public imagesName:any
  public catList:any
  public catName:any

  constructor(private gfh:GeneralfunctionhallService) { }

  ngOnInit() {
    this.getcatagories()
     
  }

   
// *********************catagories***************   
handleFile($event:any,images){
  this.imagesName= File = $event.target.files[0];
 
 console.log(this.imagesName);
 
}

postcat(){
 
 this.formData.append('images1', this.imagesName);
 
 this.formData.append('categories',this.catdata.categories);
    

  console.log(this.formData)

 this.gfh.catpost(this.formData).subscribe((res)=>{
     
     })

     this.catdata.categories="",
     this.catdata.images1=""
     
}

getcatagories(){
  this.gfh.catapiget().subscribe((res)=>{
this.catList=res
console.log(this.catList);

var id =this.catList[0];
      console.log(this.catList[0]);
      this.catagoriescal(id)

        })  }
      catagoriescal(get?){
        this.locationName="";
        this.selectAreaId="";
        this.catName=get.categories;
        this.its=get._id;
        this.getlocations()
       }
      
      // ************************************   


  postlocations(){
    this.gfh.locationspost(this.its,this.locationsdata).subscribe((res)=>{
      this.locationsdata.locations=""
    })  }

    getlocations(){
      this.gfh.locationsget(this.its).subscribe((res)=>{
      this.locationsgets=res
       var id =this.locationsgets[0];
      this.locations(id)
      })  }
      locations(get?){
        this.selectArea="";
        this.locationName=get.locations
        this.it=get._id
        this.locationss=get.locations
        this.allAreas()
        this.babycaressareasAddsOneLoc()
        this.babycaressareasAddsTwoLoc()
        this.babycaressareasAddsThreeLoc()
        this.babycaressareasAddsFourLoc()
        this.babycaressareasAddsFiveLoc()
      }

      allAreas(){
        this.gfh.areaapi(this.it).subscribe((res)=>{
          this.areasAll=res
          var id =this.areasAll[0];          
          this.selectedAreas(id)
         })
       }
      
      selectedAreas(result){
        this.selectAreaId=result._id
        this.selectArea=result.area
        this.babycaressareasAddsOnea()
        this.babycaressareasAddsTwoa()
        this.babycaressareasAddsThreea()
        this.babycaressareasAddsFoura()
      }
      
      postarea(){
        this.gfh.areapost(this.it,this.areas).subscribe((res)=>{
           console.log(res);
          this.areas.area=""
        }) 
      }

      // / ***************************************post users ********************************  
handleFileInput($event:any,img) {
  let imageFile= File = $event.target.files[0];
  this.formData.append(img, imageFile);
  console.log(imageFile);
  
}

postUsers(){
  
   
      this.formData.append('name',this.usersMain.name);
      this.formData.append('shopName',this.usersMain.shopName);
      this.formData.append('hallCatageries',this.usersMain.hallCatageries);
      this.formData.append('profileDes1',this.usersMain.profileDes1);
      this.formData.append('profileDes2',this.usersMain.profileDes2);
      this.formData.append('ourTeam',this.usersMain.ourTeam);
      this.formData.append('vinueInformations',this.usersMain.vinueInformations);
      this.formData.append('brides',this.usersMain.brides);
      this.formData.append('grooms',this.usersMain.grooms);
      this.formData.append('day1',this.usersMain.day1);
      this.formData.append('day2',this.usersMain.day2);
      this.formData.append('day3',this.usersMain.day3);
      this.formData.append('day4',this.usersMain.day4);
      this.formData.append('day5',this.usersMain.day5);
      this.formData.append('day6',this.usersMain.day6);
      this.formData.append('day7',this.usersMain.day7);
      this.formData.append('allTimes',this.usersMain.allTimes);
      this.formData.append('houseNo',this.usersMain.houseNo);
      this.formData.append('mainArea',this.selectAreaId);
      this.formData.append('subArea',this.usersMain.subArea);
      this.formData.append('landmark',this.usersMain.landmark);
      this.formData.append('city',this.usersMain.city);
      this.formData.append('distict',this.locationName);
      this.formData.append('state',this.usersMain.state);
      this.formData.append('pincode',this.usersMain.pincode);
      this.formData.append('officeNo',this.usersMain.officeNo);
      this.formData.append('mobileNo',this.usersMain.mobileNo);
      this.formData.append('whatsupno',this.usersMain.whatsupno);
      this.formData.append('emailId',this.usersMain.emailId);
      this.formData.append('latitude',this.usersMain.latitude);
      this.formData.append('longitude',this.usersMain.longitude);
   

    this.usersMain.name="",
      this.usersMain.hallCatageries="",
      this.usersMain.shopName="",
      this.usersMain.ourTeam="",
      this.usersMain.vinueInformations="",
      this.usersMain.brides="",
      this.usersMain.grooms="",
      this.usersMain.profileDes1="",
      this.usersMain.profileDes2="",
      this.usersMain.day1="",
      this.usersMain.day2="",
      this.usersMain.day3="",
      this.usersMain.day4="",
      this.usersMain.day5="",
      this.usersMain.day6="",
      this.usersMain.day7="",
      this.usersMain.allTimes="", 
      this.usersMain.houseNo="",
      this.usersMain.subArea="",
      this.usersMain.landmark="",
      this.usersMain.city="",
      this.usersMain.state="",
      this.usersMain.pincode="",
      this.usersMain.officeNo="",
      this.usersMain.mobileNo="",
      this.usersMain.whatsupno="",
      this.usersMain.emailId="",
      this.usersMain.latitude="",
      this.usersMain.longitude="",
      this.usersMain.images1="",
      this.usersMain.images2="",
      this.usersMain.images3="",
      this.usersMain.images4="",
      this.usersMain.images5="",
      this.usersMain.images6="",
       
  
      
        this.gfh.areapostuserss(this.selectAreaId,this.formData).subscribe((res)=>{
           console.log(res);
          this.areas.area=""
        }) 
      
  
  
  }

// **************************************************************************************
 
      


// ********************************************************* Adds Areas One ***********************************************************************

handleFileInputUpdates($event:any,images){
  this.imageAddsOne= File = $event.target.files[0];
 }
 

postAddsOne(){
this.formData.append('addsOneImg',this.imageAddsOne);
this.formData.append('addsOneLink',this.areaAddsOne.addsOneLink); 
 
 this. gfh.postAddsOne(this.selectAreaId,this.formData).subscribe((res)=>{
     })
     this.areaAddsOne.addsOneLink="",
     this.areaAddsOne.addsOneImg=""
}
babycaressareasAddsOnea(){
  this. gfh.babycaresAddsOnea(this.selectAreaId).subscribe((res)=>{
     this.babycaresAddsOnes=res
 
   })
}
babycaressareasAddsOneaDelete(identities){
  let areaIds=identities._id
  this. gfh.babycaresAddsOneaDelete(areaIds).subscribe((res)=>{
    
  })
}

// ********************************************************* Adds Areas Two ***********************************************************************

handleFileInputUpdatesTwo($event:any,images){
  this.imageAddsTwo= File = $event.target.files[0];
 }
 

postAddsTwo(){
this.formData.append('addsTwoImg',this.imageAddsTwo);
this.formData.append('addsTwoLink',this.areaAddsTwo.addsTwoLink); 
 
 this. gfh.postAddsTwo(this.selectAreaId,this.formData).subscribe((res)=>{
     })
     this.areaAddsTwo.addsTwoLink="",
     this.areaAddsTwo.addsTwoImg=""
}
babycaressareasAddsTwoa(){
  this. gfh.babycaresAddsTwoa(this.selectAreaId).subscribe((res)=>{
     this.babycaresAddsTwos=res
   })
}
babycaressareasAddsTwoaDelete(identities){
  let areaIds=identities._id
  this. gfh.babycaresAddsTwoaDelete(areaIds).subscribe((res)=>{
    
  })
}


// ********************************************************* Adds Areas Three ***********************************************************************

handleFileInputUpdatesThree($event:any,images){
  this.imageAddsThree= File = $event.target.files[0];
 }
 

postAddsThree(){
this.formData.append('addsThreeImg',this.imageAddsThree);
this.formData.append('addsThreeLink',this.areaAddsThree.addsThreeLink); 
 
 this. gfh.postAddsThree(this.selectAreaId,this.formData).subscribe((res)=>{
     })
     this.areaAddsThree.addsThreeLink="",
     this.areaAddsThree.addsThreeImg=""
}
babycaressareasAddsThreea(){
  this. gfh.babycaresAddsThreea(this.selectAreaId).subscribe((res)=>{
     this.babycaresAddsThrees=res
   })
}
babycaressareasAddsThreeaDelete(identities){
  let areaIds=identities._id
  this. gfh.babycaresAddsThreeaDelete(areaIds).subscribe((res)=>{  
  })
}


// ********************************************************* Adds Areas Four ***********************************************************************

handleFileInputUpdatesFour($event:any,images){
  this.imageAddsFour= File = $event.target.files[0];
 }
 

postAddsFour(){
this.formData.append('addsFourImg',this.imageAddsFour);
this.formData.append('addsFourLink',this.areaAddsFour.addsFourLink); 
 
 this. gfh.postAddsFour(this.selectAreaId,this.formData).subscribe((res)=>{
     })
     this.areaAddsFour.addsFourLink="",
     this.areaAddsFour.addsFourImg=""
}
babycaressareasAddsFoura(){
  this. gfh.babycaresAddsFoura(this.selectAreaId).subscribe((res)=>{
     this.babycaresAddsFours=res
   })
}
babycaressareasAddsFouraDelete(identities){
  let areaIds=identities._id
  this. gfh.babycaresAddsFouraDelete(areaIds).subscribe((res)=>{
  })
}

 

 


// ********************************************************* Adds Areas One ***********************************************************************

handleFileInputUpdatesOneLoc($event:any,images){
  this.imageAddsOneLoc= File = $event.target.files[0];
 }
 

postAddsOneLoc(){
this.formData.append('addsOneImgLoc',this.imageAddsOneLoc);
this.formData.append('addsOneLinkLoc',this.areaAddsOneLoc.addsOneLinkLoc); 
 
 this. gfh.postAddsOnepl(this.it,this.formData).subscribe((res)=>{
    
})
     this.areaAddsOneLoc.addsOneImgLoc="",
     this.areaAddsOneLoc.addsOneLinkLoc=""
}
babycaressareasAddsOneLoc(){
  this. gfh.babycaresAddsOnel(this.it).subscribe((res)=>{
     this.babycaresAddsOneLocl=res
   })
}
babycaressareasAddsOneLocDelete(identities){
  let areaIds=identities._id
  this. gfh.babycaresAddsOneLocDelete(areaIds).subscribe((res)=>{
    
  })
}

// ********************************************************* Adds Areas Two ***********************************************************************

handleFileInputUpdatesTwoLoc($event:any,images){
  this.imageAddsTwoLoc= File = $event.target.files[0];
 }
 

postAddsTwoLoc(){
this.formData.append('addsTwoImgLoc',this.imageAddsTwoLoc);
this.formData.append('addsTwoLinkLoc',this.areaAddsTwoLoc.addsTwoLinkLoc); 
 
 this. gfh.postAddsTwopl(this.it,this.formData).subscribe((res)=>{
     })
     this.areaAddsTwoLoc.addsTwoLinkLoc="",
     this.areaAddsTwoLoc.addsTwoImgLoc=""
}
babycaressareasAddsTwoLoc(){
  this. gfh.babycaresAddsTwol(this.it).subscribe((res)=>{
     this.babycaresAddsTwoLoc=res
   })
}
babycaressareasAddsTwoLocDelete(identities){
  let areaIds=identities._id
  this. gfh.babycaresAddsTwoLocDelete(areaIds).subscribe((res)=>{
    
  })
}


// ********************************************************* Adds Areas Three Loc***********************************************************************

handleFileInputUpdatesThreeLoc($event:any,images){
  this.imageAddsThreeLoc= File = $event.target.files[0];
 }
 

postAddsThreeLoc(){
this.formData.append('addsThreeImgLoc',this.imageAddsThreeLoc);
this.formData.append('addsThreeLinkLoc',this.areaAddsThreeLoc.addsThreeLinkLoc); 
 
 this. gfh.postAddsThreepl(this.it,this.formData).subscribe((res)=>{
     })
     this.areaAddsThreeLoc.addsThreeImgLoc="",
     this.areaAddsThreeLoc.addsThreeLinkLoc=""
}
babycaressareasAddsThreeLoc(){
  this. gfh.babycaresAddsThreel(this.it).subscribe((res)=>{
     this.babycaresAddsThreeLoc=res
   })
}
babycaressareasAddsThreeLocDelete(identities){
  let areaIds=identities._id
  this. gfh.babycaresAddsThreeLocDelete(areaIds).subscribe((res)=>{
  
  })
}


// ********************************************************* Adds Areas Four Loc***********************************************************************

handleFileInputUpdatesFourLoc($event:any,images){
  this.imageAddsFourLoc= File = $event.target.files[0];
 }
 

postAddsFourLoc(){
this.formData.append('addsFourImgLoc',this.imageAddsFourLoc);
this.formData.append('addsFourLinkLoc',this.areaAddsFourLoc.addsFourLinkLoc); 
 
 this. gfh.postAddsFourpl(this.it,this.formData).subscribe((res)=>{
     })
     this.areaAddsFourLoc.addsFourImgLoc="",
     this.areaAddsFourLoc.addsFourLinkLoc=""
}
babycaressareasAddsFourLoc(){
  this. gfh.babycaresAddsFourl(this.it).subscribe((res)=>{
     this.babycaresAddsFoursLoc=res
   })
}
babycaressareasAddsFourLocDelete(identities){
  let areaIds=identities._id
  this. gfh.babycaresAddsFourLocDelete(areaIds).subscribe((res)=>{
    
  })
}


// ********************************************************* Adds Areas Five Loc***********************************************************************

handleFileInputUpdatesFiveLoc($event:any,images){
  this.imageAddsFiveLoc= File = $event.target.files[0];
 }
 

postAddsFiveLoc(){
this.formData.append('addsFiveImgLoc',this.imageAddsFiveLoc);
this.formData.append('addsFiveLinkLoc',this.areaAddsFiveLoc.addsFiveLinkLoc); 
 
 this. gfh.postAddsFivepl(this.it,this.formData).subscribe((res)=>{
     })
     this.areaAddsFiveLoc.addsFiveImgLoc="",
     this.areaAddsFiveLoc.addsFiveLinkLoc=""
}
babycaressareasAddsFiveLoc(){
  this. gfh.babycaresAddsFivel(this.it).subscribe((res)=>{
     this.babycaresAddsFivesLoc=res
   })
}
babycaressareasAddsFiveLocDelete(identities){
  let areaIds=identities._id
  this. gfh.babycaresAddsFiveLocDelete(areaIds).subscribe((res)=>{
    
  })
}

 
}

import { TestBed, inject } from '@angular/core/testing';

import { GeneralrealestateService } from './generalrealestate.service';

describe('GeneralrealestateService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GeneralrealestateService]
    });
  });

  it('should be created', inject([GeneralrealestateService], (service: GeneralrealestateService) => {
    expect(service).toBeTruthy();
  }));
});

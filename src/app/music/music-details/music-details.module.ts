import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../shared/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MatFormFieldModule, MatInputModule } from '@angular/material';
import { MusicDetailsComponent } from './music-details.component';
import { MusicDetailsService } from './music-details.service';

const routes:Routes=[{path:'musicdetails/:_id/:name',component:MusicDetailsComponent},]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    SharedModule
  ],
  declarations: [
    MusicDetailsComponent,

  ],
  providers: [MusicDetailsService],
})
export class MusicDetailsModule { }

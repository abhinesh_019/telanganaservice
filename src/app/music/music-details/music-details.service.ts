import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
 import 'rxjs/add/operator/map';
import { environment } from '../../../environments/environment.prod';

@Injectable()

export class MusicDetailsService {

  
  constructor(private http: HttpClient) { }
  usersDetails(id){
    return   this.http.get(environment.apiUrl +"/musicClientsGetind/"+id);
   }
   serviceTypeDataGet(id){
    return this.http.get(environment.apiUrl + "/musicServicesGet/"+id);
     
  }
 
  serviceDataGet(id){
    return this.http.get(environment.apiUrl + "/musicServicesTwoGet/"+id);
     
  }
  serviceDataGetC(id){
    return this.http.get(environment.apiUrl + "/musicServicesGetCounts/"+id);
     
  }
  clientsUpdatesGet(id){
    
    return this.http.get(environment.apiUrl +"/musicupdatesget/"+id);
  }
  
  clientsUpdatesGetCounts(id){
    
    return this.http.get(environment.apiUrl +"/musicupdatesgetCounts/"+id);
  }
  commentsPoststoClints(id,data){
    
    return this.http.post(environment.apiUrl +"/musicupdatesCommentspost/"+id,data);
  }
  commentsGettoClints(id){
    
    return this.http.get(environment.apiUrl +"/musicupdatesCommentsget/"+id);
  }
  commentsGettoClintsCounts(id){
    
    return this.http.get(environment.apiUrl +"/musicupdatesCommentsgetcounts/"+id);
  }
  replyFromCommentesGet(id){
    
    return this.http.get(environment.apiUrl +"/musicupdatesCommentReplysGet/"+id);
  }

  overallscommentspost(id,data){
    
    return this.http.post(environment.apiUrl +"/musiceuserscomments/"+id,data);
  }
  // ********************************
  fecilitiesGet(id){
    return this.http.get(environment.apiUrl + "/musicfecitiesGet/"+id);
     
  }
  fecilitiesGetC(id){
    return this.http.get(environment.apiUrl + "/musicfecitiesGetCounts/"+id);
     
  }
  CatageriesGets(id){
    return this.http.get(environment.apiUrl + "/musicCatageriesGets/"+id);
     
  }
}



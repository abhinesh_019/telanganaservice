import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MusicComponent } from './music.component';
import { SharedModule } from '../../shared/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MusicService } from './music.service';
import { MusicDetailsModule } from '../music-details/music-details.module';


const routes: Routes = [
  {
    path: '', component: MusicComponent, children:
      [
      { path: 'musicdetails/:_id/:name', loadChildren: 'app/music/music-details/music-details.module#MusicDetailsModule'},
 
      ]
  }]



@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
     SharedModule,
     MusicDetailsModule
       ],
  declarations: [
    MusicComponent
     ],
  providers:[MusicService]
})

export class MusicModule { }

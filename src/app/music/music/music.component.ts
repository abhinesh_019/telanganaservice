import { Component, OnInit } from '@angular/core';
import { MusicModule } from './music.module';
import { MusicService } from './music.service';

@Component({
  selector: 'app-music',
  templateUrl: './music.component.html',
  styleUrls: ['./music.component.css']
})
export class MusicComponent implements OnInit {
 
  public locationsget:any;
public musicsUser:any;
public its:any;
public locationget:any
public furnitureusercount:any
public furnitureusercounttot:any
public locationName:String
public searchString:String;
  searchTerm:String;
  public usercountAllDist:any
  public usercountAllAreas:any
  public usercountAllAreasOver:any
  // *********************************************************
    public areasAll:any
  public selectAreaId:any
  public selectArea:any
public name;
 public usercountAll:any
 public popularareas:any
 
  public usercountMainAreas:any
  // **********************************************************
   // **********************************************************
 
   public imageAddsOne:any
   public babycaresAddsOnes:any
    
   public babycaresAddsFours:any
   public imageAddsFour:any
   public imageAddsThree:any
   public imageAddsTwo:any
   public babycaresAddsThrees:any
   public babycaresAddsTwos:any
   
   
   public babycaresAddsFivesLoc:any
   public imageAddsFiveLoc:any
   public imageAddsFourLoc:any
   public babycaresAddsFoursLoc:any
   public imageAddsThreeLoc:any
   public babycaresAddsThreeLoc:any
   public imageAddsTwoLoc:any
   public babycaresAddsTwoLoc:any
   public imageAddsOneLoc:any
   public babycaresAddsOneLoc:any
  constructor(private music:MusicService) { }

  ngOnInit() {
  
    this.getlocations()
    this.musicUserscountAll()
    this.musicUserscountAllDist()
  this.musicUserscountAllMainAreas() 
   
   
  
  }

getlocations(){
    this.music.locationapi().subscribe((res)=>{
      this.locationsget=res;
        var id =this.locationsget[0];
        this.locations(id)
    })
  
  }
  
  
  locations(get?){
    this.locationName=get.locations
    this.its=get._id
    this.locationget=get.locations
      this.allAreas()
      this.babycaressareasAddsOneLoc()
      this.babycaressareasAddsTwoLoc()
      this.babycaressareasAddsThreeLoc()
      this.babycaressareasAddsFourLoc()
      this.babycaressareasAddsFiveLoc()
     }
  allAreas(){
     
    this.music.areaapi(this.its).subscribe((res)=>{
      this.areasAll=res
      var id =this.areasAll[0];
        this.selectedAreas(id)
      
    })
   
  }

  selectedAreas(result){
    this.selectAreaId=result._id
    this.selectArea=result.area
      this.musicUsers()
   this.musicUserscountAllAreas()
   this.musicUserscountAllAreasover()
   this.babycaressareasAddsOnea()
this.babycaressareasAddsTwoa()
this.babycaressareasAddsThreea()
this.babycaressareasAddsFoura()
   }
 
  musicUsers(){
    var data:any = {}
    if(this.searchString){
      data.search=this.searchString
     }
    this.music.musicUsers(this.selectAreaId,data).subscribe((res)=>{
      this.musicsUser=res
     })
    
  }

searchFilter(){
  this.musicUsers()
}
   musicUserscountAll(){
    this.music.musicUserscountsAll().subscribe((res)=>{
      this.usercountAll=res
     })
   }

   musicUserscountAllMainAreas(){
    this.music.musicUserscountAllMainAreas().subscribe((res)=>{
      this.usercountMainAreas=res
      })
   }
   


   musicUserscountAllDist(){
    this.music.musicUserscountAllDist().subscribe((res)=>{
      this.usercountAllDist=res
      })
   }

   musicUserscountAllAreas(){
    this.music.musicUserscountAllAreas(this.selectAreaId).subscribe((res)=>{
      this.usercountAllAreas=res
      })
   }
   musicUserscountAllAreasover(){
    this.music.musicUserscountAllAreasover(this.selectAreaId).subscribe((res)=>{
      this.usercountAllAreasOver=res
       })
   }
 
// ****************************************************

// ****************************************************

babycaressareasAddsOnea(){
  this.music.babycaresAddsOnea(this.selectAreaId).subscribe((res)=>{
     this.babycaresAddsOnes=res
 
     
   })
}
babycaressareasAddsTwoa(){
  this.music.babycaresAddsTwoa(this.selectAreaId).subscribe((res)=>{
     this.babycaresAddsTwos=res
  
   })
}
babycaressareasAddsThreea(){
  this.music.babycaresAddsThreea(this.selectAreaId).subscribe((res)=>{
     this.babycaresAddsThrees=res
  
   })
}

babycaressareasAddsFoura(){
  this.music.babycaresAddsFoura(this.selectAreaId).subscribe((res)=>{
     this.babycaresAddsFours=res

   })
}

babycaressareasAddsOneLoc(){
  this.music.babycaresAddsOnel(this.its).subscribe((res)=>{
     this.babycaresAddsOneLoc=res 
   })
}
babycaressareasAddsTwoLoc(){
  this.music.babycaresAddsTwol(this.its).subscribe((res)=>{
     this.babycaresAddsTwoLoc=res
    
   })
}
babycaressareasAddsThreeLoc(){
  this.music.babycaresAddsThreel(this.its).subscribe((res)=>{
     this.babycaresAddsThreeLoc=res
     
   })
}
babycaressareasAddsFourLoc(){
  this.music.babycaresAddsFourl(this.its).subscribe((res)=>{
     this.babycaresAddsFoursLoc=res
     
   })
}
babycaressareasAddsFiveLoc(){
  this.music.babycaresAddsFivel(this.its).subscribe((res)=>{
     this.babycaresAddsFivesLoc=res
    
   })
}

}

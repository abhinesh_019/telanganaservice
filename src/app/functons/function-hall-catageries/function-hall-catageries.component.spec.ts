import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FunctionHallCatageriesComponent } from './function-hall-catageries.component';

describe('FunctionHallCatageriesComponent', () => {
  let component: FunctionHallCatageriesComponent;
  let fixture: ComponentFixture<FunctionHallCatageriesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FunctionHallCatageriesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FunctionHallCatageriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

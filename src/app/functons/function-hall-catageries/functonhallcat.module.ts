import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {HttpClientModule} from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
 import { HalldetailsModule } from '../halldetails/halldetails.module';
import { SharedModule } from '../../shared/shared.module';
import { FunctonhallsModule } from '../functonhalls/functonhalls.module';
import { FunctionHallCatageriesComponent } from './function-hall-catageries.component';
import { FunctonhallcatService } from './functonhallcat.service';
 
const routes: Routes = [
  {
    path: '', component: FunctionHallCatageriesComponent, children:
      [
      { path: 'hallSelect/:_id/:name', loadChildren: 'app/functons/functonhalls/functonhalls.module#FunctonhallsModule'},
      { path: 'halldetails/:_id/:name', loadChildren: 'app/functons/halldetails/halldetails.module#HalldetailsModule'},

       ]
  }]
 
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
    HalldetailsModule,
    FunctonhallsModule,
    SharedModule
   ],
   
  declarations: [
    FunctionHallCatageriesComponent,
     
  ],
  providers:[FunctonhallcatService]
})
export class FunctonhallcatModule { }

import {MatInputModule} from '@angular/material/input';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material';

import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { HalldetailsComponent } from './halldetails.component';
import { HallDetailsService } from './hall-details.service';



const routes:Routes=[
  {path:'halldetails/:_id/:name',component:HalldetailsComponent},

]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    SharedModule
    

  ],
  declarations: [
    HalldetailsComponent,

  ],
  providers: [HallDetailsService],
})
export class HalldetailsModule { }

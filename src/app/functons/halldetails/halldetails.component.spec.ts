import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HalldetailsComponent } from './halldetails.component';

describe('HalldetailsComponent', () => {
  let component: HalldetailsComponent;
  let fixture: ComponentFixture<HalldetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HalldetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HalldetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

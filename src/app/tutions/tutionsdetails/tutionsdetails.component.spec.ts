import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TutionsdetailsComponent } from './tutionsdetails.component';

describe('TutionsdetailsComponent', () => {
  let component: TutionsdetailsComponent;
  let fixture: ComponentFixture<TutionsdetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TutionsdetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TutionsdetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

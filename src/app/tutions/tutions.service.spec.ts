import { TestBed, inject } from '@angular/core/testing';

import { TutionsService } from './tutions.service';

describe('TutionsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TutionsService]
    });
  });

  it('should be created', inject([TutionsService], (service: TutionsService) => {
    expect(service).toBeTruthy();
  }));
});

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AnimalsService } from '../animals.service';

@Component({
  selector: 'app-animalsdetails',
  templateUrl: './animalsdetails.component.html',
  styleUrls: ['./animalsdetails.component.css']
})
export class AnimalsdetailsComponent implements OnInit {
public animalsuser:any
public ids:any
public animalsTypes:any
public animalsNames:any
public animalsId:any
public animalsTypesDes:any
public selectAnimals:any
public animalsTypesDesInd:any
public clientUpdatesCount:any
public clientUpdates:any
public commentspostsId:any
public getUsersComments:any
public getUsersCommentsCounts:any
public openshareid:any
public viewcommentsid:any
public replyid:any
public replies:any
public animalsTypesCnts:any
public clientUpdatesCnts:any

public share:boolean=false
public comments:boolean=false
public viewcomments:boolean=false
public replyfrom:boolean=false 
public plantsFertilizersGet:any
public PlantsEquipmentsGet:any

postComments={
  "descriptions":""
}
commentspost={
  "name":"",
  "email":"",
  "contactNo":"",
  "leaveComment":"",
    }
   

  constructor(private route:ActivatedRoute,private animals:AnimalsService) { }

  ngOnInit() {
    this.individualdata()
    this.animalsLists()
    this.clientsUpdates()
    this.clientsUpdatesCount()
    this.clientsUpdatesCnts()
  }
  individualdata(){
    this.ids=this.route.snapshot.params['_id'];

this.usersDetails()
}

usersDetails(){ 
this.animals.animalsusersonly(this.ids).subscribe((res)=>{
this.animalsuser=res
})
}

 
animalsLists(){   
  this.animals.animalsTypes(this.ids).subscribe((res)=>{
    this.animalsTypes=res
    var id =this.animalsTypes[0];
     this.selectedAnimals(id)
      
   })
 }


selectedAnimals(result){
this.animalsId=result._id
this.animalsNames=result.animalsTypes
this.animalsListDetails()
this.animalsListsTypeCnt()
this.plantsFertilizersGets()
this.PlantsEquipmentsGets()
}
animalsListsTypeCnt(){   
  this.animals.animalsListsTypeCnt(this.animalsId).subscribe((res)=>{
    this.animalsTypesCnts=res
    })
 }
animalsListDetails(){   
  this.animals.animalsListDetail(this.animalsId).subscribe((res)=>{
    this.animalsTypesDes=res
      
     console.log(res);
     
   })
 }

 selectAnimal(selects){
this.selectAnimals=selects._id
this.animalsListDetailsIndividuals()
 }
 animalsListDetailsIndividuals(){   
  this.animals.animalsListDetailIndv(this.selectAnimals).subscribe((res)=>{
    this.animalsTypesDesInd=res
      
     console.log(res);
     
   })
 }
// ******************************************updates present ***************************************
shareClick(up){
  this.share=!this.share
  this.comments=false
  this.viewcomments=false
  this.replyfrom=false
  this.openshareid=up._id
}
commentsClick(up){
  this.comments=!this.comments
  this.share=false
  this.viewcomments=false
  this.commentspostsId=up._id
   this.commentsGettoClints()
   this.commentsGettoClintsCounts()
 }

viewCommentsClick(up){
  this.viewcomments=!this.viewcomments
  this.share=false
  this.viewcommentsid=up._id
}
replyFromm(comms){
  this.replyfrom=!this.replyfrom
  this.share=false
  this.replyid=comms._id
  this.replyFromCommentesGet()
}
 clientsUpdates(){   
  this.animals.clientsUpdatesGet(this.ids).subscribe((res)=>{
    this.clientUpdates=res
   })
 }

 clientsUpdatesCnts(){   
  this.animals.clientsUpdatesCnts(this.ids).subscribe((res)=>{
    this.clientUpdatesCnts=res
   })
 }
 clientsUpdatesCount(){   
  this.animals.clientsUpdatesGetCounts(this.ids).subscribe((res)=>{
    this.clientUpdatesCount=res
   })
 }

 commentsPoststoClints(){   
  this.animals.commentsPoststoClints(this.commentspostsId,this.postComments).subscribe((res)=>{
     })
     this.postComments.descriptions=""
 }
 commentsGettoClints(){   
  this.animals.commentsGettoClints(this.commentspostsId).subscribe((res)=>{
    this.getUsersComments=res
   })
 }

 commentsGettoClintsCounts(){   
  this.animals.commentsGettoClintsCounts(this.commentspostsId).subscribe((res)=>{
    this.getUsersCommentsCounts=res
    })
 }

 replyFromCommentesGet(){   
  this.animals.replyFromCommentesGet(this.replyid).subscribe((res)=>{
    this.replies=res
    })
 }


  // ***********0verallcomments************
  overallcomment(){
       
     
    this.animals.overallscommentspost(this.ids,this.commentspost).subscribe((res)=>{
     
     this.commentspost.name="",
     this.commentspost.contactNo="",
     this.commentspost.email="",
     this.commentspost.leaveComment=""
    })
  }


  plantsFertilizersGets(){ 
    this.animals.plantsFertilizersGets(this.animalsId).subscribe((res)=>{
    this.plantsFertilizersGet=res
    console.log(res);
     })
    }

    PlantsEquipmentsGets(){ 
      this.animals.PlantsEquipmentsGets(this.animalsId).subscribe((res)=>{
      this.PlantsEquipmentsGet=res
        })
      }
}

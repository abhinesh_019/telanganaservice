import {MatInputModule} from '@angular/material/input';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material';

import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { AnimalsdetailsComponent } from './animalsdetails.component';
import { AnimalsService } from '../animals.service';
 



const routes:Routes=[{path:'animalsdetails/:_id/:name',component:AnimalsdetailsComponent},]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    SharedModule
    

  ],
  declarations: [ AnimalsdetailsComponent],
  providers: [AnimalsService],
})
 
export class AnimalsdetailsModule { }

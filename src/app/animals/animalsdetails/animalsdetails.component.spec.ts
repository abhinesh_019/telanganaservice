import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnimalsdetailsComponent } from './animalsdetails.component';

describe('AnimalsdetailsComponent', () => {
  let component: AnimalsdetailsComponent;
  let fixture: ComponentFixture<AnimalsdetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnimalsdetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnimalsdetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

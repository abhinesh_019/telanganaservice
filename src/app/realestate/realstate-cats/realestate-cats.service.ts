import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
 import 'rxjs/add/operator/map';
import { environment } from '../../../environments/environment.prod';

@Injectable()

export class RealestateCatsService {

  
  constructor(private http: HttpClient) { }
  locationapi( ){
    return   this.http.get(environment.apiUrl +"/realestateLocations");
   }
   areaapi(id){
    return   this.http.get(environment.apiUrl +"/realestateAreas/"+id);
   }


   realestateUsersall(id,data){
    
    return this.http.get(environment.apiUrl +"/realestateClientsget/"+id,{params:data});
  }

  ClientsAllCounts(){
    
    return this.http.get(environment.apiUrl +"/realestateClientsAllCount");
  }
  clientsAreaId(id){
    
    return this.http.get(environment.apiUrl +"/realestateClientsAreas/"+id);
  }
  clientsAreaIdind(id){
    
    return this.http.get(environment.apiUrl +"/realestateClientsCount/"+id);
  }
  locationsCountsAll(){
    
    return this.http.get(environment.apiUrl +"/realestateClientsAllCountLocations");
  }
  babycaresAddsOnea(id){

    return this.http.get(environment.apiUrl +"/realestateAddsOneGeta/"+id);
  }
  babycaresAddsTwoa(id){
    
    return this.http.get(environment.apiUrl +"/realestateAddsTwoGeta/"+id);
  }
  babycaresAddsThreea(id){
    
    return this.http.get(environment.apiUrl +"/realestateAddsThreeGeta/"+id);
  }
  babycaresAddsFoura(id){
    
    return this.http.get(environment.apiUrl +"/realestateAddsFourGeta/"+id);
  }
   
  babycaresAddsOnel(id){
    
    return this.http.get(environment.apiUrl +"/realestateAddsOneLocGet/"+id);
  }
  babycaresAddsTwol(id){
    
    return this.http.get(environment.apiUrl +"/realestateAddsTwoLocGet/"+id);
  }
  babycaresAddsThreel(id){
    
    return this.http.get(environment.apiUrl +"/realestateAddsThreeGetLoc/"+id);
  }
  babycaresAddsFourl(id){
    
    return this.http.get(environment.apiUrl +"/realestateAddsFourLocGet/"+id);
  }

  babycaresAddsFivel(id){
    
    return this.http.get(environment.apiUrl +"/realestateAddsFiveLocGet/"+id);
  }
  }


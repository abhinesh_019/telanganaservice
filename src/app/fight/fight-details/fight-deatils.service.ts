import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
 import 'rxjs/add/operator/map';
import { environment } from '../../../environments/environment.prod';
    

@Injectable()
export class FightDeatilsService {
  constructor(private http: HttpClient) { }
  usersDetails(id){
    
    return this.http.get(environment.apiUrl +"/fightsClientsgetId/"+id);
  }  
  fightServices(id){
    return this.http.get(environment.apiUrl + "/fightServicesget/"+id);
     
  }
  fightServicesc(id){
    return this.http.get(environment.apiUrl + "/fightServicesCntget/"+id);
   }
   fecilities(id){
    return this.http.get(environment.apiUrl + "/fightsfecitiesGet/"+id);
   }
   fecilitiesc(id){
    return this.http.get(environment.apiUrl + "/fightsfecitiesGetCounts/"+id);
   }
   clientsUpdatesGet(id){
    
    return this.http.get(environment.apiUrl +"/fightsupdatesget/"+id);
  }
  
  clientsUpdatesGetCounts(id){
    
    return this.http.get(environment.apiUrl +"/fightsupdatesgetCounts/"+id);
  }
  commentsPoststoClints(id,data){
    
    return this.http.post(environment.apiUrl +"/fightsupdatesCommentspost/"+id,data);
  }
  commentsGettoClints(id){
    
    return this.http.get(environment.apiUrl +"/fightsupdatesCommentsget/"+id);
  }
  commentsGettoClintsCounts(id){
    
    return this.http.get(environment.apiUrl +"/fightsupdatesCommentsgetcounts/"+id);
  }
  replyFromCommentesGet(id){
    
    return this.http.get(environment.apiUrl +"/fightsupdatesCommentReplysGet/"+id);
  }

  overallscommentspost(id,data){
    
    return this.http.post(environment.apiUrl +"/fightseuserscomments/"+id,data);
  }
  // ********************************

}
import {MatInputModule} from '@angular/material/input';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material';
import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { FightDetailsComponent } from './fight-details.component';
import { FightDeatilsService } from './fight-deatils.service';
 
const routes:Routes=[{path:'fightsDetails/:_id/:name',component:FightDetailsComponent},]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    SharedModule
    

  ],
  declarations: [
    FightDetailsComponent,

  ],
  providers: [FightDeatilsService],
})
export class FightDeatilsModule { }

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
 import 'rxjs/add/operator/map';
import { environment } from '../../../environments/environment.prod';
    

@Injectable()
export class FightService {

  constructor(private http: HttpClient) { }
  catapigetdetails(){
    
    return this.http.get(environment.apiUrl +"/fightsCategories");
  }  
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
 import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { FightComponent } from './fight.component';
import { FightService } from './fight.service';
import { FightusersModule } from '../fightusers/fightusers.module';
import { FightDeatilsModule } from '../fight-details/fight-deatils.module';
 
const routes: Routes = [
  {
    path: '', component: FightComponent, children:
      [
      { path: 'fightusers/:_id/:name', loadChildren: 'app/fight/fightusers/fightusers.module#FightusersModule'},
      { path: 'fightsDetails/:_id/:name', loadChildren: 'app/fight/fight-details/fight-deatils.module#FightDeatilsModule'},

      ]
  }]



@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
     SharedModule,
     FightusersModule,
     FightDeatilsModule
  ],
  declarations: [
    FightComponent,
  ],
  providers:[FightService]
})

export class FightModule { }

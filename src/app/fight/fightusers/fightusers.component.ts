import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FightusersService } from './fightusers.service';

@Component({
  selector: 'app-fightusers',
  templateUrl: './fightusers.component.html',
  styleUrls: ['./fightusers.component.css']
})
export class FightusersComponent implements OnInit {

  public ids:any
  public catdeatils:any
  public selectAreaId:any
  public selectArea:any
  public locationsgets:any
  public locationName:any
  public idss:any
  public areasAll:any
  public catAll:any
  public locationsAlls:any
  public searchString:any
  public clients:any
  public countsClientsArea:any
  public clinetsSubAreas:any
  // public ids:any
  // public ids:any

  public imageAddsOne:any
  public babycaresAddsOnes:any
   
  public babycaresAddsFours:any
  public imageAddsFour:any
  public imageAddsThree:any
  public imageAddsTwo:any
  public babycaresAddsThrees:any
  public babycaresAddsTwos:any
  
  
  public babycaresAddsFivesLoc:any
  public imageAddsFiveLoc:any
  public imageAddsFourLoc:any
  public babycaresAddsFoursLoc:any
  public imageAddsThreeLoc:any
  public babycaresAddsThreeLoc:any
  public imageAddsTwoLoc:any
  public babycaresAddsTwoLoc:any
  public imageAddsOneLoc:any
  public babycaresAddsOneLoc:any
  
  constructor(private fights:FightusersService,private route:ActivatedRoute) { }
  

  ngOnInit() {
    this.individualdata()
   this.usersDetails()
   this.getlocations()
   this.catageriesAllCounts()
   this.locationsAll()
  }
  individualdata(){
    this.ids=this.route.snapshot.params['_id'];
 }
usersDetails(){ 
  this.fights.individualcat(this.ids).subscribe((res)=>{
    this.catdeatils=res
      })
}
   // *********************** locations ****************
      
   getlocations(){
    this.fights.locationsget(this.ids).subscribe((res)=>{
    this.locationsgets=res
     var id =this.locationsgets[0];
     this.locations(id)
        
    })  }


    locations(get?){
      this.locationName=get.locations
      this.idss=get._id
       this.allAreas()
       this.babycaressareasAddsOneLoc()
       this.babycaressareasAddsTwoLoc()
       this.babycaressareasAddsThreeLoc()
       this.babycaressareasAddsFourLoc()
       this.babycaressareasAddsFiveLoc()
    }

// ************************************************************************************************************************************

//  **********************************area *************************************

allAreas(){
 
this.fights.areaapi(this.idss).subscribe((res)=>{
this.areasAll=res
var id =this.areasAll[0];
this.selectedAreas(id)

})

}

selectedAreas(result){
this.selectAreaId=result._id
this.selectArea=result.area
 this.clientDetails()
 this.clientsCounts()
 this.clientsSubAreasCnt()


 this.babycaressareasAddsOnea()
 this.babycaressareasAddsTwoa()
 this.babycaressareasAddsThreea()
 this.babycaressareasAddsFoura()
}

// ************************************************************************
catageriesAllCounts(){
  this.fights.catageriesAllCounts().subscribe((res)=>{
  this.catAll=res
   
      
  })  }
  locationsAll(){
    this.fights.locationsAll().subscribe((res)=>{
    this.locationsAlls=res
     
        
    })  }
  
    clientDetails(){ 
      var data:any =  {}
      if(this.searchString){
        data.search=this.searchString
        }
      this.fights.Client(this.selectAreaId,data).subscribe((res)=>{
      this.clients=res
       })
      }
      searchFilter(){
        this.clientDetails()
      }
      clientsCounts(){
        this.fights.clientsCoounts(this.selectAreaId).subscribe((res)=>{
        this.countsClientsArea=res
         
            
        })  }
        clientsSubAreasCnt(){
          this.fights.clientsSubAreasCnt(this.selectAreaId).subscribe((res)=>{
          this.clinetsSubAreas=res
           
              
          })  }

// ****************************************************

babycaressareasAddsOnea(){
  this.fights.babycaresAddsOnea(this.selectAreaId).subscribe((res)=>{
     this.babycaresAddsOnes=res
 
     
   })
}
babycaressareasAddsTwoa(){
  this.fights.babycaresAddsTwoa(this.selectAreaId).subscribe((res)=>{
     this.babycaresAddsTwos=res
  
   })
}
babycaressareasAddsThreea(){
  this.fights.babycaresAddsThreea(this.selectAreaId).subscribe((res)=>{
     this.babycaresAddsThrees=res
  
   })
}

babycaressareasAddsFoura(){
  this.fights.babycaresAddsFoura(this.selectAreaId).subscribe((res)=>{
     this.babycaresAddsFours=res

   })
}

babycaressareasAddsOneLoc(){
  this.fights.babycaresAddsOnel(this.idss).subscribe((res)=>{
     this.babycaresAddsOneLoc=res 
   })
}
babycaressareasAddsTwoLoc(){
  this.fights.babycaresAddsTwol(this.idss).subscribe((res)=>{
     this.babycaresAddsTwoLoc=res
    
   })
}
babycaressareasAddsThreeLoc(){
  this.fights.babycaresAddsThreel(this.idss).subscribe((res)=>{
     this.babycaresAddsThreeLoc=res
     
   })
}
babycaressareasAddsFourLoc(){
  this.fights.babycaresAddsFourl(this.idss).subscribe((res)=>{
     this.babycaresAddsFoursLoc=res
     
   })
}
babycaressareasAddsFiveLoc(){
  this.fights.babycaresAddsFivel(this.idss).subscribe((res)=>{
     this.babycaresAddsFivesLoc=res
    
   })
}

}

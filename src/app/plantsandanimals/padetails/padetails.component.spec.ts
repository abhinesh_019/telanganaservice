import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PadetailsComponent } from './padetails.component';

describe('PadetailsComponent', () => {
  let component: PadetailsComponent;
  let fixture: ComponentFixture<PadetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PadetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PadetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { PlantsService } from '../plants.service';

@Component({
  selector: 'app-plants',
  templateUrl: './plants.component.html',
  styleUrls: ['./plants.component.css']
})
export class PlantsComponent implements OnInit {

  public area:any
  public searchString:String;
  searchTerm:String;
  public locationsget:any
  public locations:any
  public locationName:any
public its:any
public locationget:any
public areasAll:any
public selectAreaId:any
public selectArea:any
public plantsuser:any
public plantsusercount:any
public popularareas:any
public aplantsusercount:any
public plantsusercountLocations:any
public plantsusercounttot:any
 
 // **********************************************************
 
 public imageAddsOne:any
 public babycaresAddsOnes:any
  
 public babycaresAddsFours:any
 public imageAddsFour:any
 public imageAddsThree:any
 public imageAddsTwo:any
 public babycaresAddsThrees:any
 public babycaresAddsTwos:any
 
 
 public babycaresAddsFivesLoc:any
 public imageAddsFiveLoc:any
 public imageAddsFourLoc:any
 public babycaresAddsFoursLoc:any
 public imageAddsThreeLoc:any
 public babycaresAddsThreeLoc:any
 public imageAddsTwoLoc:any
 public babycaresAddsTwoLoc:any
 public imageAddsOneLoc:any
 public babycaresAddsOneLoc:any

  constructor(private router:Router,private route:ActivatedRoute,private plants:PlantsService) { }

  ngOnInit() {
    this.getlocations()
    this.plantsuserscounttot()
    this.plantsuserscountLocations()
  }

    
  // locations*****************
getlocations(){
  this.plants.locationapi().subscribe((res)=>{
    this.locationsget=res;
   var id =this.locationsget[0];
     this.location(id)
   })}

  location(get?){
    this.locationName=get.locations
    this.its=get._id
    this.allAreas()
    this.babycaressareasAddsOneLoc()
    this.babycaressareasAddsTwoLoc()
    this.babycaressareasAddsThreeLoc()
    this.babycaressareasAddsFourLoc()
    this.babycaressareasAddsFiveLoc()
   }
 
   allAreas(){
     this.plants.areaapi(this.its).subscribe((res)=>{
      this.areasAll=res
      var id =this.areasAll[0];
       this.selectedAre(id)
     })
   }

selectedAre(result){
this.selectAreaId=result._id
this.selectArea=result.area
this.plantsuserscount()   
this.plantsusers()
this.plantsareascount()  
this.babycaressareasAddsOnea()
this.babycaressareasAddsTwoa()
this.babycaressareasAddsThreea()
this.babycaressareasAddsFoura()
  }


 plantsusers(){
  var data:any =  {}
  if(this.searchString){
    data.search=this.searchString
    }
  this.plants.plantsusers(this.selectAreaId,data).subscribe((res)=>{
    this.plantsuser=res
    console.log(res);
    
   })
 }

searchFilter(){
this.plantsusers()
}

// ***************************************************************

plantsuserscount(){
  this.plants.plantsuserscounts(this.selectAreaId).subscribe((res)=>{
    this.plantsusercount=res
   })
 }
 plantsuserscountLocations(){
  this.plants.plantsusersForLocations().subscribe((res)=>{
    this.plantsusercountLocations=res
   })
 }
// ***************************************************************
plantsuserscounttot(){
  this.plants.plantsuserscountstot().subscribe((res)=>{
    this.plantsusercounttot=res
 })
 }

 plantsareascount(){
   this.plants.plantsuserscountsarea(this.selectAreaId).subscribe((res)=>{
     this.popularareas=res
   })
 }
 
// ****************************************************

babycaressareasAddsOnea(){
  this.plants.babycaresAddsOnea(this.selectAreaId).subscribe((res)=>{
     this.babycaresAddsOnes=res
 
     
   })
}
babycaressareasAddsTwoa(){
  this.plants.babycaresAddsTwoa(this.selectAreaId).subscribe((res)=>{
     this.babycaresAddsTwos=res
  
   })
}
babycaressareasAddsThreea(){
  this.plants.babycaresAddsThreea(this.selectAreaId).subscribe((res)=>{
     this.babycaresAddsThrees=res
  
   })
}

babycaressareasAddsFoura(){
  this.plants.babycaresAddsFoura(this.selectAreaId).subscribe((res)=>{
     this.babycaresAddsFours=res

   })
}

babycaressareasAddsOneLoc(){
  this.plants.babycaresAddsOnel(this.its).subscribe((res)=>{
     this.babycaresAddsOneLoc=res 
   })
}
babycaressareasAddsTwoLoc(){
  this.plants.babycaresAddsTwol(this.its).subscribe((res)=>{
     this.babycaresAddsTwoLoc=res
    
   })
}
babycaressareasAddsThreeLoc(){
  this.plants.babycaresAddsThreel(this.its).subscribe((res)=>{
     this.babycaresAddsThreeLoc=res
     
   })
}
babycaressareasAddsFourLoc(){
  this.plants.babycaresAddsFourl(this.its).subscribe((res)=>{
     this.babycaresAddsFoursLoc=res
     
   })
}
babycaressareasAddsFiveLoc(){
  this.plants.babycaresAddsFivel(this.its).subscribe((res)=>{
     this.babycaresAddsFivesLoc=res
    
   })
}

}

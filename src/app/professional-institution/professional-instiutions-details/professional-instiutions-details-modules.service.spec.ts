import { TestBed, inject } from '@angular/core/testing';

import { ProfessionalInstiutionsDetailsModulesService } from './professional-instiutions-details-modules.service';

describe('ProfessionalInstiutionsDetailsModulesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ProfessionalInstiutionsDetailsModulesService]
    });
  });

  it('should be created', inject([ProfessionalInstiutionsDetailsModulesService], (service: ProfessionalInstiutionsDetailsModulesService) => {
    expect(service).toBeTruthy();
  }));
});

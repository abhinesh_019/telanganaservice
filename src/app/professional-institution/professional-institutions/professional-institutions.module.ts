import {MatInputModule} from '@angular/material/input';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material';
import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { ProfessionalInstitutionsComponent } from './professional-institutions.component';
import { ProfessionalInstitutionsService } from './professional-institutions.service';
  

const routes:Routes=[
  {path:'professionalInstituions/:_id/:name',component:ProfessionalInstitutionsComponent},

]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    SharedModule
    

  ],
  declarations: [
    ProfessionalInstitutionsComponent,

  ],
  providers: [ProfessionalInstitutionsService],
})
export class ProfessionalInstitutionsModule { }

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfessionalInstitutionsComponent } from './professional-institutions.component';

describe('ProfessionalInstitutionsComponent', () => {
  let component: ProfessionalInstitutionsComponent;
  let fixture: ComponentFixture<ProfessionalInstitutionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfessionalInstitutionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfessionalInstitutionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

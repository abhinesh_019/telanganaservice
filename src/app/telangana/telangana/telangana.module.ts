import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {HttpClientModule} from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TelanganaComponent } from './telangana.component';
import { TelanganaService } from './telangana.service';

const routes: Routes = [
  {
    path: 'telangana', component: TelanganaComponent, children:
      [
      
      ]
  }]



@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
    HttpClientModule
  ],
  declarations: [
    TelanganaComponent,
    
  ],
  providers: [TelanganaService],

})
export class TelanganaModule { }

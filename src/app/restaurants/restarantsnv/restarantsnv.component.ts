import { Component, OnInit } from '@angular/core';
import { RestarantsnvService } from './restarantsnv.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-restarantsnv',
  templateUrl: './restarantsnv.component.html',
  styleUrls: ['./restarantsnv.component.css']
})
export class RestarantsnvComponent implements OnInit {

  public selectDishId :any;
  public selectDishName :any
  public dishTypeGets :any
  public selectFoodItemsId :any
  public selectFoodItemsName :any
  public selectItemsTwoGets :any
  public masterIds :any
  public masterName :any
  public mainSelectItemGets :any
  public servicesOnes :any
  public ids :any
  // public selectDishId :any
  // public selectDishId :any
  // public selectDishId :any
  // public selectDishId :any
  // public selectDishId :any
  // public selectDishId :any
  // public selectDishId :any
  // public selectDishId :any
  
  
  
  constructor(private res:RestarantsnvService,private route:ActivatedRoute) { }
  
    ngOnInit() {
      this.individualdata()
    }
    individualdata(){
      this.ids=this.route.snapshot.params['_id'];
  this.servicesOne()
  }
    servicesOne(){ 
      this.res.servicesOne(this.ids).subscribe((res)=>{
      this.servicesOnes=res
      console.log(this.servicesOnes);
      
      var vsd=this.servicesOnes[0]
       })
      }
    onSelectDish(up){
      this.selectDishId=up._id
      this.selectDishName=up.foodType1
      console.log(this.selectDishId);
    this.dishTypeGet()
    }
     dishTypeGet(){ 
    this.res.dishTypeGet(this.selectDishId).subscribe((res)=>{ 
    this.dishTypeGets=res
    // var disy=this.dishTypeGets[0]
    // this.onSelectItemTypes(disy)
    })
    }
    onSelectItemTypes(up){
      this.selectFoodItemsId=up._id
      this.selectFoodItemsName=up.foodDish
      console.log(this.selectFoodItemsName);
       
    this.selectItemsTwoGet()
    }
    selectItemsTwoGet(){ 
      this.res.selectItemsTwoGet(this.selectFoodItemsId).subscribe((res)=>{
      this.selectItemsTwoGets=res
        })
      }
      onClickMasteritems(up){
        this.masterIds=up._id
        this.masterName=up.foodType1
        console.log(this.masterName);
        this.mainSelectItemGet()
      }
    
      mainSelectItemGet(){ 
        this.res.mainSelectItemGet(this.masterIds).subscribe((res)=>{
        this.mainSelectItemGets=res
          })
        }

}

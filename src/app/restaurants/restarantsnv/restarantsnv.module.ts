import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule, MatInputModule } from '@angular/material';
import { SharedModule } from '../../shared/shared.module';
import { RestarantsnvComponent } from './restarantsnv.component';
import { RestarantsnvService } from './restarantsnv.service';


const routes:Routes=[{path:'restaurantsNonVeg/:_id/:name',component:RestarantsnvComponent},]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    SharedModule
    

  ],
  declarations: [
    RestarantsnvComponent,
  ],
  providers: [RestarantsnvService],
})
 
export class RestarantsnvModule { }

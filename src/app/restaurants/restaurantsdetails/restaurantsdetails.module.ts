import {MatInputModule} from '@angular/material/input';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material';
import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { RestaurantsdetailsComponent } from './restaurantsdetails.component';
import { RestaurantsdetailsService } from './restaurantsdetails.service';



const routes:Routes=[{path:'restaurantsdetails/:_id/:name',component:RestaurantsdetailsComponent},]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    SharedModule
    

  ],
  declarations: [
    RestaurantsdetailsComponent,
  ],
  providers: [RestaurantsdetailsService],
})
 
 
export class RestaurantsdetailsModule { }

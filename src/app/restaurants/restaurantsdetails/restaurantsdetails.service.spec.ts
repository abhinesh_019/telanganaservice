import { TestBed, inject } from '@angular/core/testing';

import { RestaurantsdetailsService } from './restaurantsdetails.service';

describe('RestaurantsdetailsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RestaurantsdetailsService]
    });
  });

  it('should be created', inject([RestaurantsdetailsService], (service: RestaurantsdetailsService) => {
    expect(service).toBeTruthy();
  }));
});

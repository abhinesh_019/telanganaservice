import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
 import 'rxjs/add/operator/map';
import { environment } from '../../../environments/environment.prod';
     
@Injectable()
export class RestaurantsdetailsService {
  constructor(private http: HttpClient) { }

  usersDetails(id){
    
    return this.http.get(environment.apiUrl +"/restaurantsClientsGetind/"+id);
  } 
  servicesOne(id){
    
    return this.http.get(environment.apiUrl +"/restaurantsFoodType/"+id);
  } 
  dishTypeGet(id){
    
    return this.http.get(environment.apiUrl +"/restaurantsFoodTypeDish/"+id);
  } 
  selectItemsTwoGet(id){
    
    return this.http.get(environment.apiUrl +"/restaurantsFoodTypeTwo/"+id);
  }
  mainSelectItemGet(id){
    
    return this.http.get(environment.apiUrl +"/restaurantsServicesGetOne/"+id);
  }

  clientsUpdatesGet(id){
    
    return this.http.get(environment.apiUrl +"/restaurantsupdatesget/"+id);
  }
  
  clientsUpdatesGetCounts(id){
    
    return this.http.get(environment.apiUrl +"/restaurantsupdatesgetCounts/"+id);
  }
  commentsPoststoClints(id,data){
    
    return this.http.post(environment.apiUrl +"/restaurantsupdatesCommentspost/"+id,data);
  }
  commentsGettoClints(id){
    
    return this.http.get(environment.apiUrl +"/restaurantsupdatesCommentsget/"+id);
  }
  commentsGettoClintsCounts(id){
    
    return this.http.get(environment.apiUrl +"/restaurantsupdatesCommentsgetcounts/"+id);
  }
  replyFromCommentesGet(id){
    
    return this.http.get(environment.apiUrl +"/restaurantsupdatesCommentReplysGet/"+id);
  }

  overallscommentspost(id,data){
    
    return this.http.post(environment.apiUrl +"/restaurantsuserscomments/"+id,data);
  }
}

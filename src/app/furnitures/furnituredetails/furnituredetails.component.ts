import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FurnituresService } from '../furnitures.service';

@Component({
  selector: 'app-furnituredetails',
  templateUrl: './furnituredetails.component.html',
  styleUrls: ['./furnituredetails.component.css']
})
export class FurnituredetailsComponent implements OnInit {
public ids:any
public searchString:any
public furnitureuser:any
public image:any
public updatesId:any
public show:any
public shows:any
public showes:any
public commentsUpdatesId:any
public products:any
public adminupdates:any
public productscounts:any
 public usersUpdatesCounts:any /////////// users counts
public adminsupdateComments:any //////////adminsupdateComments
public showess:any
public selectedblogIds:any
public adminsupdateCommentsReply:any
public adminsupdateCommentsCounts:any
public fffdf:any
public productsCustomersName:any
public productsCustomersID:any
public furnitureTypeGetss:any
public furnitueTypeId:any
public furnitueTypeName:any
// public fffdf:any
// public fffdf:any
// public fffdf:any
// public fffdf:any
// public fffdf:any
// public fffdf:any
// public fffdf:any
// public fffdf:any
// public fffdf:any
// public fffdf:any
// public fffdf:any
// public fffdf:any
// public fffdf:any
// public fffdf:any
// public fffdf:any
// public fffdf:any
// public fffdf:any
// public fffdf:any
// public fffdf:any
// public fffdf:any
// public fffdf:any
// public fffdf:any
// public fffdf:any

comments={
  "name":"",
  "email":"",
  "contactNo":"",
  "leaveComment":"",
    }
    updatesCommentsdata={
      "descriptions":""
    }
  constructor(private route:ActivatedRoute,private furniture:FurnituresService) { }

  ngOnInit() {
    this.individualdata()
    this.usersDetails()
    this.getproductsdetails()
    this.getupdate()
    this.productsCount()
    this.UsersUpdatesCount()
    this.comCounts()
    this.furnitureTypeGet()
  }
   
  individualdata(){
        this.ids=this.route.snapshot.params['_id'];
 
this.usersDetails()
}

usersDetails(){ 
  this.furniture.furnitureusersonly(this.ids).subscribe((res)=>{
    this.furnitureuser=res
   })
}
images(){
  this.image=!this.image
      // this.image=false
}
// (((((((((((((((updates)))))))))))))))
showicons(){
  this.show=!this.show
  this.shows=false
}
showcomments(item){
  this.shows=!this.shows
  this.show=false
   this.updatesId=item._id
 }
 
 getupdate(){
           this.furniture.Getupdates(this.ids).subscribe((res)=>{
             this.adminupdates=res
           }) 
      }
    
     // ***********0verallcomments************
     overallcomment(){
       
     
     this.furniture.overallscommentspost(this.ids,this.comments).subscribe((res)=>{
       
      console.log(res);
      

      this.comments.name="",
      this.comments.contactNo="",
      this.comments.email="",
      this.comments.leaveComment=""
     })
   }

  //  ******************8furniture products details*************
  furnitureTypeGet(){
    this.furniture.furnitureTypeGets(this.ids).subscribe((res)=>{
  this.furnitureTypeGetss=res
  console.log(res);
  
   var furs=this.furnitureTypeGetss[0]
  this.furnitureClick(furs)
  })
  }
  furnitureClick(get?){
  this.furnitueTypeId=get._id
  this.furnitueTypeName=get.furnitureType
  this.getproductsdetails()
  }

  getproductsdetails(){
           this.furniture.products(this.furnitueTypeId).subscribe((res)=>{
             this.products=res
           }) 
         }
         productsCustomersClicks(pro){
this.productsCustomersID=pro._id
this.productsCustomersName=pro.name
         }
      productsCount(){
        this.furniture.productsCounts(this.ids).subscribe((res)=>{
           this.productscounts=res
         }) 
      }
        //  ******************furniture updated counts details*************
        
        UsersUpdatesCount(){
          this.furniture.GetupdatesCounts(this.ids).subscribe((res)=>{
             this.usersUpdatesCounts=res
           }) 
        }
        //  ******************furniture updated comments post details*************

         updatestoadmincomments(){
             this.furniture.commentspostupdates(this.updatesId,this.updatesCommentsdata).subscribe((res)=>{
             })
            this.updatesCommentsdata.descriptions=""
             
                 }
         updatestoAdmincommentsget(result){
        this.showes=!this.showes
         this.commentsUpdatesId=result
           this.comm()
           this.comCounts()
         }
         comm(){
                this.furniture.commentsgetupdates(this.commentsUpdatesId).subscribe((res)=>{
                    this.adminsupdateComments=res
                  })  }

                        comCounts(){
                          this.furniture.commentsgetupdatesCounts(this.updatesId).subscribe((res)=>{
                              this.adminsupdateCommentsCounts=res
                              console.log(this.adminsupdateCommentsCounts);
                              
                              
                            })
                          }
updatesAdminReply(items){
  this.showess=!this.showess
  this.selectedblogIds=items
  this.furniture.commentsgetupdatesReply(this.selectedblogIds).subscribe((res)=>{
    this.adminsupdateCommentsReply=res
   })
}
  

}

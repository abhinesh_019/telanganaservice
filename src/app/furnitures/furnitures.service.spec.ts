import { TestBed, inject } from '@angular/core/testing';

import { FurnituresService } from './furnitures.service';

describe('FurnituresService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FurnituresService]
    });
  });

  it('should be created', inject([FurnituresService], (service: FurnituresService) => {
    expect(service).toBeTruthy();
  }));
});

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
 import 'rxjs/add/operator/map';
import { environment } from '../../../environments/environment.prod';
  
@Injectable()
export class SwimmingService {
  constructor(private http: HttpClient) { }
  locationapi( ){
    return   this.http.get(environment.apiUrl +"/swimmingLocations");
   }
   areaapi(id){
    return   this.http.get(environment.apiUrl +"/swimmingAreas/"+id);
   }


 marriageusers(id,data){
    
    return this.http.get(environment.apiUrl +"/swimmingClientsget/"+id,{params:data});
  }
  marriageusersForSingleAreas(id){
    return   this.http.get(environment.apiUrl +"/swimmingClientsgetAreaSingleCnt/"+id);
   }
  marriageuserscountstot(){
    return   this.http.get(environment.apiUrl +"/swimmingClientsAllCount");
   }
   marriageusersForLocations(){
    return   this.http.get(environment.apiUrl +"/swimmingClientsAllCountLocations");
   }
   marriageusersForMainAreas(id){
    return   this.http.get(environment.apiUrl +"/swimmingClientsAllCountMainArea/"+id);
   }
   marriageusersForSubAreas(id){
    return   this.http.get(environment.apiUrl +"/swimmingClientsAreas/"+id);
   }

   babycaresAddsOnea(id){

    return this.http.get(environment.apiUrl +"/swimmingsAddsOneGeta/"+id);
  }
  babycaresAddsTwoa(id){
    
    return this.http.get(environment.apiUrl +"/swimmingsAddsTwoGeta/"+id);
  }
  babycaresAddsThreea(id){
    
    return this.http.get(environment.apiUrl +"/swimmingsAddsThreeGeta/"+id);
  }
  babycaresAddsFoura(id){
    
    return this.http.get(environment.apiUrl +"/swimmingsAddsFourGeta/"+id);
  }
   
  babycaresAddsOnel(id){
    
    return this.http.get(environment.apiUrl +"/swimmingsAddsOneLocGet/"+id);
  }
  babycaresAddsTwol(id){
    
    return this.http.get(environment.apiUrl +"/swimmingsAddsTwoLocGet/"+id);
  }
  babycaresAddsThreel(id){
    
    return this.http.get(environment.apiUrl +"/swimmingsAddsThreeGetLoc/"+id);
  }
  babycaresAddsFourl(id){
    
    return this.http.get(environment.apiUrl +"/swimmingsAddsFourLocGet/"+id);
  }

  babycaresAddsFivel(id){
    
    return this.http.get(environment.apiUrl +"/swimmingsAddsFiveLocGet/"+id);
  }
   
}
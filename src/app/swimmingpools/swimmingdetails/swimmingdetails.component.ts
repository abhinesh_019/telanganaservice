import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { SwimmingdetailsService } from './swimmingdetails.service';

@Component({
  selector: 'app-swimmingdetails',
  templateUrl: './swimmingdetails.component.html',
  styleUrls: ['./swimmingdetails.component.css']
})
export class SwimmingdetailsComponent implements OnInit {

  public musicDetailsClients:any
  public ids:any
  public gendersCat:any
  public acceriesGet:any
  public acceriesGetc:any
  public gendersId:any
  public showAcceries:boolean=false
public fecilitiesGets:any
public fecilitiesGetsc:any
public openshareid:any
public commentspostsId:any
public viewcommentsid:any
public clientUpdates:any
public replyid:any
public clientUpdatesCount:any
 public getUsersComments:any
public getUsersCommentsCounts:any
public share:boolean=false
public comments:boolean=false
public viewcomments:boolean=false
public replyfrom:boolean=false 
public replies:any
postComments={
  "descriptions":""
}
commentspost={
  "name":"",
  "email":"",
  "contactNo":"",
  "leaveComment":"",
    }

  constructor(private router:Router,private route:ActivatedRoute,private swim:SwimmingdetailsService) { }
   ngOnInit() {
    this.individualdata()
    this.usersDetails()
   this.servicesGenders()
   this.fecilitiesGet()
   this.fecilitiesGetc()
   this.overallcomment()
   this.clientsUpdates()
this.clientsUpdatesCount()
  }
  individualdata(){
    this.ids=this.route.snapshot.params['_id'];

}

usersDetails(){ 
this.swim.usersDetails(this.ids).subscribe((res)=>{
this.musicDetailsClients=res
console.log(res);

})
}
servicesGenders(){ 
  this.swim.servicesGenders(this.ids).subscribe((res)=>{
  this.gendersCat=res
  })
  }
  gendersClick(g){
    this.showAcceries=!this.showAcceries
    this.gendersId=g._id
    this.servicesaccGet()
    this.servicesaccGetc()
  }
  servicesaccGet(){ 
    this.swim.servicesaccGet(this.gendersId).subscribe((res)=>{
    this.acceriesGet=res
      })
    }
    servicesaccGetc(){ 
      this.swim.servicesaccGetc(this.gendersId).subscribe((res)=>{
      this.acceriesGetc=res
        })
      }
      fecilitiesGet(){ 
        this.swim.fecilitiesGet(this.ids).subscribe((res)=>{
        this.fecilitiesGets=res
         console.log(res);
         })
        }
        fecilitiesGetc(){ 
          this.swim.fecilitiesGetc(this.ids).subscribe((res)=>{
          this.fecilitiesGetsc=res
           console.log(res);
           })
          }
          // ******************************************updates present ***************************************
shareClick(up){
  this.share=!this.share
  this.comments=false
  this.viewcomments=false
  this.replyfrom=false
  this.openshareid=up._id
}
commentsClick(up){
  this.comments=!this.comments
  this.share=false
  this.viewcomments=false
  this.commentspostsId=up._id
   this.commentsGettoClints()
   this.commentsGettoClintsCounts()
 }

viewCommentsClick(up){
  this.viewcomments=!this.viewcomments
  this.share=false
  this.viewcommentsid=up._id
}
replyFromm(comms){
  this.replyfrom=!this.replyfrom
  this.share=false
  this.replyid=comms._id
  this.replyFromCommentesGet()
  console.log(this.replyid);
  
}
 clientsUpdates(){   
  this.swim.clientsUpdatesGet(this.ids).subscribe((res)=>{
    this.clientUpdates=res
   })
 }
 
 clientsUpdatesCount(){   
  this.swim.clientsUpdatesGetCounts(this.ids).subscribe((res)=>{
    this.clientUpdatesCount=res
     })
 }

 commentsPoststoClints(){   
  this.swim.commentsPoststoClints(this.commentspostsId,this.postComments).subscribe((res)=>{
     })
     this.postComments.descriptions=""
 }
 commentsGettoClints(){   
  this.swim.commentsGettoClints(this.commentspostsId).subscribe((res)=>{
    this.getUsersComments=res
   })
 }

 commentsGettoClintsCounts(){   
  this.swim.commentsGettoClintsCounts(this.commentspostsId).subscribe((res)=>{
    this.getUsersCommentsCounts=res
    })
 }

 replyFromCommentesGet(){   
  this.swim.replyFromCommentesGet(this.replyid).subscribe((res)=>{
    this.replies=res
    console.log(res);
    
    })
 }


  // ***********0verallcomments************
  overallcomment(){
       
     
    this.swim.overallscommentspost(this.ids,this.commentspost).subscribe((res)=>{
     
     this.commentspost.name="",
     this.commentspost.contactNo="",
     this.commentspost.email="",
     this.commentspost.leaveComment=""
    })
  }

}

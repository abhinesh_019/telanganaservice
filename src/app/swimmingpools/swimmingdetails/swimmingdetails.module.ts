import {MatInputModule} from '@angular/material/input';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material';

import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { SwimmingdetailsComponent } from './swimmingdetails.component';
import { SwimmingdetailsService } from './swimmingdetails.service';
 

const routes:Routes=[{path:'swimmingdetails/:_id/:name',component:SwimmingdetailsComponent},]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    SharedModule
    

  ],
  declarations: [
    SwimmingdetailsComponent,

  ],
  providers: [SwimmingdetailsService],
})
export class SwimmingdetailsModule { }

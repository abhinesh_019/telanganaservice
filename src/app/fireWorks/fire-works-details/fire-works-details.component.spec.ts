import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FireWorksDetailsComponent } from './fire-works-details.component';

describe('FireWorksDetailsComponent', () => {
  let component: FireWorksDetailsComponent;
  let fixture: ComponentFixture<FireWorksDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FireWorksDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FireWorksDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
 import 'rxjs/add/operator/map';
import { environment } from '../../../environments/environment.prod';

@Injectable()
export class FirWorksService {
 constructor(private http: HttpClient) { }

  locationapi(){
    
    return this.http.get(environment.apiUrl +"/fireworksLocations");
  }

  areaapi(id){
    
    return this.http.get(environment.apiUrl +"/fireworksareas/"+id);
  }
  indoorUsers(id,data){
    
    return this.http.get(environment.apiUrl +"/fireworksClientsget/"+id,{params:data});
  }
  indoorUserscountsAll(){
    
    return this.http.get(environment.apiUrl +"/fireworksClientsAllCount");
  }

  indoorUserscountAllMainAreas(){
    
    return this.http.get(environment.apiUrl +"/fireworksClientsCountAreas");
  }

  indoorUserscountAllDist(){
    
    return this.http.get(environment.apiUrl +"/fireworksClientsCountDist");
  }

  indoorUserscountAllAreas(id){
    
    return this.http.get(environment.apiUrl +"/fireworksClientsCount/"+id);
  }
  indoorUserscountAllAreasover(id){
    
    return this.http.get(environment.apiUrl +"/fireworksClientsAreas/"+id);
  }
  babycaresAddsOnea(id){

    return this.http.get(environment.apiUrl +"/fireworksAddsOneGeta/"+id);
  }
  babycaresAddsTwoa(id){
    
    return this.http.get(environment.apiUrl +"/fireworksAddsTwoGeta/"+id);
  }
  babycaresAddsThreea(id){
    
    return this.http.get(environment.apiUrl +"/fireworksAddsThreeGeta/"+id);
  }
  babycaresAddsFoura(id){
    
    return this.http.get(environment.apiUrl +"/fireworksAddsFourGeta/"+id);
  }
   
  babycaresAddsOnel(id){
    
    return this.http.get(environment.apiUrl +"/fireworksAddsOneLocGet/"+id);
  }
  babycaresAddsTwol(id){
    
    return this.http.get(environment.apiUrl +"/fireworksAddsTwoLocGet/"+id);
  }
  babycaresAddsThreel(id){
    
    return this.http.get(environment.apiUrl +"/fireworksAddsThreeGetLoc/"+id);
  }
  babycaresAddsFourl(id){
    
    return this.http.get(environment.apiUrl +"/fireworksAddsFourLocGet/"+id);
  }

  babycaresAddsFivel(id){
    
    return this.http.get(environment.apiUrl +"/fireworksAddsFiveLocGet/"+id);
  }
   

}

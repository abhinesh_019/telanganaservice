import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FireWorksComponent } from './fire-works.component';

describe('FireWorksComponent', () => {
  let component: FireWorksComponent;
  let fixture: ComponentFixture<FireWorksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FireWorksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FireWorksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import {MatInputModule} from '@angular/material/input';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material';

import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { IndoorgamesdetailsComponent } from './indoorgamesdetails.component';
import { IndoorgamesdetailsService } from './indoorgamesdetails.service';
 

const routes:Routes=[{path:'indoorgamesdetails/:_id/:name',component:IndoorgamesdetailsComponent},]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    SharedModule
    

  ],
  declarations: [
    IndoorgamesdetailsComponent,

  ],
  providers: [IndoorgamesdetailsService],
})
export class IndoorgamesdetailsModule { }

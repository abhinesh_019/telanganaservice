import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
 import 'rxjs/add/operator/map';
import { environment } from '../../../environments/environment.prod';

@Injectable()
export class IndoorgamesService {

  constructor(private http: HttpClient) { }

  locationapi(){
    
    return this.http.get(environment.apiUrl +"/indoorgamesLocations");
  }

  areaapi(id){
    
    return this.http.get(environment.apiUrl +"/indoorareas/"+id);
  }
  indoorUsers(id,data){
    
    return this.http.get(environment.apiUrl +"/indoorgamesClientsget/"+id,{params:data});
  }
  indoorUserscountsAll(){
    
    return this.http.get(environment.apiUrl +"/indoorgamesClientsAllCount");
  }

  indoorUserscountAllMainAreas(id){
    
    return this.http.get(environment.apiUrl +"/indoorgamesClientsCountAreas/"+id);
  }

  indoorUserscountAllDist(){
    
    return this.http.get(environment.apiUrl +"/indoorgamesClientsCountDist");
  }

  indoorUserscountAllAreas(id){
    
    return this.http.get(environment.apiUrl +"/indoorgamesClientsCount/"+id);
  }
  indoorUserscountAllAreasover(id){
    
    return this.http.get(environment.apiUrl +"/indoorgamesClientsAreas/"+id);
  }

  babycaresAddsOnea(id){

    return this.http.get(environment.apiUrl +"/indoorgamesAddsOneGeta/"+id);
  }
  babycaresAddsTwoa(id){
    
    return this.http.get(environment.apiUrl +"/indoorgamesAddsTwoGeta/"+id);
  }
  babycaresAddsThreea(id){
    
    return this.http.get(environment.apiUrl +"/indoorgamesAddsThreeGeta/"+id);
  }
  babycaresAddsFoura(id){
    
    return this.http.get(environment.apiUrl +"/indoorgamesAddsFourGeta/"+id);
  }
   
  babycaresAddsOnel(id){
    
    return this.http.get(environment.apiUrl +"/indoorgamesAddsOneLocGet/"+id);
  }
  babycaresAddsTwol(id){
    
    return this.http.get(environment.apiUrl +"/indoorgamesAddsTwoLocGet/"+id);
  }
  babycaresAddsThreel(id){
    
    return this.http.get(environment.apiUrl +"/indoorgamesAddsThreeGetLoc/"+id);
  }
  babycaresAddsFourl(id){
    
    return this.http.get(environment.apiUrl +"/indoorgamesAddsFourLocGet/"+id);
  }

  babycaresAddsFivel(id){
    
    return this.http.get(environment.apiUrl +"/indoorgamesAddsFiveLocGet/"+id);
  }
}
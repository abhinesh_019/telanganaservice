import { Component, OnInit } from '@angular/core';
import { GymService } from '../gym.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-gym',
  templateUrl: './gym.component.html',
  styleUrls: ['./gym.component.css']
})
export class GymComponent implements OnInit {
  
  public locationsgets:any
  public locationName:any
  public its:any
  public locationss:any
  public areasAll:any
  public selectAreaId:any
  public selectArea:any
  public ids:any
public catdeatils:any
public searchString:any
public gymuser:any
public gymusercount:any
public gymusercounttot:any
public popularareas:any
public gymusercountLocations:any
public gymAllCount:any
public gymAllCountAreas:any
public gymAllCountAreasNames:any
public gymAllCountAreasName:any

    // **********************************************************
 
    public imageAddsOne:any
    public babycaresAddsOnes:any
     
    public babycaresAddsFours:any
    public imageAddsFour:any
    public imageAddsThree:any
    public imageAddsTwo:any
    public babycaresAddsThrees:any
    public babycaresAddsTwos:any
    
    
    public babycaresAddsFivesLoc:any
    public imageAddsFiveLoc:any
    public imageAddsFourLoc:any
    public babycaresAddsFoursLoc:any
    public imageAddsThreeLoc:any
    public babycaresAddsThreeLoc:any
    public imageAddsTwoLoc:any
    public babycaresAddsTwoLoc:any
    public imageAddsOneLoc:any
    public babycaresAddsOneLoc:any


constructor(private router:Router,private route:ActivatedRoute,private gym:GymService) { }

  ngOnInit() {
     this.getlocations()  
      
  }
 
 
    getlocations(){
      this.gym.locationsget().subscribe((res)=>{
      this.locationsgets=res
       var id =this.locationsgets[0];
      this.locations(id)
         
      })  }
      locations(get?){
        this.locationName=get.locations
        this.its=get._id
        this.locationss=get.locations
        this.allAreas()
        this.babycaressareasAddsOneLoc()
        this.babycaressareasAddsTwoLoc()
        this.babycaressareasAddsThreeLoc()
        this.babycaressareasAddsFourLoc()
        this.babycaressareasAddsFiveLoc()
      }
      allAreas(){
         this.gym.areaapi(this.its).subscribe((res)=>{
          this.areasAll=res
          var id =this.areasAll[0];          
          this.selectedAreas(id)
         })
       }
      
      selectedAreas(result){
        this.selectAreaId=result._id
        this.selectArea=result.area
         this.gymusers()
         this.gymAllCountsAreas()
          this.gymAllCountsAreasNames()
          this.babycaressareasAddsOnea()
          this.babycaressareasAddsTwoa()
          this.babycaressareasAddsThreea()
          this.babycaressareasAddsFoura()
       }
      gymusers(){
        var data:any =  {}
        if(this.searchString){
          data.search=this.searchString
          }
        this.gym.users(this.selectAreaId,data).subscribe((res)=>{
          this.gymuser=res
          })
         this.gymAllCounts()
         this.gymAllPopularAreasGrouping()
         this.gymAllCountsAreasNames()
       }
      
      searchFilter(){
      this.gymusers()
      }
      
      gymAllCounts(){
        this.gym.gymuserscountstot().subscribe((res)=>{
         this.gymAllCount=res
         })
      }
      gymAllCountsAreas(){
        this.gym.gymuserscountstotSAreas(this.selectAreaId).subscribe((res)=>{
         this.gymAllCountAreas=res
         })
      }
      
      gymAllCountsAreasNames(){
        this.gym.gymuserscountstotSAreasNames(this.selectAreaId).subscribe((res)=>{
         this.gymAllCountAreasNames=res
        })
      }

      gymAllPopularAreasGrouping(){
        this.gym.gymuserscountsAndAreasNames(this.selectAreaId).subscribe((res)=>{
         this.gymAllCountAreasName=res
        console.log(res);
        
        })
      }
 
// ****************************************************


// ****************************************************

babycaressareasAddsOnea(){
  this.gym.babycaresAddsOnea(this.selectAreaId).subscribe((res)=>{
     this.babycaresAddsOnes=res
 
     
   })
}
babycaressareasAddsTwoa(){
  this.gym.babycaresAddsTwoa(this.selectAreaId).subscribe((res)=>{
     this.babycaresAddsTwos=res
  
   })
}
babycaressareasAddsThreea(){
  this.gym.babycaresAddsThreea(this.selectAreaId).subscribe((res)=>{
     this.babycaresAddsThrees=res
  
   })
}

babycaressareasAddsFoura(){
  this.gym.babycaresAddsFoura(this.selectAreaId).subscribe((res)=>{
     this.babycaresAddsFours=res

   })
}

babycaressareasAddsOneLoc(){
  this.gym.babycaresAddsOnel(this.its).subscribe((res)=>{
     this.babycaresAddsOneLoc=res 
   })
}
babycaressareasAddsTwoLoc(){
  this.gym.babycaresAddsTwol(this.its).subscribe((res)=>{
     this.babycaresAddsTwoLoc=res
    
   })
}
babycaressareasAddsThreeLoc(){
  this.gym.babycaresAddsThreel(this.its).subscribe((res)=>{
     this.babycaresAddsThreeLoc=res
     
   })
}
babycaressareasAddsFourLoc(){
  this.gym.babycaresAddsFourl(this.its).subscribe((res)=>{
     this.babycaresAddsFoursLoc=res
     
   })
}
babycaressareasAddsFiveLoc(){
  this.gym.babycaresAddsFivel(this.its).subscribe((res)=>{
     this.babycaresAddsFivesLoc=res
    
   })
}

}

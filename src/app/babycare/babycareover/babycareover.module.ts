import {MatInputModule} from '@angular/material/input';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material';
import { BabysharedModule } from '../babyshared/babyshared.module';
import { BabycareoverComponent } from './babycareover.component';
import { NgModule } from '@angular/core';



const routes:Routes=[{path:'babycaredetails/:_id',component:BabycareoverComponent},]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    BabysharedModule
    

  ],
  declarations: [
    BabycareoverComponent,

  ],
  providers: [],
})
export class BabycareoverModule { }

import { Component, OnInit } from '@angular/core';
import { BeautyparlourService } from '../beautyparlour.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-beautyparlour',
  templateUrl: './beautyparlour.component.html',
  styleUrls: ['./beautyparlour.component.css']
})
export class BeautyparlourComponent implements OnInit {
public clients:any
  public ids:any
  public mainuser:any
  public catdeatils:any
  public locationsgets:any
  public areasAll:any
  public locationName:any
  public idss:any
  public selectAreaId:any
  public selectArea:any
  public subsArea:any
  public areasCount:any
  public womensC:any
  public mensC:any
  public womensCo:any
  public searchString:any

  public imageAddsOne:any
  public babycaresAddsOnes:any
   
  public babycaresAddsFours:any
  public imageAddsFour:any
  public imageAddsThree:any
  public imageAddsTwo:any
  public babycaresAddsThrees:any
  public babycaresAddsTwos:any
  
  
  public babycaresAddsFivesLoc:any
  public imageAddsFiveLoc:any
  public imageAddsFourLoc:any
  public babycaresAddsFoursLoc:any
  public imageAddsThreeLoc:any
  public babycaresAddsThreeLoc:any
  public imageAddsTwoLoc:any
  public babycaresAddsTwoLoc:any
  public imageAddsOneLoc:any
  public babycaresAddsOneLoc:any
  constructor(private parlour:BeautyparlourService,private route:ActivatedRoute) { }
  

  ngOnInit() {
    this.individualdata()
    this.usersDetails()
    this.getlocations()   
    this.catContCounts()
    this.mensCounts() 
    this.womensCounts()
  }
  individualdata(){
    this.ids=this.route.snapshot.params['_id'];
 }


usersDetails(){ 
  this.parlour.individualcat(this.ids).subscribe((res)=>{
    this.catdeatils=res
      })
}

      // *********************** locations ****************
      
         getlocations(){
           this.parlour.locationsget(this.ids).subscribe((res)=>{
           this.locationsgets=res
            var id =this.locationsgets[0];
            this.locations(id)
               
           })  }


           locations(get?){
             this.locationName=get.locations
             this.idss=get._id
              this.allAreas()

      this.babycaressareasAddsOneLoc()
      this.babycaressareasAddsTwoLoc()
      this.babycaressareasAddsThreeLoc()
      this.babycaressareasAddsFourLoc()
      this.babycaressareasAddsFiveLoc()
           }

 // ************************************************************************************************************************************
 
//  **********************************area *************************************

allAreas(){
        
this.parlour.areaapi(this.idss).subscribe((res)=>{
 this.areasAll=res
 var id =this.areasAll[0];
 
 
 this.selectedAreas(id)
 

})

}

selectedAreas(result){
this.selectAreaId=result._id
this.selectArea=result.area

this.clientDetails()
this.areasCounts()
this.clientDetailsCountSubArea()
this.babycaressareasAddsOnea()
this.babycaressareasAddsTwoa()
this.babycaressareasAddsThreea()
this.babycaressareasAddsFoura()
}

// ************************************************************************

  clientDetails(){ 
    var data:any =  {}
    if(this.searchString){
      data.search=this.searchString
      }
    this.parlour.Client(this.selectAreaId,data).subscribe((res)=>{
    this.clients=res
     })
    }
    searchFilter(){
      this.clientDetails()
    }
    clientDetailsCountSubArea(){ 
      this.parlour.subArea(this.selectAreaId).subscribe((res)=>{
      this.subsArea=res
        
      })
      }
      areasCounts(){ 
        this.parlour.Area(this.selectAreaId).subscribe((res)=>{
        this.areasCount=res
          
        })
        }
        mensCounts(){ 
          this.parlour.mens().subscribe((res)=>{
          this.mensC=res
 
          })
          }
          womensCounts(){ 
            this.parlour.womens().subscribe((res)=>{
            this.womensCo=res
   
            })
            }
          catContCounts(){ 
            this.parlour.catCount().subscribe((res)=>{
            this.womensC=res
               
            })
            }

            babycaressareasAddsOnea(){
              this.parlour.babycaresAddsOnea(this.selectAreaId).subscribe((res)=>{
                 this.babycaresAddsOnes=res
             
                 
               })
            }
            babycaressareasAddsTwoa(){
              this.parlour.babycaresAddsTwoa(this.selectAreaId).subscribe((res)=>{
                 this.babycaresAddsTwos=res
              
               })
            }
            babycaressareasAddsThreea(){
              this.parlour.babycaresAddsThreea(this.selectAreaId).subscribe((res)=>{
                 this.babycaresAddsThrees=res
              
               })
            }

            babycaressareasAddsFoura(){
              this.parlour.babycaresAddsFoura(this.selectAreaId).subscribe((res)=>{
                 this.babycaresAddsFours=res
  
               })
            }

            babycaressareasAddsOneLoc(){
              this.parlour.babycaresAddsOnel(this.idss).subscribe((res)=>{
                 this.babycaresAddsOneLoc=res
                 console.log(res);

               })
            }
            babycaressareasAddsTwoLoc(){
              this.parlour.babycaresAddsTwol(this.idss).subscribe((res)=>{
                 this.babycaresAddsTwoLoc=res
                 console.log(res);

               })
            }
            babycaressareasAddsThreeLoc(){
              this.parlour.babycaresAddsThreel(this.idss).subscribe((res)=>{
                 this.babycaresAddsThreeLoc=res
                 console.log(res);

               })
            }
            babycaressareasAddsFourLoc(){
              this.parlour.babycaresAddsFourl(this.idss).subscribe((res)=>{
                 this.babycaresAddsFoursLoc=res
                 console.log(res);

               })
            }
            babycaressareasAddsFiveLoc(){
              this.parlour.babycaresAddsFivel(this.idss).subscribe((res)=>{
                 this.babycaresAddsFivesLoc=res
                 console.log(res);

               })
            }

}


import {MatInputModule} from '@angular/material/input';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material';
import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
 import { BeautyparlourService } from '../beautyparlour.service';
import { BeautyparlourComponent } from './beautyparlour.component';



const routes:Routes=[{path:'parlourselect/:_id/:name',component:BeautyparlourComponent},]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    SharedModule
    

  ],
  declarations: [
    BeautyparlourComponent,

  ],
  providers: [BeautyparlourService],
})
export class BeautyparlourModule { }

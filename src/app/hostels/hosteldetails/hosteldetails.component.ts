import { Component, OnInit } from '@angular/core';
import { HosteldetailsService } from './hosteldetails.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-hosteldetails',
  templateUrl: './hosteldetails.component.html',
  styleUrls: ['./hosteldetails.component.css']
})
export class HosteldetailsComponent implements OnInit {

  public clientsDetails:any
  public ids:any
  public showmains:boolean=false
  public updatesShows:boolean=false
  public viewupdatesShows:boolean=false
  public onlyCommentsShows:boolean=false
  public ImagesShows:boolean=false
  public ProductsShows:boolean=false
  public ProductsViewShows:boolean=false

  public usersID:any
public fireWorksClients:any
public viewsProfiles:any
public viewsNoti:any
public clientsInd:any
  public servicesOneGet:any
public servicevehicleName:any
public servicesIds:any
public servicesOneGets:any
public servicevehicleNames:any
public servicesone:any
public servicesTwos:any
public trackRecordsId:any
public trackRecordsName:any
public trackRecordsGet:any
public trackRecordsgetsData:any
public servicesOneGetc:any
public trackRecordsgetsDatac:any
public servicesTwosc:any
public totalCustomer:any
public totalCustomerc:any
public fecilitiesGets:any
public fecilitiesGetsc:any
public commentspostsId:any
  public viewcommentsid:any
  public clientUpdates:any
  public replyid:any
  public clientUpdatesCount:any
   public getUsersComments:any
  public getUsersCommentsCounts:any
  public share:boolean=false
  public comments:boolean=false
  public viewcomments:boolean=false
  public replyfrom:boolean=false 
  public replies:any
  public openshareid:any
public imageNameupdates:any
  public videoNames:any
  public gcomm:any
  public gcommc:any
  public fireWorksDetails:any
  public serviceTwoGetc:any
  public fireWorksDetailsId:any
  public fireWorksDetailsName:any
  public cusotmersgetDetails:any
  public cusotmersgetDetailsc:any
  public fecilitiesGet:any
  public fecilitiesGetc:any




  postComments={
    "descriptions":""
  }
  commentspost={
    "name":"",
    "email":"",
    "contactNo":"",
    "leaveComment":"",
      }
  constructor(private router:Router,private route:ActivatedRoute,private hosteldetails:HosteldetailsService) { }
   ngOnInit() {
    this.individualdata()
    this.usersDetails()
   this.clientsUpdates()
   this.clientsUpdatesCount()
   this.fecilities()
   this.fecilitiesc()
  }
  individualdata(){
    this.ids=this.route.snapshot.params['_id'];

}

usersDetails(){ 
this.hosteldetails.usersDetails(this.ids).subscribe((res)=>{
this.clientsDetails=res
 
})
}
 
    // ******************************************updates present ***************************************
shareClick(up){
  this.share=!this.share
  this.comments=false
  this.viewcomments=false
  this.replyfrom=false
  this.openshareid=up._id
}
commentsClick(up){
  this.comments=!this.comments
  this.share=false
  this.viewcomments=false
  this.commentspostsId=up._id
   this.commentsGettoClints()
   this.commentsGettoClintsCounts()
 }

viewCommentsClick(up){
  this.viewcomments=!this.viewcomments
  this.share=false
  this.viewcommentsid=up._id
}
replyFromm(comms){
  this.replyfrom=!this.replyfrom
  this.share=false
  this.replyid=comms._id
  this.replyFromCommentesGet()
  console.log(this.replyid);
  
}
 clientsUpdates(){   
  this.hosteldetails.clientsUpdatesGet(this.ids).subscribe((res)=>{
    this.clientUpdates=res
   })
 }
 
 clientsUpdatesCount(){   
  this.hosteldetails.clientsUpdatesGetCounts(this.ids).subscribe((res)=>{
    this.clientUpdatesCount=res
     })
 }

 commentsPoststoClints(){   
  this.hosteldetails.commentsPoststoClints(this.commentspostsId,this.postComments).subscribe((res)=>{
     })
     this.postComments.descriptions=""
 }
 commentsGettoClints(){   
  this.hosteldetails.commentsGettoClints(this.commentspostsId).subscribe((res)=>{
    this.getUsersComments=res
   })
 }

 commentsGettoClintsCounts(){   
  this.hosteldetails.commentsGettoClintsCounts(this.commentspostsId).subscribe((res)=>{
    this.getUsersCommentsCounts=res
    })
 }

 replyFromCommentesGet(){   
  this.hosteldetails.replyFromCommentesGet(this.replyid).subscribe((res)=>{
    this.replies=res
    console.log(res);
    
    })
 }


  // ***********0verallcomments************
  overallcomment(){
       
     
    this.hosteldetails.overallscommentspost(this.ids,this.commentspost).subscribe((res)=>{
     
     this.commentspost.name="",
     this.commentspost.contactNo="",
     this.commentspost.email="",
     this.commentspost.leaveComment=""
    })
  }
  fecilities(){ 
    this.hosteldetails.fecilities(this.ids).subscribe((res)=>{
    this.fecilitiesGet=res
    })
    }

    fecilitiesc(){ 
      this.hosteldetails.fecilitiesc(this.ids).subscribe((res)=>{
      this.fecilitiesGetc=res    
      })
      }
 }

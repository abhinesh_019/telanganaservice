import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import{ RouterModule,Routes, ActivatedRoute} from'@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {MatFormFieldModule} from '@angular/material/form-field';

import {MatInputModule} from '@angular/material/input';
import { SharedModule } from '../../shared/shared.module';
import { HosteldetailsComponent } from './hosteldetails.component';
import { HosteldetailsService } from './hosteldetails.service';



const routes:Routes=[{path:'hosteldetails/:_id/:name',component:HosteldetailsComponent},]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    SharedModule
    

  ],
  declarations: [
    HosteldetailsComponent,

  ],
  providers: [HosteldetailsService],
})


export class HosteldetailsModule { }

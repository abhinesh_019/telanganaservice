import { Component, OnInit } from '@angular/core';
import { AdmingymService } from './admingym.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-admingym',
  templateUrl: './admingym.component.html',
  styleUrls: ['./admingym.component.css']
})
export class AdmingymComponent implements OnInit {
  public showmains:boolean=false
  public updatesShows:boolean=false
  public serviceShows:boolean=false
  public onlyCommentsShows:boolean=false
  public ImagesShows:boolean=false
  public ProductsShows:boolean=false
  public ProductsViewShows:boolean=false
  public usersRecords:boolean=false
  public equipments:boolean=false
  public equipmentsShow:boolean=false
  public viewsProfiles:boolean=false
  public viewsNoti:boolean=false
  // public equipmentsShow:boolean=false
  public usersID:any
  public gymuser:any
  public ids:any
  public imageName:any
  public catget:any
  public genderName:any
  public genderNameId:any
  public showdetails:any
  public deatilsId:any
  public gymServicesCount:any
  public gymServicesind:any
  public gymServices:any
  public imageNames:any
  public equipmentsId:any
  public equipmentsind:any
  public equipment:any
  public equipmentsCount:any
  public fecilities:any
  public fecilitie:any
  public fecilitiesCount:any
  public onlyComm:any
public onlyCommC:any
public imageNameupdates:any
public videoName:any
public usersdetailsId:any
public usersget:any
public usersgetCount:any
public caageriesEquips:any
public genderNameEquiId:any
public genderNameEqui:any
public cageriesFecs:any
public genderNameFecId:any
public genderNameFec:any
public imageNamesEqus:any
public imageNamesEqu:any
 public genderNameUsersId:any
public genderNameUsers:any
public cageriesOnlyUser:any
// public onlyCommC:any
// public onlyCommC:any
// public onlyCommC:any
// public onlyCommC:any
// public onlyCommC:any
// public onlyCommC:any
// public onlyCommC:any
// public onlyCommC:any
// public onlyCommC:any
// public onlyCommC:any
// public onlyCommC:any
// public onlyCommC:any
// public onlyCommC:any
// public onlyCommC:any
// public onlyCommC:any
// public onlyCommC:any
// public onlyCommC:any
 
////////////////////////////////////// updates
public share:any
public comments:any
public viewcomments:any
public viewcommentsid:any
public replyfrom:any
public replyid:any
public clientUpdates:any
public clientUpdatesCnts:any
public getUsersComments:any
public commentspostsId:any
public getUsersCommentsCounts:any
public replies:any
public clientUpdatesCount:any
public openshareid:any
 

  usersonly={
  name:"",
  area:"",
  coachName:"",
  aboutCoach:"",
  goal:"",
  joinDate:"",
  shortDescp1:"",
  descriptions1:"",
  presentDate:"",
  shortDescp2:"",
  descriptions2:"",
  images1:"",
   }
postComments={
  "descriptions":""
}

services={
  serviceName:"",
  trainerName:"",
  yearsOfExp:"",
  timmings:"",
  trainerdes:"",
  generalDescription:"",
   images1:"",
  images2:"",
  }
  eqs={
    equiName:"",
    trainerName:"",
    yearsOfExp:"",
    height:"",
    weight:"",
    description1:"",
    description2:"",
    images1:"",
    images2:""
    }
    fet={
      ficilitiesName:"",
    descriptions:"",
    maintainces:"",
    images1:"",
 
      }
      ups={
        images:"",
          title:"",
          description:"",
          viedoes:"",
      }
  cat={
    genders:""
  }
  formData: FormData = new FormData(); 


  constructor(private route:ActivatedRoute,private gym:AdmingymService) { }
  
    ngOnInit() {
      this.usersID=localStorage.getItem('ads');
   this.usersDetails()
      this.servicecatageriesget()
      this.onlyComments()
      this.onlyCommentsC()
      this.clientsUpdates()
      this.clientsUpdatesCount()
      this.caageriesEquip()
      this.cageriesFecilities()
      this.fecilitiesget()
      this.fecilitiesgetCount()
      this.cageriesOnlyUsers()
      
    }
    viewProfile(){
      this.viewsProfiles=!this.viewsProfiles
      this.viewsNoti=false
    }
    viewnoti(){
      this.viewsNoti=!this.viewsNoti
      this.viewsProfiles=false
  
    }
    usersDetails(){ 
      this.usersID=localStorage.getItem('ads');
      this.gym.gymusersonly(this.usersID).subscribe((res)=>{
      this.gymuser=res
       })
      }

    mainMenuShow(){
  this.showmains=!this.showmains
  this.updatesShows=false
  this.serviceShows=false
  this.onlyCommentsShows=false
  this.ImagesShows=false
  this.ProductsShows=false
  this.ProductsViewShows=false
  this.usersRecords=false
  this.equipments=false
  this.fecilities=false

    }
    updatesShow(){
      this.updatesShows=!this.updatesShows
      this.showmains=false
      this.serviceShows=false
      this.onlyCommentsShows=false
      this.ImagesShows=false
      this.ProductsShows=false
      this.ProductsViewShows=false
      this.usersRecords=false
      this.equipments=false
      this.fecilities=false

    }
    servicesShow(){
      this.serviceShows=!this.serviceShows
      this.updatesShows=false
      this.showmains=false
      this.onlyCommentsShows=false
      this.ImagesShows=false
      this.ProductsShows=false
      this.ProductsViewShows=false
      this.usersRecords=false
      this.equipments=false
      this.fecilities=false

    }
    onlyCommentsShow(){
      this.onlyCommentsShows=!this.onlyCommentsShows
      this.updatesShows=false
      this.showmains=false
      this.serviceShows=false
      this.ImagesShows=false
      this.ProductsShows=false
      this.ProductsViewShows=false
      this.usersRecords=false
      this.equipments=false
      this.fecilities=false

    }
    ImagesShow(){
      
      this.ImagesShows=!this.ImagesShows
      this.updatesShows=false
      this.showmains=false
      this.onlyCommentsShows=false
      this.serviceShows=false
      this.ProductsShows=false
      this.ProductsViewShows=false
      this.usersRecords=false
      this.equipments=false
      this.fecilities=false

    }
    EquipnmentsShow(){
      this.ProductsShows=!this.ProductsShows
      this.updatesShows=false
      this.showmains=false
      this.onlyCommentsShows=false
      this.serviceShows=false
      this.ImagesShows=false
      this.ProductsViewShows=false
      this.usersRecords=false
      this.equipments=false
      this.fecilities=false

    }
    FicilitiesShow(){
      this.ProductsViewShows=!this.ProductsViewShows
      this.updatesShows=false
      this.showmains=false
      this.onlyCommentsShows=false
      this.serviceShows=false
      this.ImagesShows=false
      this.ProductsShows=false
      this.usersRecords=false
      this.equipments=false
      this.fecilities=false

    }
    noOfUsersShow(){
      this.usersRecords=!this.usersRecords
      this.ProductsViewShows=false
      this.updatesShows=false
      this.showmains=false
      this.onlyCommentsShows=false
      this.serviceShows=false
      this.ImagesShows=false
      this.ProductsShows=false
      this.equipments=false
      this.fecilities=false

    }
    EquipmentShow(){
      this.equipments=!this.equipments
      this.usersRecords=false
      this.ProductsViewShows=false
      this.updatesShows=false
      this.showmains=false
      this.onlyCommentsShows=false
      this.serviceShows=false
      this.ImagesShows=false
      this.ProductsShows=false
      this.fecilities=false
    }
    FacilitiesShow(){
      this.fecilities=!this.fecilities
      this.equipments=false
      this.usersRecords=false
      this.ProductsViewShows=false
      this.updatesShows=false
      this.showmains=false
      this.onlyCommentsShows=false
      this.serviceShows=false
      this.ImagesShows=false
      this.ProductsShows=false
    }

     handleFileInputServices($event:any,images){
      this.imageName= File = $event.target.files[0];
   }
   handleFileInputService($event:any,images){
    this.imageNames= File = $event.target.files[0];
 }
    servicePostData(){
      this.formData.append('images1',this.imageName);
      this.formData.append('images2',this.imageNames);
          
      this.formData.append('serviceName',this.services.serviceName);
      this.formData.append('trainerName',this.services.trainerName);
      this.formData.append('yearsOfExp',this.services.yearsOfExp);
      this.formData.append('timmings',this.services.timmings);
      this.formData.append('trainerdes',this.services.trainerdes);
      this.formData.append('generalDescription',this.services.generalDescription);
 
       this.gym.postsUpdata(this.genderNameId,this.formData).subscribe((res)=>{
          
           })
           this.services.serviceName="",
          this.services.trainerName="",
          this.services.yearsOfExp="",
          this.services.timmings="",
          this.services.trainerdes,
          this.services.generalDescription="",
          this.services.images1="",
          this.services.images2=""
       }
      //  ************************************************************** catageries for services ******************************************************************

      servicecatageries(){
        this.gym.postscatatageries(this.usersID,this.cat).subscribe((res)=>{
         })
      }
      servicecatageriesget(){
         this.gym.postscatatageriesget(this.usersID).subscribe((res)=>{
          this.catget=res
          var id =this.catget[0];
          this.caageries(id)
         })
      }
      caageries(get?){
this.genderName=get.genders
this.genderNameId=get._id
 this.userServicesCount()
 this.userServices()
   }
 
      clickImageDetails(ser){
        this.deatilsId=ser._id
        this.showdetails=!this.showdetails
         this.userServicesind()
        
     }

// ************************************************************** catageries for Equipments ******************************************************************
    
     caageriesEquip(){
      this.gym.postscatatageriesget(this.usersID).subscribe((res)=>{
       this.caageriesEquips=res
       var idss =this.caageriesEquips[0];
       this.caageriesEquis(idss)
       })
   }

   caageriesEquis(get?){
    this.genderNameEqui=get.genders
    this.genderNameEquiId=get._id
    this.equipmentsgetCount()
    this.equipmentsget()
   }
// ************************************************************** catageries for fecilities ******************************************************************

cageriesFecilities(){
  this.gym.postscatatageriesget(this.usersID).subscribe((res)=>{
   this.cageriesFecs=res
   var id =this.cageriesFecs[0];
   this.caagerieFec(id)
   })
}

caagerieFec(get?){
this.genderNameFec=get.genders
this.genderNameFecId=get._id

  }

// *****************************************************************************************************************************




      userServicesCount(){ 
       
        this.gym.userServicesCount(this.genderNameId).subscribe((res)=>{
        this.gymServicesCount=res
         
         })  
      
      }
      userServicesind(){ 
        
        this.gym.userServicesind(this.deatilsId).subscribe((res)=>{
        this.gymServicesind=res
         })  
       }
      userServices(){ 
        this.gym.userServices(this.genderNameId).subscribe((res)=>{
        this.gymServices=res
        console.log(res);
        
         })  
         
      }

// ********************************** equipment *************************

      handleFileInputequipments($event:any,images){
      this.imageNamesEqu= File = $event.target.files[0];
   }
   handleFileInputequipment($event:any,images){
    this.imageNamesEqus= File = $event.target.files[0];
 }
   equipmentPostData(){
      this.formData.append('images1',this.imageNamesEqu);
      this.formData.append('images2',this.imageNamesEqus);
      this.formData.append('equiName',this.eqs.equiName);
      this.formData.append('trainerName',this.eqs.trainerName);
      this.formData.append('yearsOfExp',this.eqs.yearsOfExp);
      this.formData.append('height',this.eqs.height);
      this.formData.append('weight',this.eqs.weight);
      this.formData.append('description1',this.eqs.description1);
      this.formData.append('description2',this.eqs.description2);

       this.gym.postsEquipment(this.genderNameEquiId,this.formData).subscribe((res)=>{
          
           })
           this.eqs.equiName="",
          this.eqs.trainerName="",
          this.eqs.yearsOfExp="",
          this.eqs.height="",
          this.eqs.weight="",
          this.eqs.description1="",
          this.eqs.description2="",
          this.eqs.images1="",
          this.eqs.images2=""
      }
   
       equipmentsget(){ 
        this.gym.equipmentsget(this.genderNameEquiId).subscribe((res)=>{
        this.equipment=res
         
         })  }
         clickEquipmentsDetails(ser){
          this.equipmentsId=ser._id
          this.equipmentsShow=!this.equipmentsShow
          this.showdetails=false
          this.equipmentsgetInd()
           
        }
        equipmentsgetCount(){ 
          this.gym.equipmentsgetCount(this.genderNameEquiId).subscribe((res)=>{
          this.equipmentsCount=res
          })  }
      equipmentsgetInd(){ 
        this.gym.equipmentsgetInd(this.equipmentsId).subscribe((res)=>{
        this.equipmentsind=res
         })  }



// ********************************** fecilities *************************


         handleFileInputFecilities($event:any,images){
          this.imageNames= File = $event.target.files[0];
       }
       fecilitiesPostData(){
          this.formData.append('images1',this.imageNames);
          this.formData.append('images2',this.imageNames);
    
          this.formData.append('ficilitiesName',this.fet.ficilitiesName);
          this.formData.append('descriptions',this.fet.descriptions);
          this.formData.append('maintainces',this.fet.maintainces);
         
    
           this.gym.postsficilities(this.usersID,this.formData).subscribe((res)=>{
              
               })
               this.fet.ficilitiesName="",
              this.fet.descriptions="",
              this.fet.maintainces=""
              this.fet.images1=""

          }

         
fecilitiesget(){ 
  this.gym.fecilitiesget(this.usersID).subscribe((res)=>{
  this.fecilitie=res
     
  })  }

  fecilitiesgetCount(){ 
    this.gym.fecilitiesgetCount(this.usersID).subscribe((res)=>{
    this.fecilitiesCount=res
    
     }) 
      }

     
  // *********************************************************
  onlyComments(){   
    this.gym.overallscommentsge(this.usersID).subscribe((res)=>{
      this.onlyComm=res
    
      
      })
   }
  
   onlyCommentsC(){   
    this.gym.onlyCommentsGetCount(this.usersID).subscribe((res)=>{
      this.onlyCommC=res
      })
   }
      
  
  
// ******************************************updates present ***************************************
shareClick(up){
  this.share=!this.share
  this.comments=false
  this.viewcomments=false
  this.replyfrom=false
  this.openshareid=up._id
}
commentsClick(up){
  this.comments=!this.comments
  this.share=false
  this.viewcomments=false
  this.commentspostsId=up._id
   this.commentsGettoClints()
   this.commentsGettoClintsCounts()
 }

viewCommentsClick(up){
  this.viewcomments=!this.viewcomments
  this.share=false
  this.viewcommentsid=up._id
}
replyFromm(comms){
  this.replyfrom=!this.replyfrom
  this.share=false
  this.replyid=comms._id
  this.replyFromCommentesGet()
   
}

handleFileInputUpdates($event:any,images){
  this.imageNameupdates= File = $event.target.files[0];  
}

handleFileInputUpdatess($event:any,viedoes){
  this.videoName= File = $event.target.files[0];
  this.formData.append(viedoes,this.videoName);
}

updatesPostData(){
this.formData.append('images',this.imageNameupdates);
this.formData.append('title',this.ups.title);
this.formData.append('description',this.ups.description);
 

  console.log(this.formData)

 this.gym.postsUpdates(this.usersID,this.formData).subscribe((res)=>{
  
     })
     this.ups.title="",
     this.ups.description="",
     this.ups.images="",
     this.ups.viedoes=""
}

 clientsUpdates(){   
  this.gym.clientsUpdatesGet(this.usersID).subscribe((res)=>{
    this.clientUpdates=res
    })
   
   
 }
 
 clientsUpdatesCount(){   
  this.gym.clientsUpdatesGetCounts(this.usersID).subscribe((res)=>{
    this.clientUpdatesCount=res
   })
 }

 commentsPoststoClints(){   
  this.gym.commentsPoststoClints(this.replyid,this.postComments).subscribe((res)=>{
     })
     this.postComments.descriptions=""
 }
 commentsGettoClints(){   
  this.gym.commentsGettoClints(this.commentspostsId).subscribe((res)=>{
    this.getUsersComments=res
   })
 }

 commentsGettoClintsCounts(){   
  this.gym.commentsGettoClintsCounts(this.commentspostsId).subscribe((res)=>{
    this.getUsersCommentsCounts=res
    })
 }

 replyFromCommentesGet(){   
  this.gym.replyFromCommentesGet(this.replyid).subscribe((res)=>{
    this.replies=res
    })
 }

// ********************************************only users **************************************************
cageriesOnlyUsers(){
  this.gym.postscatatageriesget(this.usersID).subscribe((res)=>{
   this.cageriesOnlyUser=res
   var ifds =this.cageriesOnlyUser[0];
   this.caagerieUsers(ifds)
   })
}
caagerieUsers(get?){
this.genderNameUsers=get.genders
this.genderNameUsersId=get._id
 this.usersdetailsget()
this.usersdetailsgetCount()
  }
 
handleFileInputusers($event:any,images){
  this.imageName= File = $event.target.files[0];
}
onlyusersPostData(){
  this.formData.append('images1',this.imageName);
  this.formData.append('name',this.usersonly.name);
  this.formData.append('area',this.usersonly.area);
  this.formData.append('coachName',this.usersonly.coachName);
  this.formData.append('aboutCoach',this.usersonly.aboutCoach);
  this.formData.append('goal',this.usersonly.goal);
  this.formData.append('joinDate',this.usersonly.joinDate);
  this.formData.append('shortDescp1',this.usersonly.shortDescp1);
  this.formData.append('descriptions1',this.usersonly.descriptions1);
  this.formData.append('presentDate',this.usersonly.presentDate);
  this.formData.append('shortDescp2',this.usersonly.shortDescp2);
  this.formData.append('descriptions2',this.usersonly.descriptions2);

   this.gym.postsUpdatausers(this.genderNameUsersId,this.formData).subscribe((res)=>{
      
       })
       this.usersonly.name="",
      this.usersonly.area="",
      this.usersonly.coachName="",
      this.usersonly.aboutCoach="",
      this.usersonly.goal="",
      this.usersonly.joinDate="",
      this.usersonly.shortDescp1="",
      this.usersonly.descriptions1=""
      this.usersonly.presentDate="",
      this.usersonly.shortDescp2="",
      this.usersonly.descriptions2=""
    }
    
   // ********************************** users details get *************************
usersdetailsget(){ 
  this.gym.usersdetailsget(this.genderNameUsersId).subscribe((res)=>{
  this.usersget=res
   })  }
 
  usersdetailsgetCount(){ 
    this.gym.usersdetailsgetCount(this.genderNameUsersId).subscribe((res)=>{
    this.usersgetCount=res
     }) 
     }

     
  // *********************************************************

  
   
 
}
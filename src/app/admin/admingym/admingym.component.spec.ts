import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdmingymComponent } from './admingym.component';

describe('AdmingymComponent', () => {
  let component: AdmingymComponent;
  let fixture: ComponentFixture<AdmingymComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdmingymComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdmingymComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AdminfunctionhallsService } from './adminfunctionhalls.service';

@Component({
  selector: 'app-adminfunctionhalls',
  templateUrl: './adminfunctionhalls.component.html',
  styleUrls: ['./adminfunctionhalls.component.css']
})
export class AdminfunctionhallsComponent implements OnInit {
  public showmains:boolean=false
  public updatesShows:boolean=false
  public viewupdatesShows:boolean=false
  public onlyCommentsShows:boolean=false
  public ImagesShows:boolean=false
  public ficilitiesShow:boolean=false
  public customersShows:boolean=false
  public feature:boolean=false
  public usersID:any
  public imageName:any
  public usersall:any
  public viewsProfiles:any
  public viewsNoti:any
   public hallsFec:any
   public hallsFecnt:any
    public hallscus:any
    public hallscusc:any
public imageNameupdates:any
public videoName:any
 ////////////////////////////////////// updates
 public share:any
 public comments:any
 public viewcomments:any
 public viewcommentsid:any
 public replyfrom:any
 public replyid:any
 public clientUpdates:any
 public clientUpdatesCnts:any
 public getUsersComments:any
 public commentspostsId:any
 public getUsersCommentsCounts:any
 public replies:any
 public clientUpdatesCount:any
 public openshareid:any
public onlyComm:any
public onlyCommC:any
public futuresGet:any
public futuresGetsName:any
public futuresGetsId:any
public futuresTwoGet:any
public imageNamess:any



postComments={
  "descriptions":""
}


  cus={
    brideName:"",
    groomeName:"",
    Place:"",
    images1:"",
    }

ups={
  images:"",
    title:"",
    descriptions:"",
    viedoes:"",
}


  fec={
    name:"",
    importantsKey:"",
    descriptions:"",
    maintenance:"",
    images1:"",
    }

fut={
  mainTitle:""
}

futTwo={
  subTitle:"",
  value:"",
  images:"",
  descriptions:"",


}

    formData: FormData = new FormData(); 

    constructor(private route:ActivatedRoute,private adminFunc:AdminfunctionhallsService) { }
  
    ngOnInit() {
       this.usersID=localStorage.getItem('ads');
      this.usersDetails()
      this.ficilitiesC()
      this.ficilities()
      this.customers()
      this.customersC()
      this.clientsUpdates()
      this.clientsUpdatesCnts()
      this.onlyComments()
       this.onlyCommentsC()
       this.onlyComments()
       this.onlyCommentsC()
       this.futuresGets()
    }
    viewProfile(){
      this.viewsProfiles=!this.viewsProfiles
      this.viewsNoti=false
    }
    viewnoti(){
      this.viewsNoti=!this.viewsNoti
      this.viewsProfiles=false
  
    }
    usersDetails(){ 
      this.adminFunc.usersonly(this.usersID).subscribe((res)=>{
      this.usersall=res
       })
      }
    mainMenuShow(){
  this.showmains=!this.showmains
  this.updatesShows=false
  this.viewupdatesShows=false
  this.onlyCommentsShows=false
  this.ImagesShows=false
  this.ficilitiesShow=false
  this.customersShows=false
  this.feature=false 
}
    updatesShow(){
      this.updatesShows=!this.updatesShows
      this.showmains=false
      this.viewupdatesShows=false
      this.onlyCommentsShows=false
      this.ImagesShows=false
      this.ficilitiesShow=false
      this.customersShows=false
      this.feature=false
    }
    viewupdatesShow(){
      this.viewupdatesShows=!this.viewupdatesShows
      this.updatesShows=false
      this.showmains=false
      this.onlyCommentsShows=false
      this.ImagesShows=false
      this.ficilitiesShow=false
      this.customersShows=false
      this.feature=false
    }
    onlyCommentsShow(){
      this.onlyCommentsShows=!this.onlyCommentsShows
      this.updatesShows=false
      this.showmains=false
      this.viewupdatesShows=false
      this.ImagesShows=false
      this.ficilitiesShow=false
      this.customersShows=false
      this.feature=false
    }
    ImagesShow(){
      
      this.ImagesShows=!this.ImagesShows
      this.updatesShows=false
      this.showmains=false
      this.onlyCommentsShows=false
      this.viewupdatesShows=false
      this.ficilitiesShow=false
      this.customersShows=false
      this.feature=false
    }
    Ficilities(){
      this.ficilitiesShow=!this.ficilitiesShow
      this.updatesShows=false
      this.showmains=false
      this.onlyCommentsShows=false
      this.viewupdatesShows=false
      this.ImagesShows=false
      this.customersShows=false
      this.feature=false
    }
    customersShow(){
      this.customersShows=!this.  customersShows
      this.updatesShows=false
      this.showmains=false
      this.onlyCommentsShows=false
      this.viewupdatesShows=false
      this.ImagesShows=false
      this.ficilitiesShow=false
      this.feature=false
    }
    Features(){
      this.feature=!this.feature
      this.customersShows=false
      this.updatesShows=false
      this.showmains=false
      this.onlyCommentsShows=false
      this.viewupdatesShows=false
      this.ImagesShows=false
      this.ficilitiesShow=false
    }
    handleFileInputServices($event:any,images){
      this.imageName= File = $event.target.files[0];
   }
   fecilitiesPostData(){
      this.formData.append('images1',this.imageName);
      this.formData.append('name',this.fec.name);
      this.formData.append('importantsKey',this.fec.importantsKey);
      this.formData.append('descriptions',this.fec.descriptions);
      this.formData.append('maintenance',this.fec.maintenance);
  
 
       this.adminFunc.postsUpdataFec(this.usersID,this.formData).subscribe((res)=>{
          
           })
           this.fec.name="",
          this.fec.importantsKey="",
          this.fec.descriptions="",
          this.fec.maintenance=""
         
       }

       ficilities(){ 
        this.adminFunc.ficilities(this.usersID).subscribe((res)=>{
        this.hallsFec=res
        
        })
        }
        ficilitiesC(){ 
          this.adminFunc.ficilitiesCount(this.usersID).subscribe((res)=>{
          this.hallsFecnt=res
          
          })
          }
// ********************************************************************** customers **********************************************

handleFileInputCustomers($event:any,images){
  this.imageName= File = $event.target.files[0];
}
customersPostData(){
  this.formData.append('images1',this.imageName);
  this.formData.append('brideName',this.cus.brideName);
  this.formData.append('groomeName',this.cus.groomeName);
  this.formData.append('Place',this.cus.Place);
 

   this.adminFunc.postsUpdatacus(this.usersID,this.formData).subscribe((res)=>{
      
       })
       this.cus.brideName="",
      this.cus.groomeName="",
      this.cus.Place=""
      
   }

   customers(){ 
    this.adminFunc.customers(this.usersID).subscribe((res)=>{
    this.hallscus=res
     })
    }
    customersC(){ 
      this.adminFunc.customersC(this.usersID).subscribe((res)=>{
      this.hallscusc=res
       
      })
      }

// ***************************************************updades **************************************
handleFileInputUpdates($event:any,images){
  this.imageNameupdates= File = $event.target.files[0];
 
}

handleFileInputUpdatess($event:any,viedoes){
  this.videoName= File = $event.target.files[0];
  this.formData.append(viedoes,this.videoName);
 
 
}

updatesPostData(){
this.formData.append('images',this.imageNameupdates);
this.formData.append('title',this.ups.title);
this.formData.append('descriptions',this.ups.descriptions);
 

  console.log(this.formData)

 this.adminFunc.postsUpdata(this.usersID,this.formData).subscribe((res)=>{
    console.log('assssss',res);
   console.log(this.usersID);
   
     })
     this.ups.title="",
     this.ups.descriptions="",
     this.ups.images="",
     this.ups.viedoes=""
}

// ******************************************updates present ***************************************
shareClick(up){
  this.share=!this.share
  this.comments=false
  this.viewcomments=false
  this.replyfrom=false
  this.openshareid=up._id
}
commentsClick(up){
  this.comments=!this.comments
  this.share=false
  this.viewcomments=false
  this.commentspostsId=up._id
   this.commentsGettoClints()
   this.commentsGettoClintsCounts()
 }

viewCommentsClick(up){
  this.viewcomments=!this.viewcomments
  this.share=false
  this.viewcommentsid=up._id
}
replyFromm(comms){
  this.replyfrom=!this.replyfrom
  this.share=false
  this.replyid=comms._id
  this.replyFromCommentesGet()
  console.log(this.replyid);
  
}
commentsPoststoClints(){   
  this.adminFunc.commentsPoststoClints(this.replyid,this.postComments).subscribe((res)=>{
     })
     this.postComments.descriptions=""
 }
 clientsUpdates(){   
  this.adminFunc.clientsUpdatesGet(this.usersID).subscribe((res)=>{
    this.clientUpdates=res
   
   })
   
   
 }

 clientsUpdatesCnts(){   
  this.adminFunc.clientsUpdatesCnts(this.usersID).subscribe((res)=>{
    this.clientUpdatesCnts=res
     
    
   })
 }
  
 
 commentsGettoClints(){   
  this.adminFunc.commentsGettoClints(this.commentspostsId).subscribe((res)=>{
    this.getUsersComments=res

   })
 }

 commentsGettoClintsCounts(){   
  this.adminFunc.commentsGettoClintsCounts(this.commentspostsId).subscribe((res)=>{
    this.getUsersCommentsCounts=res
    })
 }

 replyFromCommentesGet(){   
  this.adminFunc.replyFromCommentesGet(this.replyid).subscribe((res)=>{
    this.replies=res
    console.log(res);
    
    })
 }
// ********************************************************************************
 onlyComments(){   
  this.adminFunc.overallscommentspost(this.usersID).subscribe((res)=>{
    this.onlyComm=res
    console.log(res);
    
    })
 }

 onlyCommentsC(){   
  this.adminFunc.onlyCommentsGetCount(this.usersID).subscribe((res)=>{
    this.onlyCommC=res
    console.log(res);
    
    })
 }

 futuresPosts(){   
  this.adminFunc.futuresPosts(this.usersID,this.fut).subscribe((res)=>{
     })
     this.fut.mainTitle=""
 }

 futuresGets(){   
  this.adminFunc.futuresGets(this.usersID).subscribe((res)=>{
    this.futuresGet=res
    console.log(res);
    
    var furts=this.futuresGet[0]
    this.futuresClickes(furts)
    })
 }

 futuresClickes(gets){
this.futuresGetsId=gets._id
this.futuresGetsName=gets.mainTitle
this.futuresTwoGets()
 }


 handleFileInputFutures($event:any,images){
  this.imageNamess= File = $event.target.files[0];
}
futuresTwoPosts(){
  this.formData.append('images',this.imageNamess);
  this.formData.append('value',this.futTwo.value);
  this.formData.append('subTitle',this.futTwo.subTitle);
  this.formData.append('descriptions',this.futTwo.descriptions);
  this.adminFunc.futuresTwoPosts(this.futuresGetsId,this.formData).subscribe((res)=>{
      })
       this.futTwo.subTitle="",
       this.futTwo.value="",
       this.futTwo.descriptions="",
       this.futTwo.images=""
      
   }
 
 futuresTwoGets(){   
  this.adminFunc.futuresTwoGets(this.futuresGetsId).subscribe((res)=>{
    this.futuresTwoGet=res
    console.log(res);
    
    })
 }
        }
  
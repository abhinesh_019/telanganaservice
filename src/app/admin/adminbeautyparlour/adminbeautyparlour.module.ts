import {MatInputModule} from '@angular/material/input';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material';
import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
 import { AdminbeautyparlourService } from './adminbeautyparlour.service';
import { AdminbeautyparlourComponent } from './adminbeautyparlour.component';


const routes:Routes=[{path:'adminbeautyparlour',component:AdminbeautyparlourComponent},]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    SharedModule
    

  ],
  declarations: [
    AdminbeautyparlourComponent,

  ],
  providers: [AdminbeautyparlourService],
})
export class AdminbeautyparlourModule { }

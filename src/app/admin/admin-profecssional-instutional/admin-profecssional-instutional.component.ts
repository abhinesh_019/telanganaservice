import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
 import { AdminProfessionalInstituionalComponentsService } from './admin-professional-instituional-components.service';

@Component({
  selector: 'app-admin-profecssional-instutional',
  templateUrl: './admin-profecssional-instutional.component.html',
  styleUrls: ['./admin-profecssional-instutional.component.css']
})
export class AdminProfecssionalInstutionalComponent implements OnInit {
 
    public showmains:boolean=false
    public updatesShows:boolean=false
    public viewupdatesShows:boolean=false
    public onlyCommentsShows:boolean=false
    public ImagesShows:boolean=false
    public ProductsShows:boolean=false
    public ProductsViewShows:boolean=false
  
    public usersID:any
  public professionalClients:any
  public viewsProfiles:any
  public viewsNoti:any
  public aboutUsDetai:any
  public getPhilosophyd:any
  public getWhyInst:any
  public getinfraStructures:any
  public getboardsDirector:any
  public getAwardsAndReward:any
  public getcatageriesOnes:any
  public catOneName:any
  public catOneNameId:any
  public getcatageriesTwos:any
  public getcatageriesOnesMain:any
  public getTrackRecordsYear:any
  public catTwoName:any
  public catTwoNameId:any
  public getTrackRecordsYearMain:any
  public catTwoNameMain:any
  public catTwoNameIdMain:any
  public getcatageriesTwosMain:any
  public catTwoNameMains:any
  public catTwoNameIdMains:any
  public trackrecordsIdMain:any
  public trackrecordsNameMain:any
  public trackRecodsName:any
  public trackRecodsNameId:any
  public catOneNameMain:any
  public catOneNameIdMain:any
  public getBranche:any
  public branchName:any
  public branchNameId:any
  public getBrancheMain:any
  public branchNameMain:any
  public branchNameIdMain:any
  public syllabusDetailsGets:any
  public syllabusDetailsGetsMain:any
  public syllabusDetailsName:any
  public syllabusDetailsNameId:any
  public syllabusDetailsNameMain:any
  public syllabusDetailsNameIdMain:any
  public syllabusDetailsTwoGets:any
  public syllabusTwoDetailsGets:any
  public syllabusTwoDetailsName:any
  public syllabusTwoDetailsNameId:any
  public syllabusTwoDetailsGetsMain:any
  public syllabusTwoDetailsNameMain:any
  public syllabusTwoDetailsNameIdMain:any
  public elegibilitiesGetsMain:any
  public elegibilitiesTwoDetailsNameId:any
  public elegibilitiesTwoDetailsName:any
  public elegibilitiesGets:any
  public organizationGet:any
  public organizationsGetsMain:any
  public organizationsNameMain:any
  public organizationsNameIdMain:any
  public importantNotificationsGets:any
  public importantNotificationsName:any
  public importantNotificationsNameId:any
  public importantNotificationsGetsMain:any
  public importantNotificationsGetsMainNameMain:any
  public importantNotificationsGetsMainNameId:any
  public importantNotificationsGetsMainc:any
  public getMaterials:any
  public MaterialsName:any
  public MaterialsNameId:any
  public getMaterialMain:any
  public materialsMainNameMains:any
  public materialsMainNameIdMains:any
  public newBatchGetss:any
  public newBatchName:any
  public newBatchNameId:any
  public getnewBatchMains:any
  public newBatchNameMains:any
  public newBatchNameIdMains:any
  public admissionsGetGets:any
  public admissionsName:any
  public admissionsNameId:any
  public admissionsGetsMain:any
  public admissionsGetsMainName:any
  public admissionsGetsMainNameId:any
  public freeStructureGetGets:any
  public admissionssName:any
  public admissionssNameId:any
  public freeStructureGetsMain:any
  public freeStructureGetsMainNameId:any
  public freeStructureGetsMainName:any
  public batchesGets:any
  public batchesName:any
  public batchesNameId:any
    public batchesGetsMain:any
    public batchesGetsMainName:any
    public batchesGetsMainNameId:any
    public facultyGetss:any
    public facultyName:any
    public facultyNameId:any
    public getfacultyMains:any
    public testAnaysisGetss:any
    public ftestAnaysisGetss:any
    public testAnaysisName:any
    public testAnaysisNameId:any
    public gettestAnaysisMains:any
    public computerLabGetss:any
    public computerLabName:any
    public computerLabNameId:any
    public computerLabMains:any
    public libraryOneGets:any
    public libraryOneName:any
    public libraryOneNameId:any
    public libraryOneGetsMain:any
    public libraryOneGetsMainName:any
    public libraryOneGetsMainNameId:any
    public libraryTwoGets:any
    public libraryTwoName:any
    public libraryTwoNameId:any
    public libraryTwoGetsMain:any
    public libraryTwoGetsMainName:any
    public libraryTwoGetsMainNameId:any
    public computerLabThreeGetss:any
    public computerLabThreeName:any
    public computerLabThreeNameId:any
    public computerLabThreeMains:any
   public trackRecordsGetss:any
   public trackRecordsName:any
   public trackRecordsNameId:any
   public trackRecordsMains:any
  //  public libraryTwoGetsMainName:any
  //  public libraryTwoGetsMainName:any
  //  public libraryTwoGetsMainName:any
  //  public libraryTwoGetsMainName:any
   public imageNameupdates:any
  public videoName:any
  public openshareid:any
  public commentspostsId:any
  public viewcommentsid:any
  public clientUpdates:any
  public replyid:any
  public clientUpdatesCount:any
   public getUsersComments:any
  public getUsersCommentsCounts:any
  public share:boolean=false
  public comments:boolean=false
  public viewcomments:boolean=false
  public replyfrom:boolean=false 
  public replies:any
  public onlyCommC:any
  public onlyComm:any
  
  abouts={
    aboutUsDescriptionsOne:"",
    aboutUsDescriptionsTwo:"",
    managmentTeams:"",
    managmentTeamsDecriptionsOne:"",
    managmentTeamsDecriptionsTwo:"",
    visonMissionTitle:"",
    visonMissionDescriptionsOne:"",
    visonMissionDescriptionsTwo:"",
    managmentTeams1:"",
    managmentTeams2:"",
    managmentTeams3:"",
    managmentTeams4:"",
    managmentTeamImg1:"",
    managmentTeamImg2:"",
    managmentTeamImg3:"",
    managmentTeamImg4:"",
    visonMissionImg:"",
    awardsAndRewardsDescriptions:"",
   
  }
  philo={
    mainTitle:"",
    descriptionsOne:"",
    descriptionsTwo:"",
   importantNote:"",
  }

  whyInst={
    mainTtitle:"",
    titleDescriptions:"",
    subTtitle:"",
    subTtitleDescriptions:"",
    subTtitleDescriptionsOne:"",
    subTtitleDescriptionsTwo:"",
    subTtitleDescriptionsThree:"",
    subTtitleDescriptionsFour:"",
    images:"",
  }

  info={
    descriptionsOne:"",
    descriptionsTwo:"",
    images:"",
    title:"",
  }
dirc={
  mainTitle:"",
    name:"",
    aboutDirector:"",
    importantNote:"",
    directorImg:"",
}
awards={
  mainTitle:"",
     descriptions:"",
      images:"",
}
serOne={
  courseName:"",
  descriptions:"",
  images:"",
}

serTwo={
  catageriesOne:"",
    subCourseName:"",
    descriptions1:"",
    descriptions2:"",
    images:"",
}

yearsT={
  yearOfSelection:"",
}

branches={
  branchName:"",
  branchImage:"",
  houseNo:"",
  streetNo:"",
  area:"",
  city:"",
  state:"",
  picode:"",
  contactNo:"",
}

syllOne={
  mainTitle:"",
  simpleText:"",
 subTitle:"",
 
}
syllTwo={
  
     syllabusDescriptions1:"",
     syllabusDescriptions2:"",
     syllabusDescriptions3:"",
}

elegibilies={
  mainTitle:"",
  subTitle:"",
  mainTitleDescriptionsOne:"",
  mainTitleDescriptionsTwo:"",
  descriptionsTitle1:"",
  descriptionsTitle2:"",
  descriptionsTitle3:"",
  descriptionsImportantPoints1:"",
  descriptionsImportantPoints2:"",
  descriptionsImportantPoints3:"",
 descriptions1:"",
 descriptions2:"",
 descriptions3:"",
 note1:"",
 note2:"",
}

orgs={
  organizationName:"",
  noOfAttempts:"",
  ageLtms:"",
  basicQualificationalDetails:"",
  descriptions1:"",
  descriptions2:"",
  descriptions3:"",
  descriptions4:"",

}
imp={
  mainTitle:"",
  subTitle:"",
  descriptionsOne:"",
  note:"",
}

mate={
  mainTitle:"",
  descriptions:"",
  subtitle:"", 
  images:"",
}
newBranch={
  demoCode:"",
  demoTiming:"",
  demoName:"",
  demoDate:"",
  BatchDuration:"",
  EndDate:"",
   locations:"",
   demoLectureName:"",
   demoLectureIntro:"",
   demoLectureImg:"",
}

admins={
  mainTitle:"",
     admissionProcessType:"",
     necessaryRequireTitle:"",
    necessaryRequire1:"",
    necessaryRequire2:"",
    necessaryRequireDescription1:"",
    necessaryRequireDescription2:"",
    mainDescriptions:"",
impNote1:"",
impNote2:"",
}

fress={
  mainTitle:"",
  batchName:"",
  subtitle1:"",
  subtitle2:"",
  subtitle3:"",
  subtitle4:"",
  subtitle5:"",
  subtitle6:"",
  subtitle7:"",
  subtitle8:"",
  subtitleText1:"",
  subtitleText2:"",
  subtitleText3:"",
  subtitleText4:"",
  subtitleText5:"",
  subtitleText6:"",
  subtitleText7:"",
  subtitleText8:"",
  note:"",
}

batches={
  batchName:"",
  branchShift:"",
  branchTimmingsStart:"",
  branchTimmingsEnding:"",
  batchDurations:"",
  totalBatchDurations:"",
  batchDescription1:"",
  batchDescription2:"",
  batchDescription3:"",
  batchDescription4:"",
}

fatculty={
  facultyName:"",
  facultyQualifications:"",
  facultyDescriptions:"",
  facultyImg:"",
}

test={
  name:"",
  place:"",
  hallTicketNo:"",
  markSecured:"",
  TotalMarks:"",
  studentImg:"",
  studentQualification:"",
  description:"",
}

computers={
  computerLabOne:"",
    computerLabImg:"",
    descriptions:"",
}

libraryOne={
  libraryQualification:""
}
department={
  libraryDepartment:"",
}

libT={
  booksMainTitle:"",
     booksAuthor:"",
     booksName:"",
     booksImg:"",
}

tracks={
  studentName:"",
    place:"",
    studentImg:"",
marks:"",
hallTicket:"",
elegibliityPost:"",
coursedJoined:"",
studentQualification:"",
yearOfSelection:"",
   AIR:"", 
}

ups={
  images:"",
    title:"",
    description:"",
    viedoes:"",
}

postComments={
  "descriptions":""
}


  formData: FormData = new FormData(); 
  
  
  constructor(private route:ActivatedRoute,private pi:AdminProfessionalInstituionalComponentsService) { }
   ngOnInit() {
      this.usersID=localStorage.getItem('ads');
  this.usersDetails()
  this.aboutUs()
  this.getPhilosophy()
  this.getWhyInsts()
  this.getinfraStructure()
  this.getboardsDirectors()
  this.getAwardsAndRewards()
  this.getcatageriesOne()
this.getcatageriesOneMain()
this.clientsUpdatesCount()
this.clientsUpdates()
this.onlyComments()
this.onlyCommentsC()
    }
    usersDetails(){ 
      this.pi.usersDetail(this.usersID).subscribe((res)=>{
      this.professionalClients=res
       
      })
      }
      viewProfile(){
        this.viewsProfiles=!this.viewsProfiles
        this.viewsNoti=false
      }
      viewnoti(){
        this.viewsNoti=!this.viewsNoti
        this.viewsProfiles=false
    
      }
      
      mainMenuShow(){
        this.showmains=!this.showmains
        this.updatesShows=false
        this.viewupdatesShows=false
        this.onlyCommentsShows=false
        this.ImagesShows=false
        this.ProductsShows=false
        this.ProductsViewShows=false
      }
      updatesShow(){
        this.updatesShows=!this.updatesShows
        this.showmains=false
        this.viewupdatesShows=false
        this.onlyCommentsShows=false
        this.ImagesShows=false
        this.ProductsShows=false
        this.ProductsViewShows=false
      }
      viewupdatesShow(){
        this.viewupdatesShows=!this.viewupdatesShows
        this.updatesShows=false
        this.showmains=false
        this.onlyCommentsShows=false
        this.ImagesShows=false
        this.ProductsShows=false
        this.ProductsViewShows=false
      }
      onlyCommentsShow(){
        this.onlyCommentsShows=!this.onlyCommentsShows
        this.updatesShows=false
        this.showmains=false
        this.viewupdatesShows=false
        this.ImagesShows=false
        this.ProductsShows=false
        this.ProductsViewShows=false
      }
      ImagesShow(){
        
        this.ImagesShows=!this.ImagesShows
        this.updatesShows=false
        this.showmains=false
        this.onlyCommentsShows=false
        this.viewupdatesShows=false
        this.ProductsShows=false
        this.ProductsViewShows=false
      }
      ProductsShow(){
        this.ProductsShows=!this.ProductsShows
        this.updatesShows=false
        this.showmains=false
        this.onlyCommentsShows=false
        this.viewupdatesShows=false
        this.ImagesShows=false
        this.ProductsViewShows=false
      }
      ProductsViewShow(){
        this.ProductsViewShows=!this.ProductsViewShows
        this.updatesShows=false
        this.showmains=false
        this.onlyCommentsShows=false
        this.viewupdatesShows=false
        this.ImagesShows=false
        this.ProductsShows=false
      }

      // *************************************************** about us main post and get **************************************
 
handleFileInputDescriptionsTypeOne($event:any,img) {
  let imageFilePlantsOne= File = $event.target.files[0];
  this.formData.append(img,imageFilePlantsOne);
    
}
handleFileInputDescriptionsTypeTwo($event:any,img) {
  let imageFilePlantsTwo= File = $event.target.files[0];
  this.formData.append(img,imageFilePlantsTwo);
    
}
handleFileInputDescriptionsTypeThree($event:any,img) {
  let imageFilePlantsThree= File = $event.target.files[0];
  this.formData.append(img,imageFilePlantsThree);
    
}

handleFileInputDescriptionsTypeFour($event:any,img) {
  let visonMissionImg= File = $event.target.files[0];
  this.formData.append(img,visonMissionImg);
    
}
handleFileInputDescriptionsTypeFive($event:any,img) {
  let visonMissionImgfive= File = $event.target.files[0];
  this.formData.append(img,visonMissionImgfive);
    
}
postPlantsDescriptionsDetails(){
  this.formData.append('aboutUsDescriptionsOne',this.abouts.aboutUsDescriptionsOne);
 this.formData.append('aboutUsDescriptionsTwo',this.abouts.aboutUsDescriptionsTwo);
this.formData.append('managmentTeams',this.abouts.managmentTeams);
this.formData.append('managmentTeamsDecriptionsOne',this.abouts.managmentTeamsDecriptionsOne);
this.formData.append('managmentTeamsDecriptionsTwo',this.abouts.managmentTeamsDecriptionsTwo);
this.formData.append('visonMissionTitle',this.abouts.visonMissionTitle);
this.formData.append('visonMissionDescriptionsOne',this.abouts.visonMissionDescriptionsOne);
this.formData.append('visonMissionDescriptionsTwo',this.abouts.visonMissionDescriptionsTwo);
this.formData.append('managmentTeams1',this.abouts.managmentTeams1);
this.formData.append('managmentTeams2',this.abouts.managmentTeams2);
this.formData.append('managmentTeams3',this.abouts.managmentTeams3);
this.formData.append('managmentTeams4',this.abouts.managmentTeams4);
this.formData.append('awardsAndRewardsDescriptions',this.abouts.awardsAndRewardsDescriptions);

 
this.pi.postPlantsDescriptionsDetail(this.usersID,this.formData).subscribe((res)=>{

 })
 this.abouts.aboutUsDescriptionsOne="",
 this.abouts.aboutUsDescriptionsTwo="",
 this.abouts.managmentTeams="",
 this.abouts.managmentTeamsDecriptionsOne="",
 this.abouts.managmentTeamsDecriptionsTwo="",
 this.abouts.visonMissionTitle="",
 this.abouts.visonMissionDescriptionsOne="",
 this.abouts.visonMissionDescriptionsTwo="",
 this.abouts.managmentTeams1="",
 this.abouts.managmentTeams2="",
 this.abouts.managmentTeams3="",
 this.abouts.managmentTeams4="",
 this.abouts.awardsAndRewardsDescriptions=""
}

aboutUs(){ 
  this.pi.aboutUsDetais(this.usersID).subscribe((res)=>{
  this.aboutUsDetai=res
   
  })
  }
      // *****************************************************************************************
            // *************************************************** about us main post and get **************************************

      OurPhilosophy(){   
        this.pi.OurPhilosophys(this.usersID,this.philo).subscribe((res)=>{
           })
           this.philo.descriptionsOne="",
           this.philo.descriptionsTwo="",
           this.philo.importantNote="",
           this.philo.mainTitle=""
       }
       getPhilosophy(){ 
        this.pi.getPhilosophys(this.usersID).subscribe((res)=>{
        this.getPhilosophyd=res
        })
        }
              // *****************************************************************************************
          
              // *************************************************** about us Why This Ins Two **************************************
            handleFileInputImagesWhyInst($event:any,img) {
              let imagesWhyInst= File = $event.target.files[0];
              this.formData.append(img,imagesWhyInst);
                
            }
            posWhyInst(){
              this.formData.append('mainTtitle',this.whyInst.mainTtitle);
             this.formData.append('titleDescriptions',this.whyInst.titleDescriptions);
            this.formData.append('subTtitle',this.whyInst.subTtitle);
            this.formData.append('subTtitleDescriptions',this.whyInst.subTtitleDescriptions);
            this.formData.append('subTtitleDescriptionsOne',this.whyInst.subTtitleDescriptionsOne);
            this.formData.append('subTtitleDescriptionsTwo',this.whyInst.subTtitleDescriptionsTwo);
            this.formData.append('subTtitleDescriptionsThree',this.whyInst.subTtitleDescriptionsThree);
             this.formData.append('subTtitleDescriptionsFour',this.whyInst.subTtitleDescriptionsFour);
            this.formData.append('images',this.whyInst.images);
             
            
             
            this.pi.posWhyInsts(this.usersID,this.formData).subscribe((res)=>{
            
             })
             this.whyInst.mainTtitle="",
             this.whyInst.titleDescriptions="",
             this.whyInst.subTtitle="",
             this.whyInst.subTtitleDescriptions="",
             this.whyInst.subTtitleDescriptionsOne="",
             this.whyInst.subTtitleDescriptionsTwo="",
             this.whyInst.subTtitleDescriptionsThree="",
             this.whyInst.subTtitleDescriptionsFour="",
             this.whyInst.images=""
           
            }
            getWhyInsts(){ 
              this.pi.getWhyInsts(this.usersID).subscribe((res)=>{
              this.getWhyInst=res
              })
              }
          // *****************************************************************************************
// *************************************************** about us INFRASTRUCTURE **************************************
handleFileInputImagesInfraStructure($event:any,img) {
  let imagesinfraStructure= File = $event.target.files[0];
  this.formData.append(img,imagesinfraStructure);
    
}
infraStructurePosts(){
  this.formData.append('descriptionsOne',this.info.descriptionsOne);
 this.formData.append('descriptionsTwo',this.info.descriptionsTwo);
this.formData.append('title',this.info.title);

this.pi.infraStructurePost(this.usersID,this.formData).subscribe((res)=>{

 })
 this.info.descriptionsOne="",
 this.info.descriptionsTwo="",
 this.info.title="",
 this.info.images=""

}
getinfraStructure(){ 
  this.pi.getinfraStructure(this.usersID).subscribe((res)=>{
  this.getinfraStructures=res
  })
  }
// *****************************************************************************************
// *************************************************** about us Board Of Directors **************************************
handleFileInputImagesBoardOfDirectors($event:any,img) {
  let imagesboardsDirectors= File = $event.target.files[0];
  this.formData.append(img,imagesboardsDirectors);
    
}
boardsDirectorsPosts(){
  this.formData.append('mainTitle',this.dirc.mainTitle);
 this.formData.append('name',this.dirc.name);
this.formData.append('aboutDirector',this.dirc.aboutDirector);
this.formData.append('importantNote',this.dirc.importantNote);
 

this.pi.boardsDirectorsPost(this.usersID,this.formData).subscribe((res)=>{

 })
 this.dirc.mainTitle="",
 this.dirc.name="",
 this.dirc.aboutDirector="",
 this.dirc.importantNote="",
 this.dirc.directorImg=""

}
getboardsDirectors(){ 
  this.pi.getboardsDirector(this.usersID).subscribe((res)=>{
  this.getboardsDirector=res
  })
  }
// *****************************************************************************************
// *************************************************** about us Awards & Rewards  **************************************
handleFileInputImagesAwardsAndRewards($event:any,img) {
  let imagesAwardsRewards= File = $event.target.files[0];
  this.formData.append(img,imagesAwardsRewards);   
}
awardsAndRewardsPosts(){
  this.formData.append('mainTitle',this.awards.mainTitle);
 this.formData.append('descriptions',this.awards.descriptions);
this.pi.AwardsAndRewardsPost(this.usersID,this.formData).subscribe((res)=>{
 })
 this.awards.mainTitle="",
 this.awards.descriptions=""
}
getAwardsAndRewards(){ 
  this.pi.getAwardsAndRewards(this.usersID).subscribe((res)=>{
  this.getAwardsAndReward=res
  })
  }
// *****************************************************************************************
// *************************************************** serviceOne  **************************************
handleFileInputImageServiceOne($event:any,img) {
  let imagescatageriesOne= File = $event.target.files[0];
  this.formData.append(img,imagescatageriesOne);   
}
catageriesOnePosts(){
  this.formData.append('courseName',this.serOne.courseName);
 this.formData.append('descriptions',this.serOne.descriptions);
 
this.pi.catageriesOnePost(this.usersID,this.formData).subscribe((res)=>{
 })
 this.serOne.courseName="",
 this.serOne.descriptions="",
 this.serOne.images=""
}
getcatageriesOne(){ 
  this.pi.getcatageriesOne(this.usersID).subscribe((res)=>{
  this.getcatageriesOnes=res
  var catOne=this.getcatageriesOnes[0]
  catOne(catOne)
  })
  }
         catOne(get?){
          this.catOneName=get.courseName
          this.catOneNameId=get._id
          this.getcatageriesTwo()
                     }

                     getcatageriesOneMain(){ 
                      this.pi.getcatageriesOne(this.usersID).subscribe((res)=>{
                      this.getcatageriesOnesMain=res
                       })
                      }
                             catOneMain(get?){
                              this.catOneNameMain=get.courseName
                              this.catOneNameIdMain=get._id
                               this.getcatageriesTwoMain()
                                          }

// ************************************************************************************************************************************
// *************************************************** serviceTwo  **************************************
handleFileInputImageServiceTwo($event:any,img) {
  let imagescatageriesTwo= File = $event.target.files[0];
  this.formData.append(img,imagescatageriesTwo);   
}
catageriesTwoPosts(){
  this.formData.append('catageriesOne',this.serTwo.catageriesOne);
 this.formData.append('subCourseName',this.serTwo.subCourseName);
 this.formData.append('descriptions1',this.serTwo.descriptions1);
 this.formData.append('descriptions2',this.serTwo.descriptions2);


this.pi.catageriesTwoPost(this.catOneNameId,this.formData).subscribe((res)=>{
 })
 this.serTwo.catageriesOne="",
 this.serTwo.subCourseName="",
 this.serTwo.descriptions1="",
 this.serTwo.descriptions2="",
 this.serTwo.images=""
}
getcatageriesTwo(){ 
  this.pi.getcatageriesTwo(this.catOneNameId).subscribe((res)=>{
  this.getcatageriesTwos=res
  var serTwo = this.getcatageriesTwos[0]
  this.catTwo(serTwo)
    })
  }
  catTwo(get?){
    this.catTwoName=get.subCourseName
    this.catTwoNameId=get._id
    this.getTrackRecordsYears()
               }



               getcatageriesTwoMain(){ 
                this.pi.getcatageriesTwo(this.catOneNameIdMain).subscribe((res)=>{
                this.getcatageriesTwosMain=res
                var serTwoMain=this.getcatageriesTwosMain[0]
                this.catTwoMain(serTwoMain)
                  })
                }
                catTwoMain(get?){
                  this.catTwoNameMains=get.courseName
                  this.catTwoNameIdMains=get._id
                   this.getTrackRecordsYearsMain()
                             }

// ************************************************************************************************************************************
 // *************************************************** trackRecordsYears **************************************

          trackRecordsYears(){   
            this.pi.trackRecordsYears(this.catTwoNameId,this.yearsT).subscribe((res)=>{
               })
               this.yearsT.yearOfSelection=""
               
           }
           getTrackRecordsYears(){ 
            this.pi.getTrackRecordsYears(this.catTwoNameId).subscribe((res)=>{
            this.getTrackRecordsYear=res
            var years=this.getTrackRecordsYear[0]
this.yearsClick(years)
            })
            }

            yearsClick(get?){
    this.trackRecodsName=get.yearOfSelection
    this.trackRecodsNameId=get._id
    
    this.getBranches()
               }

               getTrackRecordsYearsMain(){ 
                this.pi.getTrackRecordsYears(this.catTwoNameIdMains).subscribe((res)=>{
                this.getTrackRecordsYearMain=res
                
                var yearsMain=this.getTrackRecordsYearMain[0]
    this.yearsClickMain(yearsMain)
                })
                }
    
                yearsClickMain(get?){
        this.trackrecordsNameMain=get.yearOfSelection
        this.trackrecordsIdMain=get._id
 
        this.getBranchesMain()
                    }
 // ************************************************************************************************************************************
       

  // *************************************************** Branch **************************************
  handleFileInputImageBranches($event:any,img) {
    let imagesBranches= File = $event.target.files[0];
    this.formData.append(img,imagesBranches);   
  }
   branchesPosts(){
    this.formData.append('branchName',this.branches.branchName);
    this.formData.append('houseNo',this.branches.houseNo);
    this.formData.append('streetNo',this.branches.streetNo);
    this.formData.append('area',this.branches.area);
    this.formData.append('city',this.branches.city);
    this.formData.append('state',this.branches.state);
    this.formData.append('picode',this.branches.picode);
    this.formData.append('contactNo',this.branches.contactNo);
  this.pi.branchesPosts(this.trackRecodsNameId,this.formData).subscribe((res)=>{
   })
   this.branches.branchName="",
   this.branches.houseNo="",
   this.branches.streetNo="",
   this.branches.area="",
   this.branches.city="",
   this.branches.state="",
   this.branches.picode="",
   this.branches.branchImage="",
   this.branches.contactNo=""
  }
  getBranches(){ 
    this.pi.getBranches(this.trackRecodsNameId).subscribe((res)=>{
    this.getBranche=res
     var branch=this.getBranche[0]
this.branchClick(branch)
    })
    }

    branchClick(get?){
this.branchName=get.branchName
this.branchNameId=get._id
console.log(this.branchNameId);

this.syllabusDetailsGet()
this.ElegibilitiesGet()
this.organizationsGet()
this.importantNotificationsGet()
this.getmaterials()
this.newBatchGet()
this.admissionsGet()
this.freeStructureGet()
this.batchesGet()
   }

  getBranchesMain(){ 
    this.pi.getBranches(this.trackrecordsIdMain).subscribe((res)=>{
    this.getBrancheMain=res
    
    var branchMain=this.getBrancheMain[0]
this.branchClickMain(branchMain)
    })
    }

    branchClickMain(get?){
this.branchNameMain=get.branchName
this.branchNameIdMain=get._id
console.log(this.branchNameIdMain);
this.syllabusDetailsGetMain()
this.elegibilitiesGetClickGetMain()
this.organizationsGetClickGetMain()
this.importantNotificationGetMain()
this.importantNotificationGetMainC()
this.getMaterialsMain()
this.getnewBatchMain()
this.admissionsGetMain()
this.freeStructureGetMain()
this.batchesGetMain()
   }
   // ************************************************************************************************************************************


     // *************************************************** syllabusDetails **************************************

     syllabusDetails(){   
      this.pi.syllabusDetailsp(this.branchNameId,this.syllOne).subscribe((res)=>{
         })
         this.syllOne.mainTitle="",
         this.syllOne.simpleText="",
         this.syllOne.subTitle=""
          
         
     }
     syllabusDetailsGet(){ 
      this.pi.syllabusDetailsGet(this.branchNameId).subscribe((res)=>{
      this.syllabusDetailsGets=res
      var syllby=this.syllabusDetailsGets[0]
      this.syllabusDetailsClick(syllby)
      })
      }
      syllabusDetailsClick(get?){
        this.syllabusDetailsName=get.subTitle
        this.syllabusDetailsNameId=get._id
        this.syllabusTwoDetailsGet()
           }
      
      syllabusDetailsGetMain(){ 
        this.pi.syllabusDetailsGet(this.branchNameIdMain).subscribe((res)=>{
        this.syllabusDetailsGetsMain=res
        var syllOnes=this.syllabusDetailsGetsMain[0]
        this.syllabusDetailsMainClick(syllOnes)
        })
        }
        syllabusDetailsMainClick(get?){
          this.syllabusDetailsNameMain=get.subTitle
          this.syllabusDetailsNameIdMain=get._id
          this.syllabusTwoDetailsGetMain()
             }


      // *****************************************************************************************
        // *************************************************** syllabusTwoDetails **************************************

     syllabusTwoDetails(){   
      this.pi.syllabusTwoDetailsp(this.syllabusDetailsNameId,this.syllTwo).subscribe((res)=>{
         })
          
         this.syllTwo.syllabusDescriptions1="",
         this.syllTwo.syllabusDescriptions2="",
         this.syllTwo.syllabusDescriptions3=""
         
     }
     syllabusTwoDetailsGet(){ 
      this.pi.syllabusTwoDetailsGet(this.syllabusDetailsNameId).subscribe((res)=>{
      this.syllabusDetailsTwoGets=res
      var syllby=this.syllabusTwoDetailsGets[0]
      this.syllabusTwoDetailsClick(syllby)
      })
      }
      syllabusTwoDetailsClick(get?){
        this.syllabusTwoDetailsName=get.subTitle
        this.syllabusTwoDetailsNameId=get._id
           }
      
      syllabusTwoDetailsGetMain(){ 
        this.pi.syllabusTwoDetailsGet(this.syllabusDetailsNameIdMain).subscribe((res)=>{
        this.syllabusTwoDetailsGetsMain=res
        var syllOnes=this.syllabusTwoDetailsGetsMain[0]
        this.syllabusTwoDetailsMainClick(syllOnes)
        })
        }
        syllabusTwoDetailsMainClick(get?){
          this.syllabusTwoDetailsNameMain=get.subTitle
          this.syllabusTwoDetailsNameIdMain=get._id
             }


      // *****************************************************************************************
                // *************************************************** Elegibilities **************************************
     
                ElegibilitiesPosts(){   
                  this.pi.ElegibilitiesPost(this.branchNameId,this.elegibilies).subscribe((res)=>{
                     })
                      
                     this.elegibilies.mainTitle="",
                     this.elegibilies.subTitle="",
                     this.elegibilies.mainTitleDescriptionsOne="",
                     this.elegibilies.mainTitleDescriptionsTwo="",
                     this.elegibilies.descriptionsTitle1="",
                     this.elegibilies.descriptionsTitle2="",
                     this.elegibilies.descriptionsTitle3="",
                     this.elegibilies.descriptionsImportantPoints1="",
                     this.elegibilies.descriptionsImportantPoints2="",
                     this.elegibilies.descriptionsImportantPoints3="",
                     this.elegibilies.descriptions1="",
                     this.elegibilies.descriptions2="",
                     this.elegibilies.descriptions3="",
                     this.elegibilies.note1="",
                     this.elegibilies.note2=""

                 }
                 ElegibilitiesGet(){ 
                  this.pi.elegibilitiesGet(this.branchNameId).subscribe((res)=>{
                  this.elegibilitiesGets=res
                  var syllby=this.elegibilitiesGets[0]
                  this.ElegibilitiesGetClick(syllby)
                  })
                  }
                  ElegibilitiesGetClick(get?){
                    this.elegibilitiesTwoDetailsName=get.subTitle
                    this.elegibilitiesTwoDetailsNameId=get._id
                       }
                  
                       elegibilitiesGetClickGetMain(){ 
                    this.pi.elegibilitiesGet(this.branchNameIdMain).subscribe((res)=>{
                    this.elegibilitiesGetsMain=res
                    var syllOnes=this.elegibilitiesGetsMain[0]
                    this.elegibilitiesTwoDetailsMainClick(syllOnes)
                    })
                    }
                    elegibilitiesTwoDetailsMainClick(get?){
                      this.syllabusTwoDetailsNameMain=get.subTitle
                      this.syllabusTwoDetailsNameIdMain=get._id
                         }
     



                // *****************************************************************************************

                // *************************************************** organizations **************************************

                organizationsPosts(){   
                  this.pi.organizationsPost(this.branchNameId,this.orgs).subscribe((res)=>{
                     })
                     this.orgs.organizationName="",
                     this.orgs.noOfAttempts="",
                     this.orgs.ageLtms="",
                     this.orgs.basicQualificationalDetails="",
                     this.orgs.descriptions1="",
                     this.orgs.descriptions2="",
                     this.orgs.descriptions3=""
            }
                 organizationsGet(){ 
                  this.pi.organizationsGets(this.branchNameId).subscribe((res)=>{
                  this.organizationGet=res
                  var syllby=this.organizationGet[0]
                  this.organizationsGetClick(syllby)
                  })
                  }
                  organizationsGetClick(get?){
                    this.elegibilitiesTwoDetailsName=get.subTitle
                    this.elegibilitiesTwoDetailsNameId=get._id
                       }
                  
                       organizationsGetClickGetMain(){ 
                    this.pi.organizationsGets(this.branchNameIdMain).subscribe((res)=>{
                    this.organizationsGetsMain=res
                    var syllOnes=this.organizationsGetsMain[0]
                    this.elegibilitiesTwoDetailsMainClick(syllOnes)
                    })
                    }
                    organizationsMainClick(get?){
                      this.organizationsNameMain=get.subTitle
                      this.organizationsNameIdMain=get._id
                         }
     




                // *****************************************************************************************
               
               
                // *************************************************** importantNotifications **************************************
               
                importantNotificationssPosts(){   
                  this.pi.importantNotificationsPost(this.branchNameId,this.imp).subscribe((res)=>{
                     })
                     this.imp.mainTitle="",
                     this.imp.subTitle="",
                     this.imp.descriptionsOne="",
                     this.imp.note=""
                  
            }
            importantNotificationsGet(){ 
                  this.pi.importantNotificationsGets(this.branchNameId).subscribe((res)=>{
                  this.importantNotificationsGets=res
                  var impNot=this.importantNotificationsGets[0]
                  this.importantNotificationsGetClick(impNot)
                  })
                  }
                  importantNotificationsGetClick(get?){
                    this.importantNotificationsName=get.subTitle
                    this.importantNotificationsNameId=get._id
                       }
                  
                       importantNotificationGetMain(){ 
                    this.pi.importantNotificationsGets(this.branchNameIdMain).subscribe((res)=>{
                    this.importantNotificationsGetsMain=res
                    var syllOnes=this.importantNotificationsGetsMain[0]
                    this.importantNotificationsMainClick(syllOnes)
                    })
                    }
                    importantNotificationGetMainC(){ 
                      this.pi.importantNotificationsGetsc(this.branchNameIdMain).subscribe((res)=>{
                      this.importantNotificationsGetsMainc=res
                      
                      })
                      }
                    importantNotificationsMainClick(get?){
                      this.importantNotificationsGetsMainNameMain=get.subTitle
                      this.importantNotificationsGetsMainNameId=get._id
                         }

                // *****************************************************************************************


// *************************************************** newBranch  **************************************
handleFileInputImageMaterials($event:any,img) {
  let imagesnewBranch= File = $event.target.files[0];
  this.formData.append(img,imagesnewBranch);   
}
materialsTwoPosts(){
  this.formData.append('mainTitle',this.mate.mainTitle);
 this.formData.append('descriptions',this.mate.descriptions);
 this.formData.append('subtitle',this.mate.subtitle);
 

this.pi.materialsTwoPost(this.branchNameId,this.formData).subscribe((res)=>{
 })
 this.mate.mainTitle="",
 this.mate.descriptions="",
 this.mate.subtitle="",
  this.mate.images=""
}
getmaterials(){ 
  this.pi.getmaterials(this.branchNameId).subscribe((res)=>{
  this.getMaterials=res
  var serTwo = this.getMaterials[0]
  this.getMaterialsClick(serTwo)
    })
  }
  getMaterialsClick(get?){
    this.MaterialsName=get.mainTitle
    this.MaterialsNameId=get._id
                }



               getMaterialsMain(){ 
                this.pi.getmaterials(this.branchNameIdMain).subscribe((res)=>{
                this.getMaterialMain=res
                var materials=this.getMaterialMain[0]
                this.materialsMain(materials)
                  })
                }
                materialsMain(get?){
                  this.materialsMainNameMains=get.mainTitle
                  this.materialsMainNameIdMains=get._id
                              }

// ************************************************************************************************************************************
// *************************************************** newBatch  **************************************
handleFileInputImagenewBatch($event:any,img) {
  let imagesdemoLec= File = $event.target.files[0];
  this.formData.append(img,imagesdemoLec);   
}
newBatchPosts(){
  this.formData.append('demoCode',this.newBranch.demoCode);
 this.formData.append('demoTiming',this.newBranch.demoTiming);
 this.formData.append('demoName',this.newBranch.demoName);
 this.formData.append('demoDate',this.newBranch.demoDate);
 this.formData.append('BatchDuration',this.newBranch.BatchDuration);
 this.formData.append('EndDate',this.newBranch.EndDate);
 this.formData.append('locations',this.newBranch.locations);
 this.formData.append('demoLectureName',this.newBranch.demoLectureName);
 this.formData.append('demoLectureIntro',this.newBranch.demoLectureIntro);
this.pi.newBatchPosts(this.branchNameId,this.formData).subscribe((res)=>{
  console.log(this.branchNameId);
  
 })
 this.newBranch.demoCode="",
 this.newBranch.demoTiming="",
 this.newBranch.demoName="",
  this.newBranch.demoDate="",
  this.newBranch.BatchDuration="",
  this.newBranch.EndDate="",
  this.newBranch.locations="",
   this.newBranch.demoLectureName="",
   this.newBranch.demoLectureIntro=""
 

}
newBatchGet(){ 
  this.pi.newBatchGets(this.branchNameId).subscribe((res)=>{
  this.newBatchGetss=res
  console.log(res);
  var serTwo = this.newBatchGetss[0]
  this.getMaterialsClick(serTwo)
    })
  }
  getnewBatchClick(get?){
    this.newBatchName=get.demoCode
    this.newBatchNameId=get._id
                }

    getnewBatchMain(){ 
                this.pi.newBatchGets(this.branchNameIdMain).subscribe((res)=>{
                this.getnewBatchMains=res
                console.log(res);
                  })
                }
              

// ************************************************************************************************************************************


  // *************************************************** admissions **************************************
               
  admissionsPosts(){   
    this.pi.admissionsPosts(this.branchNameId,this.admins).subscribe((res)=>{
       })
       this.admins.mainTitle="",
       this.admins.admissionProcessType="",
       this.admins.necessaryRequireTitle="",
       this.admins.necessaryRequire1="",
    
       this.admins.necessaryRequire2="",
       this.admins.necessaryRequireDescription1="",
       this.admins.necessaryRequireDescription2="",
       this.admins.mainDescriptions="",

       this.admins.impNote1="",
       this.admins.impNote2=""
}
admissionsGet(){ 
    this.pi.admissionsGet(this.branchNameId).subscribe((res)=>{
    this.admissionsGetGets=res
    var admind=this.admissionsGetGets[0]
    this.admissionsGetGetClick(admind)
    })
    }
    admissionsGetGetClick(get?){
      this.admissionsName=get.admissionProcessType
      this.admissionsNameId=get._id
         }
    
         admissionsGetMain(){ 
      this.pi.admissionsGet(this.branchNameIdMain).subscribe((res)=>{
      this.admissionsGetsMain=res
      var syllOnes=this.admissionsGetsMain[0]
      this.admissionsMainClick(syllOnes)
      })
      }
      
      admissionsMainClick(get?){
        this.admissionsGetsMainName=get.admissionProcessType
        this.admissionsGetsMainNameId=get._id
           }

  // *****************************************************************************************



  // *************************************************** freeStructure **************************************
               
  freeStructurePosts(){   
    this.pi.freeStructurePosts(this.branchNameId,this.fress).subscribe((res)=>{
       })
       this.fress.mainTitle="",
       this.fress.batchName="",
       this.fress.subtitle1="",
       this.fress.subtitle2="",
       this.fress.subtitle3="",
       this.fress.subtitle4="",
       this.fress.subtitle5="",
       this.fress.subtitle6="",
       this.fress.subtitle7="",
       this.fress.subtitle8="",
       this.fress.subtitleText1="",
       this.fress.subtitleText2="",
       this.fress.subtitleText3="",
       this.fress.subtitleText4="",
       this.fress.subtitleText5="",
       this.fress.subtitleText6="",
       this.fress.subtitleText7="",
       this.fress.subtitleText8="",
       this.fress.note=""

}
freeStructureGet(){ 
    this.pi.freeStructureGet(this.branchNameId).subscribe((res)=>{
    this.freeStructureGetGets=res
    var admind=this.freeStructureGetGets[0]
    this.freeStructureGetClick(admind)
    })
    }
    freeStructureGetClick(get?){
      this.admissionssName=get.batchName
      this.admissionssNameId=get._id
         }
    
         freeStructureGetMain(){ 
      this.pi.freeStructureGet(this.branchNameIdMain).subscribe((res)=>{
      this.freeStructureGetsMain=res
      var freeStr=this.freeStructureGetsMain[0]
      this.freeStructureMainClick(freeStr)
      })
      }
      
      freeStructureMainClick(get?){
        this.freeStructureGetsMainName=get.batchName
        this.freeStructureGetsMainNameId=get._id
           }

  // *****************************************************************************************



  // *************************************************** batches **************************************
               
  batchesPosts(){   
    this.pi.batchesPost(this.branchNameId,this.batches).subscribe((res)=>{
       })
       this.batches.batchName="",
       this.batches.branchShift="",
       this.batches.branchTimmingsStart="",
       this.batches.branchTimmingsEnding="",
       this.batches.batchDurations="",
       this.batches.totalBatchDurations="",
       this.batches.batchDescription1="",
       this.batches.batchDescription2="",
       this.batches.batchDescription3="",
       this.batches.batchDescription4=""
      

}
batchesGet(){ 
    this.pi.batchesGet(this.branchNameId).subscribe((res)=>{
    this.batchesGets=res
    var batches=this.batchesGets[0]
    this.batchesGetClick(batches)
    })
    }
    batchesGetClick(get?){
      this.batchesName=get.batchName
      this.batchesNameId=get._id
      this.facultyGet()
      this.testAnaysisGet()
      this.computerLabGet()
      this.libraryOneGet()
         }
    
         batchesGetMain(){ 
      this.pi.batchesGet(this.branchNameIdMain).subscribe((res)=>{
      this.batchesGetsMain=res
      var freeStr=this.batchesGetsMain[0]
      this.batchesMainClick(freeStr)
      })
      }
      
      batchesMainClick(get?){
        this.batchesGetsMainName=get.batchName
        this.batchesGetsMainNameId=get._id
        this.getfacultyMain()
        this.gettestAnaysisMain()
        this.computerLabMain()
        this.libraryOneGetMain()
        this.trackRecordsGetMain()
           }

  // *****************************************************************************************


  // *************************************************** faculty  **************************************
handleFileInputImagefaculty($event:any,img) {
  let imagesFaculty= File = $event.target.files[0];
  this.formData.append(img,imagesFaculty);   
}
facultyPost(){
  this.formData.append('facultyName',this.fatculty.facultyName);
 this.formData.append('facultyQualifications',this.fatculty.facultyQualifications);
 this.formData.append('facultyDescriptions',this.fatculty.facultyDescriptions);
 
this.pi.facultyPosts(this.batchesNameId,this.formData).subscribe((res)=>{
  console.log(this.branchNameId);
  
 })
 this.fatculty.facultyName="",
 this.fatculty.facultyQualifications="",
 this.fatculty.facultyDescriptions=""
}
facultyGet(){ 
  this.pi.facultyGets(this.batchesNameId).subscribe((res)=>{
  this.facultyGetss=res
  console.log(res);
  var faculty = this.facultyGetss[0]
  this.getfacultyClick(faculty)
    })
  }
  getfacultyClick(get?){
    this.facultyName=get.demoCode
    this.facultyNameId=get._id
                }

    getfacultyMain(){ 
                this.pi.facultyGets(this.batchesGetsMainNameId).subscribe((res)=>{
                this.getfacultyMains=res
                console.log(res);
                  })
                }
              

// ************************************************************************************************************************************

 // *************************************************** testAnaysis  **************************************
 handleFileInputImagetestAnaysis($event:any,img) {
  let imagestestAnaysis= File = $event.target.files[0];
  this.formData.append(img,imagestestAnaysis);   
}
testAnaysisPost(){
  this.formData.append('name',this.test.name);
 this.formData.append('place',this.test.place);
 this.formData.append('hallTicketNo',this.test.hallTicketNo);
 this.formData.append('markSecured',this.test.markSecured);
 this.formData.append('TotalMarks',this.test.TotalMarks);
 this.formData.append('studentQualification',this.test.studentQualification);
 this.formData.append('description',this.test.description);
this.pi.testAnaysisPosts(this.batchesNameId,this.formData).subscribe((res)=>{
  console.log(this.branchNameId);
 })
 this.test.name="",
 this.test.place="",
 this.test.hallTicketNo="",
 this.test.markSecured="",
 this.test.TotalMarks="",
 this.test.studentQualification="",
 this.test.description=""
 
}
testAnaysisGet(){ 
  this.pi.testAnaysisGets(this.batchesNameId).subscribe((res)=>{
  this.testAnaysisGetss=res
  console.log(res);
  var tests = this.testAnaysisGetss[0]
  this.gettestAnaysisClick(tests)
    })
  }
  gettestAnaysisClick(get?){
    this.testAnaysisName=get.demoCode
    this.testAnaysisNameId=get._id
                }

    gettestAnaysisMain(){ 
                this.pi.testAnaysisGets(this.batchesGetsMainNameId).subscribe((res)=>{
                this.gettestAnaysisMains=res
                console.log(res);
                  })
                }
              

// ************************************************************************************************************************************


 // *************************************************** computerLab  **************************************
 handleFileInputImagecomputerLab($event:any,img) {
  let imagescomputerLab= File = $event.target.files[0];
  this.formData.append(img,imagescomputerLab);   
}
computerLabPost(){
  this.formData.append('computerLabOne',this.computers.computerLabOne);
 this.formData.append('descriptions',this.computers.descriptions);
 
this.pi.computerLabPosts(this.batchesNameId,this.formData).subscribe((res)=>{
  console.log(this.branchNameId);
 })
 this.computers.computerLabOne="",
 this.computers.descriptions="",
 this.computers.computerLabImg=""
}
computerLabGet(){ 
  this.pi.computerLabGets(this.batchesNameId).subscribe((res)=>{
  this.computerLabGetss=res
  console.log(res);
  var tests = this.computerLabGetss[0]
  this.computerLabClick(tests)
    })
  }
  computerLabClick(get?){
    this.computerLabName=get.computerLabOne
    this.computerLabNameId=get._id
                }

                computerLabMain(){ 
                this.pi.computerLabGets(this.batchesGetsMainNameId).subscribe((res)=>{
                this.computerLabMains=res
                console.log(res);
                  })
                }
              

// ************************************************************************************************************************************


 // *************************************************** libraryOne **************************************
               
 libraryOnePosts(){   
  this.pi.libraryOnePosts(this.batchesNameId,this.libraryOne).subscribe((res)=>{
     })
     this.libraryOne.libraryQualification=""
  
}
libraryOneGet(){ 
  this.pi.libraryOneGet(this.batchesNameId).subscribe((res)=>{
  this.libraryOneGets=res
  var library=this.libraryOneGets[0]
  this.libraryOneGetClick(library)
  })
  }
  libraryOneGetClick(get?){
    this.libraryOneName=get.libraryQualification
    this.libraryOneNameId=get._id
    this.libraryTwoGet()
       }
  
       libraryOneGetMain(){ 
    this.pi.libraryOneGet(this.batchesGetsMainNameId).subscribe((res)=>{
    this.libraryOneGetsMain=res
    var library=this.libraryOneGetsMain[0]
    this.libraryOneMainClick(library)
    })
    }
    
    libraryOneMainClick(get?){
      this.libraryOneGetsMainName=get.libraryQualification
      this.libraryOneGetsMainNameId=get._id
    this.libraryTwoGetMain()
         }

// *****************************************************************************************



 // *************************************************** libraryTwo **************************************
               
 libraryTwoPosts(){   
  this.pi.libraryTwoPosts(this.libraryOneNameId,this.department).subscribe((res)=>{
     })
     this.department.libraryDepartment=""
  
}
libraryTwoGet(){ 
  this.pi.libraryTwoGet(this.libraryOneNameId).subscribe((res)=>{
  this.libraryTwoGets=res
  var libraryT=this.libraryTwoGets[0]
  this.libraryTwoGetClick(libraryT)
  })
  }
  libraryTwoGetClick(get?){
    this.libraryTwoName=get.libraryDepartment
    this.libraryTwoNameId=get._id
    this.computerLabThreeGet()
       }
  
       libraryTwoGetMain(){ 
    this.pi.libraryTwoGet(this.libraryOneGetsMainNameId).subscribe((res)=>{
    this.libraryTwoGetsMain=res
    var libraryT=this.libraryTwoGetsMain[0]
    this.libraryTwoMainClick(libraryT)
    })
    }
    
    libraryTwoMainClick(get?){
      this.libraryTwoGetsMainName=get.libraryDepartment
      this.libraryTwoGetsMainNameId=get._id
    this.computerLabThreeMain()
         }

// *****************************************************************************************


 // *************************************************** computerLabThree  **************************************
 handleFileInputImagecomputerLabThree($event:any,img) {
  let imagescomputerLabThree= File = $event.target.files[0];
  this.formData.append(img,imagescomputerLabThree);   
}
computerLabThreePost(){
  this.formData.append('booksMainTitle',this.libT.booksMainTitle);
 this.formData.append('booksAuthor',this.libT.booksAuthor);
 this.formData.append('booksName',this.libT.booksName);

this.pi.computerLabThreePosts(this.libraryTwoNameId,this.formData).subscribe((res)=>{
  })
 this.libT.booksMainTitle="",
 this.libT.booksAuthor="",
 this.libT.booksName="",
 this.libT.booksImg=""
}
computerLabThreeGet(){ 
  this.pi.computerLabThreeGets(this.libraryTwoNameId).subscribe((res)=>{
  this.computerLabThreeGetss=res
  console.log(res);
  var tests = this.computerLabThreeGetss[0]
  this.computerLabThreeClick(tests)
    })
  }
  computerLabThreeClick(get?){
    this.computerLabThreeName=get.booksMainTitle
    this.computerLabThreeNameId=get._id
                }

                computerLabThreeMain(){ 
                this.pi.computerLabThreeGets(this.libraryTwoGetsMainNameId).subscribe((res)=>{
                this.computerLabThreeMains=res
                console.log(res);
                  })
                }
              

// ************************************************************************************************************************************


// *************************************************** trackRecords  **************************************
handleFileInputImagetrackRecords($event:any,img) {
  let imagetrackRecords = File = $event.target.files[0];
  this.formData.append(img,imagetrackRecords);   
}
trackRecordsPost(){
  this.formData.append('studentName',this.tracks.studentName);
 this.formData.append('place',this.tracks.place);
 this.formData.append('marks',this.tracks.marks);
 this.formData.append('hallTicket',this.tracks.hallTicket);
 this.formData.append('elegibliityPost',this.tracks.elegibliityPost);
 this.formData.append('coursedJoined',this.tracks.coursedJoined);
 this.formData.append('studentQualification',this.tracks.studentQualification);
 this.formData.append('yearOfSelection',this.tracks.yearOfSelection);
 this.formData.append('AIR',this.tracks.AIR);
this.pi.trackRecordsPost(this.batchesNameId,this.formData).subscribe((res)=>{
  })
 this.tracks.studentName="",
 this.tracks.place="",
 this.tracks.marks="",
 this.tracks.hallTicket="",
  this.tracks.elegibliityPost="",
 this.tracks.coursedJoined="",
 this.tracks.studentImg="",
  this.tracks.AIR="",
 this.tracks.studentQualification="",
 this.tracks.yearOfSelection=""
}
trackRecordsGet(){ 
  this.pi.trackRecordsGets(this.batchesNameId).subscribe((res)=>{
  this.trackRecordsGetss=res
  console.log(res);
  var tracks = this.trackRecordsGetss[0]
  this.trackRecordsClick(tracks)
    })
  }
  trackRecordsClick(get?){
    this.trackRecordsName=get.booksMainTitle
    this.trackRecordsNameId=get._id
                }

                trackRecordsGetMain(){ 
                this.pi.trackRecordsGets(this.batchesGetsMainNameId).subscribe((res)=>{
                this.trackRecordsMains=res
                console.log(res);
                  })
                }
              
// ***************************************************updades **************************************
handleFileInputUpdatesImages($event:any,images){
  this.imageNameupdates= File = $event.target.files[0];
 
}

handleFileInputUpdatessViedoes($event:any,viedoes){
  this.videoName= File = $event.target.files[0];
  this.formData.append(viedoes,this.videoName);
 
 
}

updatesPostData(){
this.formData.append('images',this.imageNameupdates);
this.formData.append('title',this.ups.title);
this.formData.append('description',this.ups.description);
 

  console.log(this.formData)

 this.pi.postsUpdata(this.usersID,this.formData).subscribe((res)=>{
    console.log('assssss',res);
   console.log(this.usersID);
   
     })
     this.ups.title="",
     this.ups.description="",
     this.ups.images="",
     this.ups.viedoes=""
}
 // ******************************************updates present ***************************************
 shareClick(up){
  this.share=!this.share
  this.comments=false
  this.viewcomments=false
  this.replyfrom=false
  this.openshareid=up._id
}
commentsClick(up){
  this.comments=!this.comments
  this.share=false
  this.viewcomments=false
  this.commentspostsId=up._id
   this.commentsGettoClints()
   this.commentsGettoClintsCounts()
 }

viewCommentsClick(up){
  this.viewcomments=!this.viewcomments
  this.share=false
  this.viewcommentsid=up._id
}
replyFromm(comms){
  this.replyfrom=!this.replyfrom
  this.share=false
  this.replyid=comms._id
  this.replyFromCommentesGet()
  console.log(this.replyid);
  
}
 clientsUpdates(){   
  this.pi.clientsUpdatesGet(this.usersID).subscribe((res)=>{
    this.clientUpdates=res
   })
 }
 
 clientsUpdatesCount(){   
  this.pi.clientsUpdatesGetCounts(this.usersID).subscribe((res)=>{
    this.clientUpdatesCount=res
     })
 }

 commentsPoststoClints(){   
  this.pi.commentsPoststoClints(this.replyid,this.postComments).subscribe((res)=>{
     })
     this.postComments.descriptions=""
 }
 commentsGettoClints(){   
  this.pi.commentsGettoClints(this.commentspostsId).subscribe((res)=>{
    this.getUsersComments=res
   })
 }

 commentsGettoClintsCounts(){   
  this.pi.commentsGettoClintsCounts(this.commentspostsId).subscribe((res)=>{
    this.getUsersCommentsCounts=res
    })
 }

 replyFromCommentesGet(){   
  this.pi.replyFromCommentesGet(this.replyid).subscribe((res)=>{
    this.replies=res
    console.log(res);
    
    })
 }
 onlyComments(){   
  this.pi.overallscommentspost(this.usersID).subscribe((res)=>{
    this.onlyComm=res
    console.log(res);
    
    })
 }
 onlyCommentsC(){   
  this.pi.onlyCommentsGetCount(this.usersID).subscribe((res)=>{
    this.onlyCommC=res
    console.log(res);
    
    })
 }


}
  
  
  
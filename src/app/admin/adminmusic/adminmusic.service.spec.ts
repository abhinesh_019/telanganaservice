import { TestBed, inject } from '@angular/core/testing';

import { AdminmusicService } from './adminmusic.service';

describe('AdminmusicService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AdminmusicService]
    });
  });

  it('should be created', inject([AdminmusicService], (service: AdminmusicService) => {
    expect(service).toBeTruthy();
  }));
});

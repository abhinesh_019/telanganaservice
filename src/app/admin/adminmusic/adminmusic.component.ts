import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AdminmusicService } from './adminmusic.service';

@Component({
  selector: 'app-adminmusic',
  templateUrl: './adminmusic.component.html',
  styleUrls: ['./adminmusic.component.css']
})
export class AdminmusicComponent implements OnInit {
  public showmains:boolean=false
public updatesShows:boolean=false
public serviceShows:boolean=false
public onlyCommentsShows:boolean=false
public ImagesShows:boolean=false
public ProductsShows:boolean=false
 public serviceType:any
public usersID:any
public musicTypeId:any
public musicTypeName:any
public imageNameupdates:any
public videoName:any
public servicegets:any
public servicegetsc:any
public imageName:any
public catget:any
public  musClients:any
public fecilities:any
public fecilitiesc:any
public share:boolean=false
public comments:boolean=false
public viewcomments:boolean=false
public replyfrom:boolean=false 
public openshareid:any
public clientUpdates:any
public clientUpdatesCount:any
public commentspostsId:any
public getUsersComments:any
public getUsersCommentsCounts:any
 public viewcommentsid:any
 public replies:any
 public replyid:any
 public musicDetailsClients:any
 public gcomm:any
 public serviceOneGets:any
 public servicesOneName:any
 public servicesOneID:any
 public serviceTwoGets:any
 public gcommc:any
 public imageServicesTwoMasterImg:any
 public imagesServiesTwoNormal:any
 public videoReff:any
 public catageriesGetss:any
 public catageriesDanceName:any
 public catageriesDanceNameId:any
 public imageNameCat:any
 public viewsProfiles:any
 public viewsNoti:any
//  public gcommc:any
//  public gcommc:any
//  public gcommc:any
//  public gcommc:any
//  public gcommc:any
//  public gcommc:any
//  public gcommc:any
//  public gcommc:any
//  public gcommc:any
//  public gcommc:any
//  public gcommc:any
//  public gcommc:any
//  public gcommc:any
//  public gcommc:any
//  public gcommc:any
//  public gcommc:any
//  public gcommc:any
//  public gcommc:any
//  public gcommc:any


sec={
  musicsType:"",
 }
ser={
     masterName:"",
    yearOfExp:"",
    descriptions:"",
    masterImg:"",
    images:"",
    viedoes:"",
}
fec={
  name:"",
     importantsKey:"",
    descriptions:"",
    images:"",
 }

 ups={
  images:"",
    title:"",
    descriptions:"",
    viedoes:"",
}
postComments={
  "descriptions":""
}
musicCat={
  musicCatatgeries:"",
  musicImg:"",
}

formData: FormData = new FormData(); 


constructor(private route:ActivatedRoute,private mus:AdminmusicService) { }


  ngOnInit() {
    this.usersID=localStorage.getItem('ads');
    this.serviceTypesGet()
     this.usersDetails()
     this.fecilitiesGet()
     this.fecilitiesGetC()
     this.clientsUpdates()
     this.clientsUpdatesCount()
     this.generalCommGet()
     this.generalCommGetc()
     this.CatageriesGets()
   }
 
   viewProfile(){
    this.viewsProfiles=!this.viewsProfiles
    this.viewsNoti=false
  }
  viewnoti(){
    this.viewsNoti=!this.viewsNoti
    this.viewsProfiles=false

  }
usersDetails(){ 
this.mus.usersDetails(this.usersID).subscribe((res)=>{
this.musicDetailsClients=res
console.log(res);

})
}
  

                              // *************************************************** services Two  **************************************
    handleFileInputTwo($event:any,masterImg){
    this.imageServicesTwoMasterImg= File = $event.target.files[0];
   }
   handleFileInputUpdatesITwo($event:any,viedoes){
    this.imagesServiesTwoNormal= File = $event.target.files[0];
    this.formData.append(viedoes,this.imagesServiesTwoNormal);
    }
    handleFileInputUpdatesVTwo($event:any,viedoes){
      this.videoReff= File = $event.target.files[0];
      this.formData.append(viedoes,this.videoReff);
      }
  servicesTwoPosts(){
  this.formData.append('masterImg',this.imageServicesTwoMasterImg);
  this.formData.append('masterName',this.ser.masterName);
  this.formData.append('yearOfExp',this.ser.yearOfExp);
  this.formData.append('descriptions',this.ser.descriptions);
  
   this.mus.servicesTwoPosts(this.catageriesDanceNameId,this.formData).subscribe((res)=>{
      console.log('assssss',res);
     console.log(this.usersID);
     
       })
       this.ser.masterName="",
       this.ser.descriptions="",
        this.ser.images="",
       this.ser.viedoes="",
       this.ser.masterImg="",
       this.ser.yearOfExp=""
   }
   serviceTwoGet(){ 
    this.mus.serviceTwoGet(this.catageriesDanceNameId).subscribe((res)=>{
    this.serviceTwoGets=res
    var sart=this.serviceTwoGets[0]
        })
         }
    // <!-- ***************************************************************************************************************** -->
  

// <!-- **************************************************************** create Fecilities*************************************************** -->
handleFileInputFec($event:any,images){
  this.imageName= File = $event.target.files[0];
console.log(this.imageName);

}

updatesPostDataFec(){ 
  this.formData.append('images',this.imageName);
this.formData.append('name',this.fec.name);
this.formData.append('importantsKey',this.fec.importantsKey);
this.formData.append('descriptions',this.fec.descriptions);
 this.mus.updatesPostDataFec(this.usersID,this.formData).subscribe((res)=>{
   console.log(this.usersID);
   
 })
 this.fec.name="",
 this.fec.importantsKey="",
 this.fec.descriptions="",
   this.fec.images=""
                   }
                   fecilitiesGet(){   
                    this.mus.fecilitiesGet(this.usersID).subscribe((res)=>{
                      this.fecilities=res
                       })
                   }
                   fecilitiesGetC(){   
                    this.mus.fecilitiesGetC(this.usersID).subscribe((res)=>{
                      this.fecilitiesc=res
                       })
                   }
                                     
    // <!-- ***************************************************************************************************************** -->
   
   serviceTypesGet(){   
    this.mus.serviceTypeDataGet(this.usersID).subscribe((res)=>{
      this.serviceType=res
       var id =this.serviceType[0];
      this.cageries(id)
   
      
      })
   }
 
  cageries(get?){
this.musicTypeName=get.catageriesType
this.musicTypeId=get._id
 console.log(this.musicTypeName);
 console.log(this.musicTypeId);
this.serviceGet()
this.serviceGetC()
}
serviceGet(){   
  this.mus.serviceDataGet(this.musicTypeId).subscribe((res)=>{
     this.servicegets=res
     console.log(res);
     
    })
 }

 serviceGetC(){   
  this.mus.serviceDataGetC(this.musicTypeId).subscribe((res)=>{
    this.servicegetsc=res
    console.log(res);
    })
 }


 
handleFileInputUpdates($event:any,images){
  this.imageNameupdates= File = $event.target.files[0];
  
}

handleFileInputUpdatess($event:any,viedoes){
  this.videoName= File = $event.target.files[0];
   console.log(this.videoName);
 }

updatesPostData(){
  this.formData.append('viedoes',this.videoName);
 this.formData.append('images',this.imageNameupdates);
this.formData.append('masterName',this.ser.masterName);
this.formData.append('yearOfExp',this.ser.yearOfExp);
this.formData.append('descriptions',this.ser.descriptions);

  console.log(this.formData)

 this.mus.postsServices(this.musicTypeId,this.formData).subscribe((res)=>{
  
     })
          this.ser.masterName="",
          this.ser.yearOfExp="",
          this.ser.descriptions=""
          this.ser.images=""
          this.ser.viedoes=""
}
  
  mainMenuShow(){
this.showmains=!this.showmains
this.updatesShows=false
this.serviceShows=false
this.onlyCommentsShows=false
this.ImagesShows=false
this.ProductsShows=false
   }
  updatesShow(){
    this.updatesShows=!this.updatesShows
    this.showmains=false
    this.serviceShows=false
    this.onlyCommentsShows=false
    this.ImagesShows=false
    this.ProductsShows=false
   }
  ServiceShow(){
    this.serviceShows=!this.serviceShows
    this.updatesShows=false
    this.showmains=false
    this.onlyCommentsShows=false
    this.ImagesShows=false
    this.ProductsShows=false
   }
  onlyCommentsShow(){
    this.onlyCommentsShows=!this.onlyCommentsShows
    this.updatesShows=false
    this.showmains=false
    this.serviceShows=false
    this.ImagesShows=false
    this.ProductsShows=false
   }
  ImagesShow(){
    
    this.ImagesShows=!this.ImagesShows
    this.updatesShows=false
    this.showmains=false
    this.onlyCommentsShows=false
    this.serviceShows=false
    this.ProductsShows=false
   }
  ProductsShow(){
    this.ProductsShows=!this.ProductsShows
    this.updatesShows=false
    this.showmains=false
    this.onlyCommentsShows=false
    this.serviceShows=false
    this.ImagesShows=false
   }
  // ***************************************************updades **************************************
  handleFileInputUpdatesI($event:any,images){
  this.imageNameupdates= File = $event.target.files[0];
 
}

handleFileInputUpdatesV($event:any,viedoes){
  this.videoName= File = $event.target.files[0];
  this.formData.append(viedoes,this.videoName);
 
 
}

updatesPostDatas(){
this.formData.append('images',this.imageNameupdates);
this.formData.append('title',this.ups.title);
this.formData.append('descriptions',this.ups.descriptions);
 

  console.log(this.formData)

 this.mus.updatesPostDatas(this.usersID,this.formData).subscribe((res)=>{
    console.log('assssss',res);
   console.log(this.usersID);
   
     })
     this.ups.title="",
     this.ups.descriptions="",
     this.ups.images="",
     this.ups.viedoes=""
}

// ******************************************updates present ***************************************
shareClick(up){
  this.share=!this.share
  this.comments=false
  this.viewcomments=false
  this.replyfrom=false
  this.openshareid=up._id
}
commentsClick(up){
  this.comments=!this.comments
  this.share=false
  this.viewcomments=false
  this.commentspostsId=up._id
   this.commentsGettoClints()
   this.commentsGettoClintsCounts()
 }

viewCommentsClick(up){
  this.viewcomments=!this.viewcomments
  this.share=false
  this.viewcommentsid=up._id
}
replyFromm(comms){
  this.replyfrom=!this.replyfrom
  this.share=false
  this.replyid=comms._id
  this.replyFromCommentesGet()

}
 clientsUpdates(){   
  this.mus.clientsUpdatesGet(this.usersID).subscribe((res)=>{
    this.clientUpdates=res
   
   })
 }
 
 clientsUpdatesCount(){   
  this.mus.clientsUpdatesGetCounts(this.usersID).subscribe((res)=>{
    this.clientUpdatesCount=res
     })
 }

 commentsPoststoClints(){   
  this.mus.commentsPoststoClints(this.replyid,this.postComments).subscribe((res)=>{
    
  })
     this.postComments.descriptions=""
 }
 commentsGettoClints(){   
  this.mus.commentsGettoClints(this.commentspostsId).subscribe((res)=>{
    this.getUsersComments=res
   })
 }

 commentsGettoClintsCounts(){   
  this.mus.commentsGettoClintsCounts(this.commentspostsId).subscribe((res)=>{
    this.getUsersCommentsCounts=res
    })
 }

 replyFromCommentesGet(){   
  this.mus.replyFromCommentesGet(this.replyid).subscribe((res)=>{
    this.replies=res
    console.log(this.replyid);
    
    })
 }
 generalCommGet(){ 
  this.mus.generalsCommGet(this.usersID).subscribe((res)=>{
  this.gcomm=res
     })
  }

  generalCommGetc(){ 
    this.mus.generalsCommGetC(this.usersID).subscribe((res)=>{
    this.gcommc=res
      })
    }
    // <!-- **************************************************************** music catagories************************************************** -->
  handleFileInputCat($event:any,images){
    this.imageNameCat= File = $event.target.files[0];
   
  }
  
  updatesPostDataCat(){ 
    this.formData.append('musicImg',this.imageNameCat);
  this.formData.append('musicCatatgeries',this.musicCat.musicCatatgeries);
 
   this.mus.updatesPostDataCats(this.usersID,this.formData).subscribe((res)=>{
     
   })

   this.musicCat.musicCatatgeries="",
     this.musicCat.musicImg=""
                     }

                     CatageriesGets(){   
                      this.mus.CatageriesGets(this.usersID).subscribe((res)=>{
                        this.catageriesGetss=res
                         })
                     }
                     catageriesGetClick(get){
                       this.catageriesDanceName=get.musicCatatgeries
                       this.catageriesDanceNameId=get._id
                       this.serviceTwoGet()
                     }
   
                                       
      // <!-- ***************************************************************************************************************** -->
     
}

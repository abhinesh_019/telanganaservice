import {MatInputModule} from '@angular/material/input';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material';
import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { AdminPackersMoversComponent } from './admin-packers-movers.component';
import { AdminPackersMoversService } from './admin-packers-movers.service';
 
const routes:Routes=[{path:'admidPackersMovers',component:AdminPackersMoversComponent},]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    SharedModule
  ],
  declarations: [
    AdminPackersMoversComponent,

  ],
  providers: [AdminPackersMoversService],
})
export class AdminPackersMoversModule { }

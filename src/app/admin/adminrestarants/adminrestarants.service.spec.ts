import { TestBed, inject } from '@angular/core/testing';

import { AdminrestarantsService } from './adminrestarants.service';

describe('AdminrestarantsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AdminrestarantsService]
    });
  });

  it('should be created', inject([AdminrestarantsService], (service: AdminrestarantsService) => {
    expect(service).toBeTruthy();
  }));
});

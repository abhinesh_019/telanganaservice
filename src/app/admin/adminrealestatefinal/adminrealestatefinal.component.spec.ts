import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminrealestatefinalComponent } from './adminrealestatefinal.component';

describe('AdminrealestatefinalComponent', () => {
  let component: AdminrealestatefinalComponent;
  let fixture: ComponentFixture<AdminrealestatefinalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminrealestatefinalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminrealestatefinalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

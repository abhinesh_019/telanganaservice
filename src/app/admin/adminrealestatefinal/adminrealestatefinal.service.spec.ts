import { TestBed, inject } from '@angular/core/testing';

import { AdminrealestatefinalService } from './adminrealestatefinal.service';

describe('AdminrealestatefinalService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AdminrealestatefinalService]
    });
  });

  it('should be created', inject([AdminrealestatefinalService], (service: AdminrealestatefinalService) => {
    expect(service).toBeTruthy();
  }));
});

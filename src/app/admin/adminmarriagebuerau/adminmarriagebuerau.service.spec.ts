import { TestBed, inject } from '@angular/core/testing';

import { AdminmarriagebuerauService } from './adminmarriagebuerau.service';

describe('AdminmarriagebuerauService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AdminmarriagebuerauService]
    });
  });

  it('should be created', inject([AdminmarriagebuerauService], (service: AdminmarriagebuerauService) => {
    expect(service).toBeTruthy();
  }));
});

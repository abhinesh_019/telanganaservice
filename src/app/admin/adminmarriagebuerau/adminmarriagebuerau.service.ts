import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
 import 'rxjs/add/operator/map';
import { environment } from '../../../environments/environment.prod';
 
@Injectable()
export class AdminmarriagebuerauService {

  constructor(private http: HttpClient) { }
  
  usersDetails(id){
    return this.http.get(environment.apiUrl + "/marriagesClientsGetind/"+id);
     
  }
    
  casteCat(id,data){
    return this.http.post(environment.apiUrl + "/marriagesServicesTypePost/"+id,data);
     
  }
 
  casteCatGet(id){
    return this.http.get(environment.apiUrl + "/marriagesServicesTypeGet/"+id);
     
  }
  mainCasteCat(id,data){
    return this.http.post(environment.apiUrl + "/marriagesServicesTypemainPost/"+id,data);
     
  }
  smainCasteCat(id,data){
    return this.http.post(environment.apiUrl + "/marriagesServicesPost/"+id,data);
     
  }
  maincasteCatGet(id){
    return this.http.get(environment.apiUrl + "/marriagesServicesTypemainGet/"+id);
     
  }
  smaincasteCatGet(id){
    return this.http.get(environment.apiUrl + "/marriagesServicesGet/"+id);
     
  }
  trackRecords(id,data){
    return this.http.post(environment.apiUrl + "/marriagesTrackRecordsPost/"+id,data);
     
  }
  tracksRecords(id){
    return this.http.get(environment.apiUrl + "/marriagesTrackRecordsGet/"+id);
     
  }
  tracksRecordsc(id){
    return this.http.get(environment.apiUrl + "/marriagesTrackRecordsGetCounts/"+id);
     
  }
  postsUpdata(id,data){
                    
    return this.http.post(environment.apiUrl +"/marriagesupdatesgetpostsdata/"+id,data);
  }
  clientsUpdatesGet(id){
    
    return this.http.get(environment.apiUrl +"/marriagesupdatesget/"+id);
  }
  
  clientsUpdatesGetCounts(id){
    
    return this.http.get(environment.apiUrl +"/marriagesupdatesgetCounts/"+id);
  }
  commentsPoststoClints(id,data){
    
    return this.http.post(environment.apiUrl +"/marriagesupdatesCommentReplysPost/"+id,data);
  }
  commentsGettoClints(id){
    
    return this.http.get(environment.apiUrl +"/marriagesupdatesCommentsget/"+id);
  }
  commentsGettoClintsCounts(id){
    
    return this.http.get(environment.apiUrl +"/marriagesupdatesCommentsgetcounts/"+id);
  }
  replyFromCommentesGet(id){
    
    return this.http.get(environment.apiUrl +"/marriagesupdatesCommentReplysGet/"+id);
  }

  overallscommentspost(id){
  
    return this.http.get(environment.apiUrl +"/marriageseuserscomments/"+id);
  }
  // ********************************
   
  
  onlyCommentsGetCount(id){
    
    return this.http.get(environment.apiUrl +"/marriagesCeuserscommentsCounts/"+id);
  }
  }

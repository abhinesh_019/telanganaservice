import {MatInputModule} from '@angular/material/input';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material';
import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { AdmintravelsComponent } from './admintravels.component';
import { AdmintravelsService } from './admintravels.service';
 
 

const routes:Routes=[{path:'admintravels',component:AdmintravelsComponent},]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    SharedModule
  ],
  declarations: [
    AdmintravelsComponent,

  ],
  providers: [AdmintravelsService],
})
export class AdmintravelsModule { }

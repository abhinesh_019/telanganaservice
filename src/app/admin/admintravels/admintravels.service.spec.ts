import { TestBed, inject } from '@angular/core/testing';

import { AdmintravelsService } from './admintravels.service';

describe('AdmintravelsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AdmintravelsService]
    });
  });

  it('should be created', inject([AdmintravelsService], (service: AdmintravelsService) => {
    expect(service).toBeTruthy();
  }));
});

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
 import 'rxjs/add/operator/map';
import { environment } from '../../../environments/environment.prod';

@Injectable()
export class AdminfireWorksService {

 
  
  constructor(private http: HttpClient) { }
 
  usersDetails(id){
    
    return this.http.get(environment.apiUrl +"/fireworksClientsGetind/"+id);
  }
  postUpdatesForm(id,data){
    return this.http.post(environment.apiUrl + "/fireworksupdatespostsdata/"+id,data);
     } 
     //  ************************updates present ***************************
  clientsUpdatesGet(id){
    
    return this.http.get(environment.apiUrl +"/fireworksupdatesget/"+id);
  }
  
  clientsUpdatesGetCounts(id){
    
    return this.http.get(environment.apiUrl +"/fireworksupdatesgetCounts/"+id);
  }
  commentsPoststoClints(id,data){
    
    return this.http.post(environment.apiUrl +"/fireworksupdatesCommentReplysPost/"+id,data);
  }
  commentsGettoClints(id){
    
    return this.http.get(environment.apiUrl +"/fireworksupdatesCommentsget/"+id);
  }
  commentsGettoClintsCounts(id){
    
    return this.http.get(environment.apiUrl +"/fireworksupdatesCommentsgetcounts/"+id);
  }
  replyFromCommentesGet(id){
    
    return this.http.get(environment.apiUrl +"/fireworksupdatesCommentReplysGet/"+id);
  }
  generalsCommGet(id) {
    return this.http.get(environment.apiUrl + "/fireworksUsersComments/"+id);
  }
  generalsCommGetC(id) {
    return this.http.get(environment.apiUrl + "/fireworksUsersCommentsCounts/"+id);
  }
  fireworksServicesOne(id,data){
    return this.http.post(environment.apiUrl + "/fireworksServicesPost/"+id,data);
     }
     fireworksServicesOneGet(id) {
      return this.http.get(environment.apiUrl + "/fireworksServicesGet/"+id);
    }
    fireworksServicesOneGetC(id) {
      return this.http.get(environment.apiUrl + "/fireworksServicesGetCounts/"+id);
    }
    fireworksServicesTwo(id,data){
      return this.http.post(environment.apiUrl + "/fireworksServicesTwoPost/"+id,data);
       }
       fireworksServicesTwoGet(id) {
        return this.http.get(environment.apiUrl + "/fireworksServicesTwo/"+id);
      }
      fireworksServicesTwoGetC(id) {
        return this.http.get(environment.apiUrl + "/fireworksServicesTwoCounts/"+id);
      }


      fireworksServicesThree(id,data){
        return this.http.post(environment.apiUrl + "/fireworkServicesCustomersPost/"+id,data);
         }
         fireworksServicesThreeGet(id) {
          return this.http.get(environment.apiUrl + "/fireworkServicesCustomers/"+id);
        }
        fireworksServicesThreeGetC(id) {
          return this.http.get(environment.apiUrl + "/fireworkServicesCustomersCounts/"+id);
        }
}


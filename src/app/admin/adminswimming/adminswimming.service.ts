import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { environment } from '../../../environments/environment.prod';
 
@Injectable()
export class AdminswimmingService {

  constructor(private http: HttpClient) { }
  usersDetails(id){ return this.http.get(environment.apiUrl +"/swimmingClientsGetind/"+id)}
  createServicesPosts(id,data)
  { 
    return this.http.post(environment.apiUrl +"/swimmingCreatingGendersPost/"+id,data)
  }
  servicesGenders(id){
     return this.http.get(environment.apiUrl +"/swimmingCreatingGendersGet/"+id)
    }
    createAccessaoriesPosts(id,data)
    { 
      return this.http.post(environment.apiUrl +"/swimmingAccessaoriesPost/"+id,data)
    }
    servicesaccGet(id){
      return this.http.get(environment.apiUrl +"/swimmingAccessaoriesGet/"+id)
     }
     servicesaccGetc(id){
      return this.http.get(environment.apiUrl +"/swimmingAccessaoriesCounts/"+id)
     }
     postFecilities(id,data)
     { 
       return this.http.post(environment.apiUrl +"/swimmingFecilitiesPost/"+id,data)
     }
     fecilitiesGet(id){
      return this.http.get(environment.apiUrl +"/swimmingFecilitiesGet/"+id)
     }
     fecilitiesGetc(id){
      return this.http.get(environment.apiUrl +"/swimmingFecilitiesCounts/"+id)
     }
     postsUpdata(id,data){
                    
      return this.http.post(environment.apiUrl +"/swimmingsupdatesgetpostsdata/"+id,data);
    }
    clientsUpdatesGet(id){
    
      return this.http.get(environment.apiUrl +"/swimmingsupdatesget/"+id);
    }
    
    clientsUpdatesGetCounts(id){
      
      return this.http.get(environment.apiUrl +"/swimmingsupdatesgetCounts/"+id);
    }
    commentsPoststoClints(id,data){
      
      return this.http.post(environment.apiUrl +"/swimmingsupdatesCommentReplysPost/"+id,data);
    }
    commentsGettoClints(id){
      
      return this.http.get(environment.apiUrl +"/swimmingsupdatesCommentsget/"+id);
    }
    commentsGettoClintsCounts(id){
      
      return this.http.get(environment.apiUrl +"/swimmingsupdatesCommentsgetcounts/"+id);
    }
    replyFromCommentesGet(id){
      
      return this.http.get(environment.apiUrl +"/swimmingsupdatesCommentReplysGet/"+id);
    }
  
    overallscommentspost(id){
  
      return this.http.get(environment.apiUrl +"/swimmingseuserscomments/"+id);
    }
    
    onlyCommentsGetCount(id){
      
      return this.http.get(environment.apiUrl +"/swimmingsCeuserscommentsCounts/"+id);
    }
  }
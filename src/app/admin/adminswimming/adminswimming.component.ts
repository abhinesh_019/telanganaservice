import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AdminswimmingService } from './adminswimming.service';

@Component({
  selector: 'app-adminswimming',
  templateUrl: './adminswimming.component.html',
  styleUrls: ['./adminswimming.component.css']
})
export class AdminswimmingComponent implements OnInit {

  public showmains:boolean=false
  public updatesShows:boolean=false
  public viewupdatesShows:boolean=false
  public onlyCommentsShows:boolean=false
  public ImagesShows:boolean=false
  public ProductsShows:boolean=false
  public ProductsViewShows:boolean=false

  public usersID:any
public swimmingDetailsClients:any
public viewsProfiles:any
public viewsNoti:any
public imageName:any
public gendersCat:any
public gendersName:any
public gendersId:any
public gendersCatacc:any
public imageNames:any
public acceriesGet:any
public acceriesGetc:any
public fecilitiesGets:any
public fecilitiesGetsc:any
public imageNameupdates:any
public videoName:any
public openshareid:any
public commentspostsId:any
public viewcommentsid:any
public clientUpdates:any
public replyid:any
public clientUpdatesCount:any
 public getUsersComments:any
public getUsersCommentsCounts:any
public share:boolean=false
public comments:boolean=false
public viewcomments:boolean=false
public replyfrom:boolean=false 
public replies:any
public onlyCommC:any
public onlyComm:any
// public usersID:any
// public usersID:any
// public usersID:any
// public usersID:any
// public usersID:any
// public usersID:any


formData: FormData = new FormData(); 

cgs={
  genders:"",
  gendersimages:"",
  coachName:"",
  coachYearsOfExp:"",
  coachImg:"",
  coachNo:"",
  poolsImg1:"",
  poolsImg2:"",
  poolsImg3:"",
  poolsImg4:"",
  poolsImg5:"",
  descriptions:"",
}
acc={
  acceriesName:"",
  acceriesUse:"",
  acceriesDescriptions:"",
  acceriesImg:"",
}
fec={
  fecilitiesName:"",
  fecilitiesImg:"",
  fecilitiesDescriptions:"",
  
}
ups={
  images:"",
    title:"",
    description:"",
    viedoes:"",
}

postComments={
  "descriptions":""
}

constructor(private route:ActivatedRoute,private mus:AdminswimmingService) { }
 ngOnInit() {
    this.usersID=localStorage.getItem('ads');
this.usersDetails()
this.servicesGenders()
this.servicesGendersacc()
this.fecilitiesGet()
this.fecilitiesGetc()
this.clientsUpdatesCount()
this.clientsUpdates()
this.onlyComments()
this.onlyCommentsC()
  }
  usersDetails(){ 
    this.mus.usersDetails(this.usersID).subscribe((res)=>{
    this.swimmingDetailsClients=res
    console.log(res);
    
    })
    }
    viewProfile(){
      this.viewsProfiles=!this.viewsProfiles
      this.viewsNoti=false
    }
    viewnoti(){
      this.viewsNoti=!this.viewsNoti
      this.viewsProfiles=false
  
    }
    
    mainMenuShow(){
      this.showmains=!this.showmains
      this.updatesShows=false
      this.viewupdatesShows=false
      this.onlyCommentsShows=false
      this.ImagesShows=false
      this.ProductsShows=false
      this.ProductsViewShows=false
    }
    updatesShow(){
      this.updatesShows=!this.updatesShows
      this.showmains=false
      this.viewupdatesShows=false
      this.onlyCommentsShows=false
      this.ImagesShows=false
      this.ProductsShows=false
      this.ProductsViewShows=false
    }
    viewupdatesShow(){
      this.viewupdatesShows=!this.viewupdatesShows
      this.updatesShows=false
      this.showmains=false
      this.onlyCommentsShows=false
      this.ImagesShows=false
      this.ProductsShows=false
      this.ProductsViewShows=false
    }
    onlyCommentsShow(){
      this.onlyCommentsShows=!this.onlyCommentsShows
      this.updatesShows=false
      this.showmains=false
      this.viewupdatesShows=false
      this.ImagesShows=false
      this.ProductsShows=false
      this.ProductsViewShows=false
    }
    ImagesShow(){
      
      this.ImagesShows=!this.ImagesShows
      this.updatesShows=false
      this.showmains=false
      this.onlyCommentsShows=false
      this.viewupdatesShows=false
      this.ProductsShows=false
      this.ProductsViewShows=false
    }
    ProductsShow(){
      this.ProductsShows=!this.ProductsShows
      this.updatesShows=false
      this.showmains=false
      this.onlyCommentsShows=false
      this.viewupdatesShows=false
      this.ImagesShows=false
      this.ProductsViewShows=false
    }
    ProductsViewShow(){
      this.ProductsViewShows=!this.ProductsViewShows
      this.updatesShows=false
      this.showmains=false
      this.onlyCommentsShows=false
      this.viewupdatesShows=false
      this.ImagesShows=false
      this.ProductsShows=false
    }
    // ********** swimming pools*******
  
     
   handleFileInputUpdates($event:any,images) {
    let imageName= File = $event.target.files[0];
    this.formData.append(images, imageName);
    console.log(imageName);
    
  }
   createServicesPosts(){
      this.formData.append('descriptions', this.cgs.descriptions);
     this.formData.append('genders', this.cgs.genders);
     this.formData.append('coachName', this.cgs.coachName);
     this.formData.append('coachYearsOfExp', this.cgs.coachYearsOfExp);
     this.formData.append('coachNo', this.cgs.coachNo);

     console.log(this.formData)
  
    this.mus.createServicesPosts(this.usersID,this.formData).subscribe((res)=>{
       console.log('assssss',res);
       })
        this.cgs.genders="",
        this.cgs.gendersimages="",
        this.cgs.coachName="",
        this.cgs.coachYearsOfExp="",
        this.cgs.coachNo="",
        this.cgs.descriptions="",
        this.cgs.coachImg="",
        this.cgs.poolsImg1="",
        this.cgs.poolsImg2="",
        this.cgs.poolsImg3="",
        this.cgs.poolsImg4="",
        this.cgs.poolsImg5=""
  }
  servicesGenders(){ 
    this.mus.servicesGenders(this.usersID).subscribe((res)=>{
    this.gendersCat=res
    })
    }
       
    handleFileInputUpdatesacc($event:any,images) {
    let imageNames= File = $event.target.files[0];
    this.formData.append(images, imageNames);
   }
   createAccessaoriesPosts(){
      this.formData.append('acceriesDescriptions', this.acc.acceriesDescriptions);
     this.formData.append('acceriesName', this.acc.acceriesName);
     this.formData.append('acceriesUse', this.acc.acceriesUse);
     
    this.mus.createAccessaoriesPosts(this.gendersId,this.formData).subscribe((res)=>{
        })
        this.acc.acceriesDescriptions="",
        this.acc.acceriesName="",
        this.acc.acceriesUse="",
        this.acc.acceriesImg=""
  }
    servicesGendersacc(){ 
      this.mus.servicesGenders(this.usersID).subscribe((res)=>{
      this.gendersCatacc=res
      var id = this.gendersCatacc[0]
      console.log(res);
      
       this.gendersClick(id)
     })
      }
    gendersClick(get?){
      this.gendersName=get.genders
      this.gendersId=get._id
      this. servicesaccGet()
      this.servicesaccGetc()

     }
    servicesaccGet(){ 
      this.mus.servicesaccGet(this.gendersId).subscribe((res)=>{
      this.acceriesGet=res
       console.log(res);
       })
      }
      servicesaccGetc(){ 
        this.mus.servicesaccGetc(this.gendersId).subscribe((res)=>{
        this.acceriesGetc=res
         console.log(res);
         })
        }
             
        handleFileInputFec($event:any,images) {
      let imageNames= File = $event.target.files[0];
      this.formData.append(images, imageNames);
     }
     postFecilities(){
        this.formData.append('fecilitiesName', this.fec.fecilitiesName);
       this.formData.append('fecilitiesDescriptions', this.fec.fecilitiesDescriptions);
        
      this.mus.postFecilities(this.usersID,this.formData).subscribe((res)=>{
          })
          this.fec.fecilitiesName="",
          this.fec.fecilitiesDescriptions="",
          this.fec.fecilitiesImg=""
     }
    fecilitiesGet(){ 
      this.mus.fecilitiesGet(this.usersID).subscribe((res)=>{
      this.fecilitiesGets=res
       console.log(res);
       })
      }
      fecilitiesGetc(){ 
        this.mus.fecilitiesGetc(this.usersID).subscribe((res)=>{
        this.fecilitiesGetsc=res
         console.log(res);
         })
        }

// ***************************************************updades **************************************
handleFileInputUpdatesImages($event:any,images){
  this.imageNameupdates= File = $event.target.files[0];
 
}

handleFileInputUpdatessViedoes($event:any,viedoes){
  this.videoName= File = $event.target.files[0];
  this.formData.append(viedoes,this.videoName);
 
 
}

updatesPostData(){
this.formData.append('images',this.imageNameupdates);
this.formData.append('title',this.ups.title);
this.formData.append('description',this.ups.description);
 

  console.log(this.formData)

 this.mus.postsUpdata(this.usersID,this.formData).subscribe((res)=>{
    console.log('assssss',res);
   console.log(this.usersID);
   
     })
     this.ups.title="",
     this.ups.description="",
     this.ups.images="",
     this.ups.viedoes=""
}
 // ******************************************updates present ***************************************
 shareClick(up){
  this.share=!this.share
  this.comments=false
  this.viewcomments=false
  this.replyfrom=false
  this.openshareid=up._id
}
commentsClick(up){
  this.comments=!this.comments
  this.share=false
  this.viewcomments=false
  this.commentspostsId=up._id
   this.commentsGettoClints()
   this.commentsGettoClintsCounts()
 }

viewCommentsClick(up){
  this.viewcomments=!this.viewcomments
  this.share=false
  this.viewcommentsid=up._id
}
replyFromm(comms){
  this.replyfrom=!this.replyfrom
  this.share=false
  this.replyid=comms._id
  this.replyFromCommentesGet()
  console.log(this.replyid);
  
}
 clientsUpdates(){   
  this.mus.clientsUpdatesGet(this.usersID).subscribe((res)=>{
    this.clientUpdates=res
   })
 }
 
 clientsUpdatesCount(){   
  this.mus.clientsUpdatesGetCounts(this.usersID).subscribe((res)=>{
    this.clientUpdatesCount=res
     })
 }

 commentsPoststoClints(){   
  this.mus.commentsPoststoClints(this.replyid,this.postComments).subscribe((res)=>{
     })
     this.postComments.descriptions=""
 }
 commentsGettoClints(){   
  this.mus.commentsGettoClints(this.commentspostsId).subscribe((res)=>{
    this.getUsersComments=res
   })
 }

 commentsGettoClintsCounts(){   
  this.mus.commentsGettoClintsCounts(this.commentspostsId).subscribe((res)=>{
    this.getUsersCommentsCounts=res
    })
 }

 replyFromCommentesGet(){   
  this.mus.replyFromCommentesGet(this.replyid).subscribe((res)=>{
    this.replies=res
    console.log(res);
    
    })
 }
 onlyComments(){   
  this.mus.overallscommentspost(this.usersID).subscribe((res)=>{
    this.onlyComm=res
    console.log(res);
    
    })
 }
 onlyCommentsC(){   
  this.mus.onlyCommentsGetCount(this.usersID).subscribe((res)=>{
    this.onlyCommC=res
    console.log(res);
    
    })
 }


}
  
  
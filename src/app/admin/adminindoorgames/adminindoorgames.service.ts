import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { environment } from '../../../environments/environment';
 
@Injectable()
export class AdminindoorgamesService {

  constructor(private http: HttpClient) { }

  usersDetails(id){
    return this.http.get(environment.apiUrl + "/indoorgamesClientsGetind/"+id);
     }
     servicesOne(id){
      return this.http.get(environment.apiUrl + "/indoorgamesServiceGet/"+id);
       }
       servicesOnec(id){
        return this.http.get(environment.apiUrl + "/indoorgamesServiceGetc/"+id);
         }
         updateServices(id,data){
          return this.http.post(environment.apiUrl + "/indoorgamesServiceCatPost/"+id,data);
           }

           servicesTwo(id){
            return this.http.get(environment.apiUrl + "/indoorgamesServicesGet/"+id);
             }
             servicesTwoc(id){
              return this.http.get(environment.apiUrl + "/indoorgamesServicesGetCounts/"+id);
               }
               updateServicesTwo(id,data){
                return this.http.post(environment.apiUrl + "/indoorgamesServicesPost/"+id,data);
                 }
                       
             clientsUpdatesGet(id){
    
              return this.http.get(environment.apiUrl +"/indoorgamesupdatesget/"+id);
            }
            
            clientsUpdatesGetCounts(id){
              
              return this.http.get(environment.apiUrl +"/indoorgamesupdatesgetCounts/"+id);
            }
            commentsPoststoClints(id,data){
              
              return this.http.post(environment.apiUrl +"/indoorgamesupdatesCommentReplysPost/"+id,data);
            }
            commentsGettoClints(id){
              
              return this.http.get(environment.apiUrl +"/indoorgamesupdatesCommentsget/"+id);
            }
            commentsGettoClintsCounts(id){
              
              return this.http.get(environment.apiUrl +"/indoorgamesupdatesCommentsgetcounts/"+id);
            }
            replyFromCommentesGet(id){
              
              return this.http.get(environment.apiUrl +"/indoorgamesupdatesCommentReplysGet/"+id);
            }
            postUpdatesForm(id,data){
              return this.http.post(environment.apiUrl + "/indoorgamesupdatespostsdata/"+id,data);
               }    
               postFecilities(id,data){
                return this.http.post(environment.apiUrl + "/indoorgamesfecitiesPost/"+id,data);
                 }
                 fecilities(id){
                  return this.http.get(environment.apiUrl + "/indoorgamesfecitiesGet/"+id);
                 }
                 fecilitiesc(id){
                  return this.http.get(environment.apiUrl + "/indoorgamesfecitiesGetCounts/"+id);
                 }
                 generalsCommGet(id) {
                  return this.http.get(environment.apiUrl + "/indoorgamesUsersComments/"+id);
                }
                generalsCommGetC(id) {
                  return this.http.get(environment.apiUrl + "/indoorGamesUsersCommentsCounts/"+id);
                }
    }

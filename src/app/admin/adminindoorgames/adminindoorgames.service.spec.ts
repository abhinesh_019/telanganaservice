import { TestBed, inject } from '@angular/core/testing';

import { AdminindoorgamesService } from './adminindoorgames.service';

describe('AdminindoorgamesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AdminindoorgamesService]
    });
  });

  it('should be created', inject([AdminindoorgamesService], (service: AdminindoorgamesService) => {
    expect(service).toBeTruthy();
  }));
});

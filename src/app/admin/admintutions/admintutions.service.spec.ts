import { TestBed, inject } from '@angular/core/testing';

import { AdmintutionsService } from './admintutions.service';

describe('AdmintutionsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AdmintutionsService]
    });
  });

  it('should be created', inject([AdmintutionsService], (service: AdmintutionsService) => {
    expect(service).toBeTruthy();
  }));
});

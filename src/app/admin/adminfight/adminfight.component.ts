import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AdminfightService } from './adminfight.service';

@Component({
  selector: 'app-adminfight',
  templateUrl: './adminfight.component.html',
  styleUrls: ['./adminfight.component.css']
})
export class AdminfightComponent implements OnInit {

  public showmains:boolean=false
  public updatesShows:boolean=false
  public viewupdatesShows:boolean=false
  public onlyCommentsShows:boolean=false
  public ImagesShows:boolean=false
  public ProductsShows:boolean=false
  public ProductsViewShows:boolean=false

  public usersID:any
public fightsDetailsClients:any
public viewsProfiles:any
public viewsNoti:any
public imageNameupdates:any
public videoName:any
public imageNameupdates1:any
public fightServicesGet:any
public fightServicesGetc:any
public imageNameupdates2:any
public imageNameupdates3:any
public imageNameupdates4:any
public imageNameupdates5:any
public imageNameupdates6:any
public imgFec:any
public fecilitiesGet:any
public fecilitiesGetc:any
public videoNames:any
public openshareid:any
public commentspostsId:any
public viewcommentsid:any
public clientUpdates:any
public replyid:any
public clientUpdatesCount:any
 public getUsersComments:any
public getUsersCommentsCounts:any
public share:boolean=false
public comments:boolean=false
public viewcomments:boolean=false
public replyfrom:boolean=false 
public replies:any
public gcomm:any
public gcommc:any
// public usersID:any
// public usersID:any
// public usersID:any
// public usersID:any

postComments={
  "descriptions":""
}
commentspost={
  "name":"",
  "email":"",
  "contactNo":"",
  "leaveComment":"",
    }

formData: FormData = new FormData(); 

fs={
  optionalTypeName:"",
  fightType:"",
  aboutFightType:"",
  traineerName:"",
  traineerImg:"",
  traineerYearOfExp:"",
  shifts:"",
  timming:"",
  fightType1:"",
  fightType2:"",
  fightType3:"",
  fightType4:"",
  fightType5:"",
  fightTypeOthers:"",
  fightTypeImg1:"",
  fightTypeImg2:"",
  fightTypeImg3:"",
  fightTypeImg4:"",
  fightTypeImg5:"",
  fightTypeImgOthers:"",
  viedoes:"",
}

fec={

  fecilitiesName:"",
  fecilitiesImg:"",
  fecilitiesDescriptions:"",
}
ups={
  images:"",
    title:"",
    descriptions:"",
    viedoes:"",
}
constructor(private route:ActivatedRoute,private fig:AdminfightService) { }
 ngOnInit() {
    this.usersID=localStorage.getItem('ads');
this.usersDetails()
this.fightServicesc()
this.fightServices()
this.fecilitiesc()
this.fecilities()
this.clientsUpdates()
this.clientsUpdatesCount()
this.generalCommGet()
this.generalCommGetc()
  }
 
    usersDetails(){ 
      this.fig.usersDetails(this.usersID).subscribe((res)=>{
      this.fightsDetailsClients=res
      })
      }
    viewProfile(){
      this.viewsProfiles=!this.viewsProfiles
      this.viewsNoti=false
    }
    viewnoti(){
      this.viewsNoti=!this.viewsNoti
      this.viewsProfiles=false
    }
    
    mainMenuShow(){
      this.showmains=!this.showmains
      this.updatesShows=false
      this.viewupdatesShows=false
      this.onlyCommentsShows=false
      this.ImagesShows=false
      this.ProductsShows=false
      this.ProductsViewShows=false
    }
    updatesShow(){
      this.updatesShows=!this.updatesShows
      this.showmains=false
      this.viewupdatesShows=false
      this.onlyCommentsShows=false
      this.ImagesShows=false
      this.ProductsShows=false
      this.ProductsViewShows=false
    }
    viewupdatesShow(){
      this.viewupdatesShows=!this.viewupdatesShows
      this.updatesShows=false
      this.showmains=false
      this.onlyCommentsShows=false
      this.ImagesShows=false
      this.ProductsShows=false
      this.ProductsViewShows=false
    }
    onlyCommentsShow(){
      this.onlyCommentsShows=!this.onlyCommentsShows
      this.updatesShows=false
      this.showmains=false
      this.viewupdatesShows=false
      this.ImagesShows=false
      this.ProductsShows=false
      this.ProductsViewShows=false
    }
    ImagesShow(){
      
      this.ImagesShows=!this.ImagesShows
      this.updatesShows=false
      this.showmains=false
      this.onlyCommentsShows=false
      this.viewupdatesShows=false
      this.ProductsShows=false
      this.ProductsViewShows=false
    }
    ProductsShow(){
      this.ProductsShows=!this.ProductsShows
      this.updatesShows=false
      this.showmains=false
      this.onlyCommentsShows=false
      this.viewupdatesShows=false
      this.ImagesShows=false
      this.ProductsViewShows=false
    }
    ProductsViewShow(){
      this.ProductsViewShows=!this.ProductsViewShows
      this.updatesShows=false
      this.showmains=false
      this.onlyCommentsShows=false
      this.viewupdatesShows=false
      this.ImagesShows=false
      this.ProductsShows=false
    }


    handleFileInputUpdatesI($event:any,images){
      this.imageNameupdates= File = $event.target.files[0];
     }

    handleFileInputUpdatesI1($event:any,images){
      this.imageNameupdates1= File = $event.target.files[0];
     }
     handleFileInputUpdatesI2($event:any,images){
      this.imageNameupdates2= File = $event.target.files[0];
     }
     handleFileInputUpdatesI3($event:any,images){
      this.imageNameupdates3= File = $event.target.files[0];
     }
     handleFileInputUpdatesI4($event:any,images){
      this.imageNameupdates4= File = $event.target.files[0];
     }
     handleFileInputUpdatesI5($event:any,images){
      this.imageNameupdates5= File = $event.target.files[0];
     }
     handleFileInputUpdatesI6($event:any,images){
      this.imageNameupdates6= File = $event.target.files[0];
     }
    handleFileInputUpdatesV($event:any,viedoes){
      this.videoName= File = $event.target.files[0];
      }
    
     updateServices(){
      
      this.formData.append('viedoes',this.videoName);
     this.formData.append('fightTypeImg1',this.imageNameupdates1);
      this.formData.append('fightTypeImg2',this.imageNameupdates2);
     this.formData.append('fightTypeImg3',this.imageNameupdates3);
     this.formData.append('fightTypeImg4',this.imageNameupdates4);
     this.formData.append('fightTypeImg5',this.imageNameupdates5);
     this.formData.append('traineerImg',this.imageNameupdates);
     this.formData.append('fightTypeImgOthers',this.imageNameupdates6);
     this.formData.append('optionalTypeName',this.fs.optionalTypeName);
    this.formData.append('fightType',this.fs.fightType);
    this.formData.append('aboutFightType',this.fs.aboutFightType);
    this.formData.append('traineerName',this.fs.traineerName);
    this.formData.append('traineerYearOfExp',this.fs.traineerYearOfExp);
    this.formData.append('shifts',this.fs.shifts);
    this.formData.append('timming',this.fs.timming);
    this.formData.append('fightType1',this.fs.fightType1);
    this.formData.append('fightType2',this.fs.fightType2);
    this.formData.append('fightType3',this.fs.fightType3);
    this.formData.append('fightType4',this.fs.fightType4);
    this.formData.append('fightType5',this.fs.fightType5);
    this.formData.append('fightTypeOthers',this.fs.fightTypeOthers);
 
     
     this.fig.postsServices(this.usersID,this.formData).subscribe((res)=>{
      
         })
         this.fs.optionalTypeName="",
         this.fs.fightType="",
         this.fs.aboutFightType="",
         this.fs.traineerName="",
         this.fs.traineerImg="",
         this.fs.traineerYearOfExp="",
         this.fs.shifts="",
         this.fs.timming="",
         this.fs.fightType1="",
         this.fs.fightType2="",
         this.fs.fightType3="",
         this.fs.fightType4="",
         this.fs.fightType5="",
         this.fs.fightTypeOthers="",
         this.fs.fightTypeImg1="",
         this.fs.fightTypeImg2="",
         this.fs.fightTypeImg3="",
         this.fs.fightTypeImg4="",
         this.fs.fightTypeImg5="",
         this.fs.fightTypeImgOthers="",
         this.fs.viedoes=""
       
    }

    fightServices(){ 
      this.fig.fightServices(this.usersID).subscribe((res)=>{
      this.fightServicesGet=res
       
      })
      }

      fightServicesc(){ 
        this.fig.fightServicesc(this.usersID).subscribe((res)=>{
        this.fightServicesGetc=res
         
        })
        }
// ************************************************fecilities **********************************
handleFileFec($event:any,images){
  this.imgFec= File = $event.target.files[0];
  }

  postFecilities(){
  
  this.formData.append('fecilitiesImg',this.imgFec);
 this.formData.append('fecilitiesName',this.fec.fecilitiesName);
 this.formData.append('fecilitiesDescriptions',this.fec.fecilitiesDescriptions);
 
 this.fig.postFecilities(this.usersID,this.formData).subscribe((res)=>{
  
     })
     this.fec.fecilitiesDescriptions="",
     this.fec.fecilitiesImg="",
     this.fec.fecilitiesName=""
   }

   fecilities(){ 
    this.fig.fecilities(this.usersID).subscribe((res)=>{
    this.fecilitiesGet=res
  
    
    })
    }

    fecilitiesc(){ 
      this.fig.fecilitiesc(this.usersID).subscribe((res)=>{
      this.fecilitiesGetc=res
       
      })
      }

 // ***************************************************updades **************************************
 handleFileInputUpdates($event:any,images){
  this.imageNameupdates= File = $event.target.files[0];
}

handleFileInputUpdate($event:any,viedoes){
  this.videoNames= File = $event.target.files[0];
  this.formData.append(viedoes,this.videoNames);
  
}

postUpdatesForm(){
this.formData.append('images',this.imageNameupdates);
this.formData.append('title',this.ups.title);
this.formData.append('descriptions',this.ups.descriptions);
 

 

 this.fig.postUpdatesForm(this.usersID,this.formData).subscribe((res)=>{
  
    })
     this.ups.title="",
     this.ups.descriptions="",
     this.ups.images="",
     this.ups.viedoes=""
}

// ******************************************updates present ***************************************
shareClick(up){
  this.share=!this.share
  this.comments=false
  this.viewcomments=false
  this.replyfrom=false
  this.openshareid=up._id
}
commentsClick(up){
  this.comments=!this.comments
  this.share=false
  this.viewcomments=false
  this.commentspostsId=up._id
   this.commentsGettoClints()
   this.commentsGettoClintsCounts()
 }

viewCommentsClick(up){
  this.viewcomments=!this.viewcomments
  this.share=false
  this.viewcommentsid=up._id
}
replyFromm(comms){
  this.replyfrom=!this.replyfrom
  this.share=false
  this.replyid=comms._id
  this.replyFromCommentesGet()
   
}
 clientsUpdates(){   
  this.fig.clientsUpdatesGet(this.usersID).subscribe((res)=>{
    this.clientUpdates=res
   })
 }
 
 clientsUpdatesCount(){   
  this.fig.clientsUpdatesGetCounts(this.usersID).subscribe((res)=>{
    this.clientUpdatesCount=res
     })
 }

 commentsPoststoClints(){   
  this.fig.commentsPoststoClints(this.replyid,this.postComments).subscribe((res)=>{
     })
     this.postComments.descriptions=""
    
     
 }
 commentsGettoClints(){   
  this.fig.commentsGettoClints(this.commentspostsId).subscribe((res)=>{
    this.getUsersComments=res
   })
 }

 commentsGettoClintsCounts(){   
  this.fig.commentsGettoClintsCounts(this.commentspostsId).subscribe((res)=>{
    this.getUsersCommentsCounts=res
    })
 }

 replyFromCommentesGet(){   
  this.fig.replyFromCommentesGet(this.replyid).subscribe((res)=>{
    this.replies=res
     
    })
 }
 generalCommGet(){ 
  this.fig.generalsCommGet(this.usersID).subscribe((res)=>{
  this.gcomm=res
     })
  }

  generalCommGetc(){ 
    this.fig.generalsCommGetC(this.usersID).subscribe((res)=>{
    this.gcommc=res
      })
    }
}

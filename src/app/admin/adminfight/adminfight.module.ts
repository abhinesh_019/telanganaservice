import {MatInputModule} from '@angular/material/input';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material';
import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { AdminfightComponent } from './adminfight.component';
import { AdminfightService } from './adminfight.service';
 
const routes:Routes=[{path:'adminfights',component:AdminfightComponent},]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    SharedModule
  ],
  declarations: [
    AdminfightComponent,
  ],
  providers: [AdminfightService],
})
export class AdminfightModule { }

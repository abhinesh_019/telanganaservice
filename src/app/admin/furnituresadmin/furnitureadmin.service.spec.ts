import { TestBed, inject } from '@angular/core/testing';

import { FurnitureadminService } from './furnitureadmin.service';

describe('FurnitureadminService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FurnitureadminService]
    });
  });

  it('should be created', inject([FurnitureadminService], (service: FurnitureadminService) => {
    expect(service).toBeTruthy();
  }));
});

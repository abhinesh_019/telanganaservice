import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
 import 'rxjs/add/operator/map';
import { environment } from '../../../environments/environment.prod';
 
@Injectable()
export class AdmindanceService {

  constructor(private http: HttpClient) { }
  
  usersDetails(id){
    return this.http.get(environment.apiUrl + "/danceClientsGetind/"+id);
     
  }
  updatesPostDataFec(id,data){
    return this.http.post(environment.apiUrl + "/dancefecitiesPost/"+id,data);
     
  }
  serviceTypesCat(id,data){
    return this.http.post(environment.apiUrl + "/danceServicesPost/"+id,data);
     
  }
  servicesCatageriesGet(id){
    return this.http.get(environment.apiUrl + "/danceServicesGet/"+id);
     
  }
  fecilitiesGet(id){
    return this.http.get(environment.apiUrl + "/dancefecitiesGet/"+id);
     
  }
 
  fecilitiesGetC(id){
    return this.http.get(environment.apiUrl + "/dancefecitiesGetCounts/"+id);
     
  }

  serviceTypeDataGet(id){
    return this.http.get(environment.apiUrl + "/danceServiceGet/"+id);
     
  }

  updatesPostDatas(id,data){
    return this.http.post(environment.apiUrl + "/danceupdatespostsdata/"+id,data);
  }

  postsServices(id,data){
    return this.http.post(environment.apiUrl + "/danceServicesPost/"+id,data);
  }
  updatesPostData(id,data){
    return this.http.post(environment.apiUrl + "/danceupdatesTwopostsdata/"+id,data);
  }
  serviceTwoGet(id) {
    return this.http.get(environment.apiUrl + "/danceServicesTwoGet/"+id);
  }
  
  serviceDataGet(id){
    return this.http.get(environment.apiUrl + "/danceServicesGet/"+id);
     
  }
  serviceDataGetC(id){
    return this.http.get(environment.apiUrl + "/danceServicesGetCounts/"+id);
     
  }
  clientsUpdatesGet(id){
    
    return this.http.get(environment.apiUrl +"/danceupdatesget/"+id);
  }
  
  clientsUpdatesGetCounts(id){
    
    return this.http.get(environment.apiUrl +"/danceupdatesgetCounts/"+id);
  }
  commentsPoststoClints(id,data){
    
    return this.http.post(environment.apiUrl +"/danceupdatesCommentReplysPost/"+id,data);
  }
  commentsGettoClints(id){
    
    return this.http.get(environment.apiUrl +"/danceupdatesCommentsget/"+id);
  }
  commentsGettoClintsCounts(id){
    
    return this.http.get(environment.apiUrl +"/danceupdatesCommentsgetcounts/"+id);
  }
  replyFromCommentesGet(id){
    
    return this.http.get(environment.apiUrl +"/danceupdatesCommentReplysGet/"+id);
  }
  generalsCommGet(id) {
    return this.http.get(environment.apiUrl + "/daneceuserscomments/"+id);
  }
  generalsCommGetC(id) {
    return this.http.get(environment.apiUrl + "/daneceuserscommentsCounts/"+id);
  }

  updatesPostDataCats(id,data){
    return this.http.post(environment.apiUrl + "/danceCatageriesPosts/"+id,data);
     
  }
  CatageriesGets(id){
    return this.http.get(environment.apiUrl + "/danceCatageriesGets/"+id);
     
  }
  
  
}
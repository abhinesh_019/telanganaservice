import { Component, OnInit } from '@angular/core';
import { MarriagebureauService } from './marriagebureau.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AnonymousSubscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-marriagebureaus',
  templateUrl: './marriagebureaus.component.html',
  styleUrls: ['./marriagebureaus.component.css']
})
export class MarriagebureausComponent implements OnInit {

 
  public area:any
  public searchString:String;
  searchTerm:String;
  public locationsget:any
  public locations:any
  public locationName:any
public its:any
public locationget:any
public areasAll:any
public selectAreaId:any
public selectArea:any
public marriageuser:any
public marriageusercount:any
public popularareas:any
public amarriageusercount:any
public marriageusercountLocations:any
public marriageusercounttot:any
 public marriageusercountMainAreas:any
public marriageusercountSubAreas:any
public marriageusercountSingleAreas:any
    // **********************************************************
 
    public imageAddsOne:any
    public babycaresAddsOnes:any
     
    public babycaresAddsFours:any
    public imageAddsFour:any
    public imageAddsThree:any
    public imageAddsTwo:any
    public babycaresAddsThrees:any
    public babycaresAddsTwos:any
    
    
    public babycaresAddsFivesLoc:any
    public imageAddsFiveLoc:any
    public imageAddsFourLoc:any
    public babycaresAddsFoursLoc:any
    public imageAddsThreeLoc:any
    public babycaresAddsThreeLoc:any
    public imageAddsTwoLoc:any
    public babycaresAddsTwoLoc:any
    public imageAddsOneLoc:any
    public babycaresAddsOneLoc:any
  constructor(private router:Router,private route:ActivatedRoute,private marriage:MarriagebureauService) { }

  ngOnInit() {
    this.getlocations()
    this.marriageuserscounttot()
    this.marriageuserscountLocations()
   }

    
  // locations*****************
getlocations(){
  this.marriage.locationapi().subscribe((res)=>{
    this.locationsget=res;
   var id =this.locationsget[0];
     this.location(id)
   })}

  location(get?){
    this.locationName=get.locations
    this.its=get._id
    this.allAreas()
    this.babycaressareasAddsOneLoc()
      this.babycaressareasAddsTwoLoc()
      this.babycaressareasAddsThreeLoc()
      this.babycaressareasAddsFourLoc()
      this.babycaressareasAddsFiveLoc()
   }
 
   allAreas(){
     this.marriage.areaapi(this.its).subscribe((res)=>{
      this.areasAll=res
      var id =this.areasAll[0];
       this.selectedAre(id)
     })
   }

selectedAre(result){
this.selectAreaId=result._id
this.selectArea=result.area
this.marriageuserscountMainAreas()   
this.marriageusers()
this.marriageuserscount()
this.marriageuserscountMainAreas()
this.babycaressareasAddsOnea()
this.babycaressareasAddsTwoa()
this.babycaressareasAddsThreea()
this.babycaressareasAddsFoura()
   }


 marriageusers(){
  var data:any =  {}
  if(this.searchString){
    data.search=this.searchString
    }
  this.marriage.marriageusers(this.selectAreaId,data).subscribe((res)=>{
    this.marriageuser=res
    console.log(res);
    
   })
 }

searchFilter(){
this.marriageusers()
}

// ***************************************************************
marriageuserSinglecountArea(){
  this.marriage.marriageusersForSingleAreas(this.selectAreaId).subscribe((res)=>{
    this.marriageusercountSingleAreas=res
   })
 }
marriageuserscount(){
  this.marriage.marriageusersForSubAreas(this.selectAreaId).subscribe((res)=>{
    this.marriageusercountSubAreas=res
   })
 }
 marriageuserscountLocations(){
  this.marriage.marriageusersForLocations().subscribe((res)=>{
    this.marriageusercountLocations=res
   })
 }
 marriageuserscountMainAreas(){
  this.marriage.marriageusersForMainAreas(this.selectAreaId).subscribe((res)=>{
    this.marriageusercountMainAreas=res
   })
 }
// ***************************************************************
marriageuserscounttot(){
  this.marriage.marriageuserscountstot().subscribe((res)=>{
    this.marriageusercounttot=res
 })
 }
 
 
// ****************************************************


// ****************************************************

babycaressareasAddsOnea(){
  this.marriage.babycaresAddsOnea(this.selectAreaId).subscribe((res)=>{
     this.babycaresAddsOnes=res
 
     
   })
}
babycaressareasAddsTwoa(){
  this.marriage.babycaresAddsTwoa(this.selectAreaId).subscribe((res)=>{
     this.babycaresAddsTwos=res
  
   })
}
babycaressareasAddsThreea(){
  this.marriage.babycaresAddsThreea(this.selectAreaId).subscribe((res)=>{
     this.babycaresAddsThrees=res
  
   })
}

babycaressareasAddsFoura(){
  this.marriage.babycaresAddsFoura(this.selectAreaId).subscribe((res)=>{
     this.babycaresAddsFours=res

   })
}

babycaressareasAddsOneLoc(){
  this.marriage.babycaresAddsOnel(this.its).subscribe((res)=>{
     this.babycaresAddsOneLoc=res 
   })
}
babycaressareasAddsTwoLoc(){
  this.marriage.babycaresAddsTwol(this.its).subscribe((res)=>{
     this.babycaresAddsTwoLoc=res
    
   })
}
babycaressareasAddsThreeLoc(){
  this.marriage.babycaresAddsThreel(this.its).subscribe((res)=>{
     this.babycaresAddsThreeLoc=res
     
   })
}
babycaressareasAddsFourLoc(){
  this.marriage.babycaresAddsFourl(this.its).subscribe((res)=>{
     this.babycaresAddsFoursLoc=res
     
   })
}
babycaressareasAddsFiveLoc(){
  this.marriage.babycaresAddsFivel(this.its).subscribe((res)=>{
     this.babycaresAddsFivesLoc=res
    
   })
}

}

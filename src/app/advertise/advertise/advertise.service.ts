import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
 import 'rxjs/add/operator/map';
import { environment } from '../../../environments/environment.prod';


@Injectable()
export class AdvertiseService {

  constructor(private http: HttpClient) { }

 
  addsPosts(data){
    
    return this.http.post(environment.apiUrl +"/advertiseOnly",data);
  }
  addsPost(data){
    
    return this.http.post(environment.apiUrl +"/feedbackOnly",data);
  }
}

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AdvertiseService } from './advertise.service';

@Component({
  selector: 'app-advertise',
  templateUrl: './advertise.component.html',
  styleUrls: ['./advertise.component.css']
})
export class AdvertiseComponent implements OnInit {
   advertises={
    "companyName":"",
    "firstName":"",
    "lastName":"",
    "city":"",
    "mobileNo":"",
    "landLineNo":""
      }

      feedbackpost={
        city:"",
        text:""
      }
      constructor(private route:ActivatedRoute,private feeds:AdvertiseService) { }

  ngOnInit() {
  }
  // ***********0verallcomments************
  advertise(){
       
     
    this.feeds.addsPosts(this.advertises).subscribe((res)=>{
     
     this.advertises.companyName="",
     this.advertises.firstName="",
     this.advertises.lastName="",
     this.advertises.city="",
     this.advertises.mobileNo="",
     this.advertises.landLineNo=""
    })
  }

   // ***********0verallcomments************
   overallcomment(){
       
     
    this.feeds.addsPosts(this.feedbackpost).subscribe((res)=>{
     
     this.feedbackpost.text="",
    
     this.feedbackpost.city=""
    
    })
  }
}
